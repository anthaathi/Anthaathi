package org.anthaathi.commerce.checkout.utils

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class RelayUtilsTest {
    @Test
    fun globalID() {
        val result = RelayUtils.fromGlobalID(RelayUtils.GlobalID(id = "1", type = "Cart", queryParams = mapOf(
            "hash" to mutableListOf("123")
        )))

        assertEquals("Z2lkOi8vYW50aGFhdGhpL0NhcnQvMT9oYXNoPTEyMw==", result)
    }

    @Test
    fun toGlobalId() {
        val result = RelayUtils.toGlobalID("Z2lkOi8vYW50aGFhdGhpL0NhcnQvMT9oYXNoPTEyMw==")

        assertEquals("Cart", result.type)
        assertEquals("1", result.id)
        assertEquals("123", result.queryParams?.get("hash")?.first())
    }
}
