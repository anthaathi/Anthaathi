package org.anthaathi.authentication.entity

import java.io.Serializable
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "metafield")
open class Metafield : Serializable {
    @Id
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @Column(name = "date_created")
    open var dateCreated: OffsetDateTime? = null

    @Column(name = "date_updated")
    open var dateUpdated: OffsetDateTime? = null

    @Column(name = "key")
    open var key: String? = null

    @Column(name = "namespace")
    open var namespace: String? = null

    @Column(name = "value")
    open var value: String? = null

    @Column(name = "type")
    open var type: String? = null

    @Column(name = "description")
    open var description: String? = null

    companion object {
        private const val serialVersionUID = -6198756472182950749L
    }
}
