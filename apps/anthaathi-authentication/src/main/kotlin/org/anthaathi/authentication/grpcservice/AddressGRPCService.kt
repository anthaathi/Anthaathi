package org.anthaathi.authentication.grpcservice

import io.quarkus.grpc.GrpcService
import io.smallrye.mutiny.Uni
import org.anthaathi.authentication.repository.CustomerAddressRepository
import org.anthaathi.authentication.repository.CustomerDefaultAddressRepository
import org.anthaathi.authentication.service.AddressService
import org.anthaathi.common.*

@GrpcService
class AddressGRPCService(
    var addressService: AddressService,
) : CustomerAddressGRPC {
    override fun customerAddressCreate(request: CustomerAddressCreateInput?): Uni<CustomerAddressCreateResponse> {
        return request?.let { this.addressService.customerAddressCreate(it) }!!
    }

    override fun customerAddressDelete(request: CustomerAddressDeleteInput?): Uni<CustomerAddressDeleteResponse> {
        return request?.let { this.addressService.customerAddressDelete(request) }!!
    }

    override fun customerAddressUpdate(request: CustomerAddressUpdateInput?): Uni<CustomerAddressUpdateResponse> {
        return request?.let { this.addressService.customerAddressUpdate(request) }!!
    }

    override fun customerDefaultAddressUpdate(request: CustomerDefaultAddressUpdateInput?): Uni<CustomerDefaultAddressUpdateResponse> {
        return request?.let { this.addressService.customerDefaultAddressUpdate(request) }!!
    }
}
