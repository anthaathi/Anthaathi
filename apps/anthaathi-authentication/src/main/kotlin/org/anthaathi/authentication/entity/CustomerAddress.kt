package org.anthaathi.authentication.entity

import org.anthaathi.common.CustomerAddressCreateResponse
import org.anthaathi.common.MailingAddress
import org.anthaathi.common.MailingAddressInput
import java.io.Serializable
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "customer_address")
open class CustomerAddress : Serializable {
    @Id
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @Column(name = "date_created")
    open var dateCreated: OffsetDateTime? = null

    @Column(name = "date_updated")
    open var dateUpdated: OffsetDateTime? = null

    @Column(name = "address1")
    open var address1: String? = null

    @Column(name = "address2")
    open var address2: String? = null

    @Column(name = "city")
    open var city: String? = null

    @Column(name = "customer_id")
    open var customerId: UUID? = null

    @Column(name = "company")
    open var company: String? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_info", insertable = false, updatable = false)
    open var countryInfo: Country? = null

    @Column(name = "country_info")
    open var countryCode: String? = null

    @Column(name = "\"default\"")
    open var defaultField: Boolean? = null

    @Column(name = "first_name")
    open var firstName: String? = null

    @Column(name = "last_name")
    open var lastName: String? = null

    @Column(name = "phone")
    open var phone: String? = null

    @Column(name = "province")
    open var province: String? = null

    @Column(name = "province_code")
    open var provinceCode: String? = null

    @Column(name = "zip")
    open var zip: String? = null

    companion object {
        private const val serialVersionUID = 6053634668407147350L

        fun fromGRPCType(input: MailingAddressInput): CustomerAddress {
            val result = CustomerAddress()

            result.address1 = input.address1
            result.address2 = input.address2
            result.city = input.city
            result.company = input.company
            result.countryCode = input.country
            result.company = input.company
            result.city = input.city
            result.firstName = input.firstName
            result.lastName = input.lastName
            result.phone = input.phone
            result.province = input.province
            result.provinceCode = input.provinceCode
            result.zip = input.zip

            return result
        }
    }

    fun toGRPCType(): MailingAddress {
        return MailingAddress.newBuilder()
            .setAddress1(this.address1)
            .setAddress2(this.address2)
            .setCity(this.city)
            .setCompany(this.company)
            .setCountry(this.countryCode)
            .setFirstName(this.firstName)
            .setLastName(this.lastName)
            .setPhone(this.phone)
            .setProvince(this.province)
            .setProvinceCode(this.provinceCode)
            .setZip(this.zip)
            .build()
    }
}
