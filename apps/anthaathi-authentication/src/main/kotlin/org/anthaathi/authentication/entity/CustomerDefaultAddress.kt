package org.anthaathi.authentication.entity

import java.util.UUID
import javax.persistence.*

@Entity
@Table(name = "customer_default_address")
open class CustomerDefaultAddress {
    @Id
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id", insertable = false, updatable = false)
    open var defaultAddress: CustomerAddress? = null

    @Column(name = "address_id")
    open var defaultAddressId: UUID? = null
}
