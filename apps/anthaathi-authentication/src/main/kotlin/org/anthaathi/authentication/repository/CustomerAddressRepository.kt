package org.anthaathi.authentication.repository

import io.quarkus.hibernate.orm.panache.kotlin.PanacheRepositoryBase
import org.anthaathi.authentication.entity.CustomerAddress
import java.util.*
import javax.enterprise.context.ApplicationScoped

@ApplicationScoped
class CustomerAddressRepository : PanacheRepositoryBase<CustomerAddress, UUID> {
    fun deleteCustomerAddress(customerId: UUID, addressId: UUID): Long {
        return delete("customerId = ?1 and id = ?", customerId, addressId)
    }

    fun findByCustomerIdAndId(customerId: UUID, addressId: UUID): CustomerAddress {
        return find("customerId = ?1 and id = ?", customerId, addressId).singleResult()
    }
}
