package org.anthaathi.authentication.grpcservice

import io.quarkus.grpc.GrpcService
import io.smallrye.common.annotation.Blocking
import io.smallrye.mutiny.Uni
import org.anthaathi.authentication.service.CustomerService
import org.anthaathi.common.CustomerGRPC
import org.anthaathi.common.GetCustomerByIdInput
import org.anthaathi.common.GetCustomerByIdResponse
import java.util.*

@GrpcService
class CustomerGRPCService(
    var customerService: CustomerService
) : CustomerGRPC {

    @Blocking
    override fun getCustomerById(request: GetCustomerByIdInput?): Uni<GetCustomerByIdResponse> {
        return request?.let {
            val customer = customerService.findById(UUID.fromString(it.customerId))

            Uni.createFrom().item {
                GetCustomerByIdResponse.newBuilder()
                    .setCustomer(customer)
                    .build()
            }
        }!!
    }
}
