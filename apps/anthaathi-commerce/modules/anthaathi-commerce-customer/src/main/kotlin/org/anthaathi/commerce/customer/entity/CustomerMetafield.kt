package org.anthaathi.commerce.customer.entity

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "customer_metafield")
open class CustomerMetafield {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @Column(name = "key", nullable = false)
    open var key: String? = null
}
