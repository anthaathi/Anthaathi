package org.anthaathi.commerce.customer.entity

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "customer")
open class CustomerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    open var id: UUID? = null
}
