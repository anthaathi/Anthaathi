package org.anthaathi.commerce.address.grpcservice

import io.quarkus.grpc.GrpcService
import io.smallrye.common.annotation.Blocking
import io.smallrye.mutiny.Uni
import org.anthaathi.commerce.address.entity.MailingAddressEntity
import org.anthaathi.commerce.address.repository.MailingAddressRepository
import org.anthaathi.common.*
import org.anthaathi.common.MailingAddress
import java.util.*
import javax.transaction.Transactional

@GrpcService
class MailingAddress(
    var mailingAddressRepository: MailingAddressRepository,
): MailingAddressGRPC {
    @Blocking
    @Transactional
    override fun createMailingAddress(request: CreateMailingAddressMessage?): Uni<MailingAddress> {
        return Uni.createFrom().item {
            request?.let {
                if (!Locale.getISOCountries().contains(it.address.country)) {
                    return@let null
                }

                val result = MailingAddressEntity.fromGRPCObject(it)
                mailingAddressRepository.persist(result)
                result.toGRPCType()
            }
        }
    }

    @Blocking
    @Transactional
    override fun getMailingAddressForCustomer(request: GetMailingAddressForCustomerMessage?): Uni<MailingAddressList> {
        return Uni.createFrom().item {
            request?.let {
                if (it.customerId.isNullOrEmpty()) {
                    return@item null
                }
                MailingAddressList.newBuilder()
                    .addAllAddress(
                        mailingAddressRepository
                            .findByCustomerId(UUID.fromString(it.customerId)).map { mailAddressEntity ->
                                mailAddressEntity.toGRPCType()
                            }
                    )
                    .build()
            }
        }
    }

    @Blocking
    @Transactional
    override fun getMailingAddressById(request: GetMailingAddressByIdMessage?): Uni<MailingAddress> {
        return Uni.createFrom().item {
            request?.let {
                if (it.customerId.isNullOrEmpty()) {
                    return@item null
                }

                mailingAddressRepository
                    .findByIdAndCustomerId(
                        UUID.fromString(request.addressId),
                        UUID.fromString(request.customerId)
                    ).toGRPCType()
            }
        }
    }

    @Blocking
    @Transactional
    override fun deleteMailingAddressById(request: DeleteMailingAddressByIdMessage?): Uni<MailingAddress> {
        return Uni.createFrom().item {
            request?.let {
                if (it.customerId.isNullOrEmpty()) {
                    return@item null
                }

                val customerId = UUID.fromString(request.customerId)
                val addressId = UUID.fromString(request.addressId)

                val result = mailingAddressRepository.findByIdAndCustomerId(addressId, customerId)

                mailingAddressRepository.deleteByIdAndCustomerId(addressId, UUID.fromString(request.customerId))

                result.toGRPCType()
            }
        }
    }
}
