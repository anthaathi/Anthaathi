import {
  CategoricalColumn,
  CustomColumn,
  DatetimeColumn,
  NumericalColumn,
  StringColumn,
} from 'baseui/data-table';
import { SalesDataOrderInterface } from '../../__generated__';
import { PLACEMENT, StatefulPopover } from 'baseui/popover';
import { Button, KIND, SIZE } from 'baseui/button';
import { Overflow } from 'baseui/icon';
import { useStyletron } from 'baseui';
import { StatefulMenu } from 'baseui/menu';
import type { FilterPageColumn } from '../../features/FilterBuilder';
import { Tag } from 'baseui/tag';
import { Link } from 'react-router-dom';
import { StyledLink } from 'baseui/link';

export const columns = ({
  authority,
}: {
  authority: string[];
}): FilterPageColumn[] => {
  return [
    {
      column: CustomColumn({
        title: 'Order ID',
        mapDataToValue: (data: SalesDataOrderInterface) => {
          return data.incrementId ?? '-';
        },
        renderCell: (data) => {
          return (
            <StyledLink $as={Link} to={data.value}>
              {data.value}
            </StyledLink>
          );
        },
        minWidth: 32,
      }),
      filterOrder: -1,
      authority: [
        'admin',
        'supervisor',
        'foreman',
        'picker',
        'dispatcher',
        'driver',
      ],
      key: 'increment_id',
      filterConfig: {},
    },
    {
      key: 'picker_status',
      column: StringColumn({
        title: 'Picker Status',
        mapDataToValue: (data: SalesDataOrderInterface) => data[1],
      }),
      authority: ['supervisor'],
    },
    {
      column: StringColumn({
        title: 'Assigned status',
        mapDataToValue: (data: SalesDataOrderInterface) => data[2],
      }),
      authority: ['foreman'],
    },
    {
      authority: ['dispatcher'],
      column: StringColumn({
        title: 'Assigned status',
        mapDataToValue: (data: SalesDataOrderInterface) => data[3],
      }),
    },
    {
      key: 'city',
      authority: ['admin', 'supervisor', 'foreman', 'dispatcher', 'driver'],
      filterOrder: 4,
      column: CategoricalColumn({
        title: 'Emirate / City',
        mapDataToValue: (data: SalesDataOrderInterface) =>
          data.billingAddress?.city ?? '',
      }),
    },
    {
      key: 'zone',
      filterOrder: 5,
      column: CategoricalColumn({
        title: 'Zone',
        mapDataToValue: (data: SalesDataOrderInterface) =>
          data.billingAddress?.street?.[1] ?? '',
      }),
      authority: [
        'admin',
        'supervisor',
        'foreman',
        'driver',
        'dispatcher',
        'driver',
      ],
    },
    {
      key: 'delivery_date',
      column: DatetimeColumn({
        title: 'Delivery Date',
        formatString: 'd MMMM yyyy',
        mapDataToValue: (data: SalesDataOrderInterface) =>
          new Date(data.deliveryDate ?? new Date().getTime()),
      }),
    },
    {
      key: ['customer_firstname', 'customer_lastname'],
      authority: [
        'admin',
        'supervisor',
        'foreman',
        'picker',
        'dispatcher',
        'driver',
      ],
      column: StringColumn({
        title: 'Customer Details',
        minWidth: 240,
        mapDataToValue: (data: SalesDataOrderInterface) => {
          return `${data.customerFirstname} ${data.customerLastname}
                  ${data.billingAddress?.telephone}` as never;
        },
      }),
    },
    {
      filterConfig: false,
      column: CategoricalColumn({
        title: 'Address Details',
        mapDataToValue: (data: SalesDataOrderInterface) =>
          data.billingAddress?.street?.[0] ?? '',
      }),
      authority: ['admin', 'supervisor', 'foreman', 'driver', 'dispatcher'],
    },
    {
      key: 'total_qty_ordered',
      authority: ['foreman', 'picker'],
      column: NumericalColumn({
        title: 'Number of items',
        mapDataToValue: (data: SalesDataOrderInterface) =>
          data.items.length ?? 0,
      }),
    },
    {
      key: 'discount_description',
      authority: ['admin', 'supervisor'],
      filterConfig: false,
      column: CategoricalColumn({
        title: 'Discount Code',
        mapDataToValue: (data: SalesDataOrderInterface) =>
          data?.discountDescription ?? '',
      }),
    },
    {
      key: 'grand_total',
      authority: ['admin', 'supervisor'],
      column: NumericalColumn({
        title: 'Total',
        format: (value) => {
          return Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'AED',
          }).format(value);
        },
        mapDataToValue: (data: SalesDataOrderInterface) => data.grandTotal,
      }),
    },
    {
      authority: ['admin', 'supervisor'],
      filterConfig: {},
      column: CategoricalColumn({
        title: 'Payment Type',
        mapDataToValue: (data: SalesDataOrderInterface) =>
          data.payment?.additionalInformation?.[0] || '',
      }),
    },
    {
      key: 'created_at',
      authority: ['admin', 'supervisor'],
      column: DatetimeColumn({
        title: 'Order Date',
        formatString: 'd-MMMM-yyyy',
        mapDataToValue: (data: SalesDataOrderInterface) =>
          new Date(data.createdAt ?? new Date()),
      }),
    },
    {
      key: 'status',
      authority: ['admin', 'supervisor'],
      enums: ['canceled', 'pending'],
      column: CustomColumn({
        title: 'Order Status',
        mapDataToValue: (data: SalesDataOrderInterface) => data.status ?? '-',
        renderCell: (input) => {
          switch (input.value) {
            case 'canceled':
              return (
                <Tag closeable={false} kind={'red'} variant={'solid'}>
                  {input.value}
                </Tag>
              );
            case 'pending':
              return (
                <Tag
                  closeable={false}
                  kind={'neutral'}
                  variant={'solid'}
                  overrides={{}}
                >
                  {input.value}
                </Tag>
              );

            default:
              return <Tag closeable={false}>{input.value}</Tag>;
          }
        },
      }),
    },
    {
      authority: ['admin', 'supervisor'],
      column: CategoricalColumn({
        title: 'Warehouse Status',
        mapDataToValue: (data: SalesDataOrderInterface) => data[7],
      }),
    },
    {
      key: 'picker_username',
      authority: ['picker'],
      column: CategoricalColumn({
        title: 'Checkout Status',
        mapDataToValue: (data: SalesDataOrderInterface) => data[7],
      }),
    },
    {
      key: 'dispatcher_username',
      authority: ['dispatcher'],
      column: CategoricalColumn({
        title: 'Picker status',
        mapDataToValue: (data: SalesDataOrderInterface) => data[7],
      }),
    },
    {
      authority: ['driver', 'dispatcher'],
      column: CategoricalColumn({
        title: 'Delivery Status',
        mapDataToValue: (data: SalesDataOrderInterface) => data[7],
      }),
    },
    {
      key: 'assigned_picker',
      authority: ['supervisor', 'foreman'],
      column: CategoricalColumn({
        title: 'Assigned Picker',
        mapDataToValue: (data: SalesDataOrderInterface) => data[7],
      }),
    },
    {
      key: 'assigned_driver',
      authority: ['supervisor', 'dispatcher'],
      column: CategoricalColumn({
        title: 'Assigned Driver',
        mapDataToValue: (data: SalesDataOrderInterface) => data[7],
      }),
    },
    {
      key: 'customer_note',
      authority: ['admin', 'supervisor'],
      filterConfig: false,
      column: StringColumn({
        title: 'Special Instructions',
        filterable: false,
        mapDataToValue: (data: SalesDataOrderInterface) =>
          data.customerNote ?? '-',
      }),
    },
    {
      key: 'region',
      filterOrder: 10,
      authority: ['admin', 'supervisor', 'foreman', 'dispatcher', 'driver'],
      column: CategoricalColumn({
        title: 'Area',
        mapDataToValue: (data: SalesDataOrderInterface) => data[7],
      }),
      enums: ['Omkar'],
    },
    {
      key: ['customer_firstname', 'customer_lastname'],
      filterOrder: 2,
      column: CategoricalColumn({
        title: 'Customer Name',
        mapDataToValue: (data: SalesDataOrderInterface) =>
          data.customerFirstname + ' ' + data.customerLastname,
      }),
    },
    {
      key: 'customer_email',
      filterConfig: false,
      column: CategoricalColumn({
        title: 'Customer Email',
        mapDataToValue: (data: SalesDataOrderInterface) => data.customerEmail,
      }),
    },
    {
      column: CategoricalColumn({
        title: 'Customer Contact',
        mapDataToValue: (data: SalesDataOrderInterface) =>
          data.billingAddress?.telephone ?? '-',
      }),
    },
    {
      filterConfig: false,
      column: CustomColumn({
        mapDataToValue(data: SalesDataOrderInterface) {
          return data.incrementId;
        },
        title: 'Actions',
        filterable: false,
        sortable: false,
        cellBlockAlign: 'center',
        renderCell() {
          const [css] = useStyletron();

          return (
            <div
              className={css({
                width: '100%',
                display: 'flex',
                alignItems: 'center',
                placeContent: 'center',
              })}
            >
              <StatefulPopover
                placement={PLACEMENT.bottom}
                content={
                  <div className={css({ backgroundColor: '#FFF' })}>
                    <StatefulMenu
                      onItemSelect={console.log}
                      items={[
                        {
                          label: 'Assign Driver',
                          id: 'assign-driver',
                        },
                        {
                          label: 'Assign Picker',
                          id: 'assign-picker',
                        },
                      ]}
                    />
                  </div>
                }
              >
                <Button kind={KIND.secondary} size={SIZE.mini}>
                  <Overflow />
                </Button>
              </StatefulPopover>
            </div>
          );
        },
      }),
    },
  ].filter(
    (res) =>
      res.authority?.findIndex((res) => authority.indexOf(res) !== -1) !== -1
  );
};
