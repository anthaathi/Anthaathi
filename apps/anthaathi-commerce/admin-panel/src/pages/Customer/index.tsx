import { Breadcrumbs } from 'baseui/breadcrumbs';
import { StyledLink } from 'baseui/link';
import React, { useEffect, useMemo, useState } from 'react';
import { useStyletron } from 'baseui';
import { FilterBuilder, FilterPageColumn } from '../../features/FilterBuilder';
import {
  CustomColumn,
  DataTable,
  Row,
  SortDirections,
  StringColumn,
} from 'baseui/data-table';
import { useRestConfig } from '../../hooks/use-rest-config';
import usePromise, { States } from '../../hooks/use-promise';
import {
  CustomerDataCustomerInterface,
  CustomersSearchApi,
} from '../../__generated__';
import { usePage } from '../../features/FilterPage/hooks/usePage';
import { SORT_DIRECTION } from 'baseui/table';
import { DataTableLoading } from '../../features/Common/Components/Loading';
import { Pagination } from 'baseui/pagination';
import { Link } from 'react-router-dom';

const useColumn: () => FilterPageColumn[] = () => {
  return useMemo(
    () => [
      {
        column: CustomColumn({
          title: 'Customer ID',
          mapDataToValue: (data) => data.id,
          renderCell: (value) => {
            return (
              <StyledLink $as={Link} to={`/customer/${value.value}`}>
                {value.value}
              </StyledLink>
            );
          },
        }),
      },
      {
        column: StringColumn({
          title: 'First name',
          mapDataToValue: (data: CustomerDataCustomerInterface) =>
            data.firstname,
        }),
      },
      {
        column: StringColumn({
          title: 'Last name',
          mapDataToValue: (data: CustomerDataCustomerInterface) =>
            data.lastname,
        }),
      },
      {
        column: StringColumn({
          title: 'Email',
          mapDataToValue: (data: CustomerDataCustomerInterface) => data.email,
        }),
      },
      {
        column: StringColumn({
          title: 'Date of birth',
          mapDataToValue: (data: CustomerDataCustomerInterface) =>
            data.dob ?? '-',
        }),
      },
    ],
    []
  );
};

export default function Customer() {
  const [css, $theme] = useStyletron();
  const restConfig = useRestConfig();

  const [page, setPage] = usePage();

  const [result, error, state] = usePromise(() => {
    return new CustomersSearchApi(restConfig).getV1CustomersSearch({
      searchCriteriaPageSize: 25,
      searchCriteriaCurrentPage: page,
    });
  }, [restConfig, page]);

  const numPages_ =
    (result?.totalCount ?? 0) / (result?.searchCriteria?.pageSize ?? 0);
  const currentPage_ = result?.searchCriteria?.currentPage ?? 1;

  const [numPages, setNumPages] = useState(numPages_);
  const [currentPage, setCurrentPage] = useState(currentPage_);

  useEffect(() => {
    if (!isNaN(numPages_) && numPages_ !== numPages) {
      setNumPages(numPages_);
    }
    if (!isNaN(currentPage_) && currentPage !== currentPage_) {
      setCurrentPage(currentPage_);
    }
  }, [currentPage_, numPages_]);

  const rows = useMemo(() => {
    return (
      (result?.items.map((res) => ({
        data: res,
        id: res.id,
      })) as Row[]) ?? []
    );
  }, [result]);

  const [sort, setSort] = useState<{
    index: number;
    direction: typeof SORT_DIRECTION.ASC | typeof SORT_DIRECTION.DESC;
  }>({ index: 0, direction: SORT_DIRECTION.ASC });

  const columns = useColumn();

  return (
    <div
      className={css({
        margin: '0 auto',
        paddingInlineStart: '64px',
        paddingInlineEnd: '64px',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      })}
    >
      <div
        className={css({
          display: 'flex',
          alignItems: 'center',
          paddingTop: $theme.sizing.scale800,
          paddingBottom: $theme.sizing.scale800,
        })}
      >
        <Breadcrumbs>
          <StyledLink href="#parent">Admin</StyledLink>
          <StyledLink href="#sub">Product</StyledLink>
          <StyledLink href="#sub">Customer</StyledLink>
        </Breadcrumbs>

        <span className={css({ flexGrow: 1 })} />
      </div>

      <FilterBuilder columns={columns} onFilter={() => {}} />

      <div className={css({ flexGrow: 1, paddingTop: '12px' })}>
        <DataTable
          sortIndex={sort.index}
          sortDirection={sort.direction}
          onSort={(columnIndex, sortDirection?: SortDirections) => {
            setSort({
              index: columnIndex,
              direction: sortDirection ?? SORT_DIRECTION.DESC,
            });
            return;
          }}
          resizableColumnWidths={true}
          loading={state === States.pending}
          columns={columns.map((res) => res.column)}
          loadingMessage={() => <DataTableLoading />}
          rows={rows}
        />
      </div>
      <Pagination
        numPages={Math.ceil(numPages)}
        currentPage={currentPage}
        onPageChange={({ nextPage }) => {
          setPage(nextPage);
        }}
      />
    </div>
  );
}
