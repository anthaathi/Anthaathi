import { useStyletron } from 'baseui';
import { useParams } from 'react-router-dom';
import usePromise, { States } from '../../hooks/use-promise';
import { CustomersCustomerIdApi } from '../../__generated__';
import { useRestConfig } from '../../hooks/use-rest-config';
import {
  Card,
  CardCopyData,
  CardDivider,
  CardTitle,
} from '../../features/Card';
import {
  AppWrapper,
  BreadcrumbWrapper,
} from '../../features/BreadcrumbWrapper';
import { Breadcrumbs } from 'baseui/breadcrumbs';
import { StyledLink } from 'baseui/link';
import React from 'react';
import { Cell, Grid } from 'baseui/layout-grid';
import { RenderAddress } from '../../features/RenderAddress';
import { CustomerOrders } from '../../features/Customer/GetCustomerOrder';
import { LabelSmall } from 'baseui/typography';

export function CustomerInfo() {
  const [css, $theme] = useStyletron();

  const params = useParams<{ id: string }>();
  const restConfig = useRestConfig();

  const [result, error, state] = usePromise(() => {
    return new CustomersCustomerIdApi(restConfig).getV1CustomersCustomerId({
      customerId: +params?.id!!,
    });
  }, [params.id, restConfig]);

  if (state === States.pending) {
    return <>Loading</>;
  }

  return (
    <AppWrapper>
      <BreadcrumbWrapper>
        <Breadcrumbs>
          <StyledLink href="#parent">Admin</StyledLink>
          <StyledLink href="#sub">Customer</StyledLink>
          <StyledLink href="#sub">#{}</StyledLink>
        </Breadcrumbs>
      </BreadcrumbWrapper>

      <Grid
        gridMaxWidth={0}
        gridMargins={0}
        overrides={{
          GridWrapper: { style: { width: '100%', height: '100%' } },
        }}
      >
        <Cell span={8}>
          <Card>
            <CardTitle>Previous Orders</CardTitle>
            <CardDivider />
            {result.id && <CustomerOrders customerId={result.id} />}
          </Card>
        </Cell>
        <Cell span={4}>
          <Card>
            <CardTitle>Customer</CardTitle>
            <CardDivider />
            <LabelSmall $style={{ fontWeight: 400 }}>
              {result.firstname} {result.lastname}
            </LabelSmall>
            <CardCopyData>{result.email}</CardCopyData>
            <LabelSmall marginBottom="scale200">Created At</LabelSmall>
            <LabelSmall $style={{ fontWeight: 400 }}>
              {result.createdAt}
            </LabelSmall>
          </Card>
          <div className={css({ paddingBottom: '12px' })} />
          <Card>
            <CardTitle>Addresses</CardTitle>
            {result.addresses?.map((res) => {
              return (
                <>
                  <CardDivider />
                  <RenderAddress address={res} />
                </>
              );
            })}
          </Card>
        </Cell>
      </Grid>
    </AppWrapper>
  );
}
