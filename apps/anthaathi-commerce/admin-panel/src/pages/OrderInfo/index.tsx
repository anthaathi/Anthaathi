import { useStyletron } from 'baseui';
import { Breadcrumbs } from 'baseui/breadcrumbs';
import { StyledLink } from 'baseui/link';
import React from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import usePromise, { States } from '../../hooks/use-promise';
import { useRestConfig } from '../../hooks/use-rest-config';
import {
  OrdersIdApi,
  SalesDataOrderAddressInterface,
} from '../../__generated__';
import { Cell, Grid } from 'baseui/layout-grid';
import { LabelSmall, ParagraphSmall } from 'baseui/typography';
import { Button, KIND, SIZE } from 'baseui/button';
import { Check, Overflow } from 'baseui/icon';
import { SHAPE } from 'baseui/button-group';
import { Tag } from 'baseui/tag';
import { StatefulPopover } from 'baseui/popover';
import { StatefulMenu } from 'baseui/menu';
import {
  Card,
  CardCopyData,
  CardDivider,
  CardSubheading,
  CardTitle,
  SpaceDivider,
} from '../../features/Card';
import {
  AppWrapper,
  BreadcrumbWrapper,
} from '../../features/BreadcrumbWrapper';
import { RenderAddress } from '../../features/RenderAddress';

export default function OrderInfo() {
  const [css, $theme] = useStyletron();

  const restConfig = useRestConfig();

  const navigate = useNavigate();

  const params = useParams<{ id: string }>();

  const [result, error, state] = usePromise(() => {
    const returnValue = new OrdersIdApi(restConfig);

    return returnValue.getV1OrdersId({
      id: +(params.id || 0),
    });
  }, [restConfig, params.id]);

  const table = result
    ? [
        [
          'Subtotal',
          `${result.items.length} Items`,
          result.subtotal &&
            Intl.NumberFormat('en-US', {
              currency: result.orderCurrencyCode,
              style: 'currency',
            }).format(result.subtotal),
        ],
        [
          'Tax',
          result.customerTaxvat,
          result.taxAmount &&
            Intl.NumberFormat('en-US', {
              currency: result.orderCurrencyCode,
              style: 'currency',
            }).format(result.taxAmount),
        ],
        [
          'Shipping',
          null,
          result.shippingAmount &&
            Intl.NumberFormat('en-US', {
              currency: result.orderCurrencyCode,
              style: 'currency',
            }).format(result.shippingAmount),
        ],
        [
          'Discount',
          result.discountDescription,
          result.discountAmount &&
            Intl.NumberFormat('en-US', {
              currency: result.orderCurrencyCode,
              style: 'currency',
            }).format(result.discountAmount),
        ],
        [
          'Total',
          null,
          result.grandTotal &&
            Intl.NumberFormat('en-US', {
              currency: result.orderCurrencyCode,
              style: 'currency',
            }).format(result.grandTotal ?? 0),
        ],
      ].filter((res) => Boolean(res[2]))
    : [];

  if (state === States.pending) {
    return <>Loading</>;
  }

  return (
    <AppWrapper>
      <BreadcrumbWrapper>
        <Breadcrumbs>
          <StyledLink $as={Link} to="#parent">
            Admin
          </StyledLink>
          <StyledLink $as={Link} to="/order">
            Orders
          </StyledLink>
          <span>#101011</span>
        </Breadcrumbs>

        <span className={css({ flexGrow: 1 })} />

        <StatefulPopover
          content={() => (
            <StatefulMenu
              items={[
                { label: 'Print', id: 'print' },
                { label: 'Change status', id: 'change-status' },
              ]}
              onItemSelect={(item: any) => {
                if (item.item.id === 'print') {
                  navigate(`/order-print/${+(params.id || 0)}`);
                }
              }}
            />
          )}
          returnFocus
          autoFocus
        >
          <Button kind={KIND.secondary} size={SIZE.compact}>
            Actions
          </Button>
        </StatefulPopover>
      </BreadcrumbWrapper>

      <div
        className={css({ maxWidth: '1220px', margin: '0 auto', width: '100%' })}
      >
        <Grid
          gridMaxWidth={0}
          gridMargins={0}
          overrides={{
            GridWrapper: {
              style: { width: '100%', height: '100%' },
            },
          }}
        >
          <Cell span={[12, 12, 8]}>
            <Card>
              <table className={css({ width: '100%' })}>
                <thead>
                  <tr className={css({ borderBottom: '1px solid #EEE' })}>
                    <th>
                      <ParagraphSmall>#</ParagraphSmall>
                    </th>
                    <th align="center">
                      <ParagraphSmall>Status</ParagraphSmall>
                    </th>
                    <th align="left">
                      <ParagraphSmall>Name</ParagraphSmall>
                    </th>
                    <th>
                      <ParagraphSmall>Qty</ParagraphSmall>
                    </th>
                    <th align="right">
                      <ParagraphSmall>Price</ParagraphSmall>
                    </th>
                    <th align="right">
                      <ParagraphSmall>Tax</ParagraphSmall>
                    </th>
                    <th align="right">
                      <ParagraphSmall>Total</ParagraphSmall>
                    </th>
                    <th align="right">
                      <ParagraphSmall>Action</ParagraphSmall>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {result.items.map((res, index) => {
                    return (
                      <tr key={index}>
                        <td align="center">
                          <ParagraphSmall>{index + 1}</ParagraphSmall>
                        </td>
                        <td align="center">
                          <Tag closeable={false} size="small">
                            Pending Pick
                          </Tag>
                        </td>
                        <td align="left">
                          <ParagraphSmall>
                            <StyledLink
                              $as={Link}
                              to={`/product/${res.productId}`}
                            >
                              {res.name}
                            </StyledLink>
                          </ParagraphSmall>
                        </td>
                        <td align="center">
                          <ParagraphSmall>{res.qtyOrdered}</ParagraphSmall>
                        </td>
                        <td align="right">
                          <ParagraphSmall>
                            {Intl.NumberFormat('en-US', {
                              currency: result.orderCurrencyCode,
                              style: 'currency',
                            }).format(res.price ?? 0)}
                          </ParagraphSmall>
                        </td>
                        <td align="right">
                          <ParagraphSmall>
                            {Intl.NumberFormat('en-US', {
                              currency: result.orderCurrencyCode,
                              style: 'currency',
                            }).format(res.taxAmount ?? 0)}
                          </ParagraphSmall>
                        </td>
                        <td align="right">
                          <ParagraphSmall>
                            {Intl.NumberFormat('en-US', {
                              currency: result.orderCurrencyCode,
                              style: 'currency',
                            }).format(res.rowTotal ?? 0)}
                          </ParagraphSmall>
                        </td>
                        <td align="right">
                          <Button size={SIZE.compact} kind={KIND.tertiary}>
                            <Overflow />
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </Card>
            <SpaceDivider />
            <Card>
              <CardTitle>
                <Button
                  shape={SHAPE.circle}
                  kind={KIND.primary}
                  size={SIZE.mini}
                >
                  <Check size={22} />
                </Button>
                &nbsp; &nbsp;
                {result.totalDue ? 'Paid' : 'Payment Pending'}
              </CardTitle>

              <CardDivider />

              <div>
                <table
                  className={css({
                    width: '100%',
                  })}
                >
                  <tbody>
                    {table.map((res1, index2) => {
                      return (
                        <tr key={index2} className={css({ height: '34px' })}>
                          {res1.map((res, index) => {
                            const align: Record<
                              string,
                              | 'left'
                              | 'center'
                              | 'right'
                              | 'justify'
                              | 'char'
                              | undefined
                            > = {
                              [((res1?.length || 0) - 1).toString()]: 'right',
                              '0': 'left',
                            };

                            return (
                              <td
                                align={align[index.toString()] ?? 'center'}
                                key={index}
                              >
                                <LabelSmall $style={{ fontWeight: 400 }}>
                                  {res1[index]}
                                </LabelSmall>
                              </td>
                            );
                          })}
                        </tr>
                      );
                    })}
                  </tbody>
                </table>

                <CardDivider />

                <div className={css({ marginTop: '24px', display: 'flex' })}>
                  <LabelSmall>Paid by customer</LabelSmall>
                  <span className={css({ flexGrow: 1 })} />
                  <LabelSmall>
                    {Intl.NumberFormat('en-US', {
                      currency: result.orderCurrencyCode,
                      style: 'currency',
                    }).format(result.totalPaid ?? 0)}
                  </LabelSmall>
                </div>
              </div>
            </Card>
            <SpaceDivider />
          </Cell>

          <Cell span={[12, 12, 4]}>
            <Card>
              <CardTitle
                action={
                  <StyledLink
                    $style={{
                      cursor: 'pointer',
                      fontSize: $theme.typography.LabelSmall.fontSize,
                    }}
                  >
                    Edit
                  </StyledLink>
                }
              >
                Notes
              </CardTitle>
              <CardDivider />
              <LabelSmall
                paddingTop="scale200"
                $style={{ fontWeight: result.customerNote ? 400 : undefined }}
              >
                {result.customerNote || 'No notes from customer'}
              </LabelSmall>
            </Card>

            <SpaceDivider />

            <Card>
              <CardTitle>Customer</CardTitle>
              <CardDivider />
              <LabelSmall
                paddingTop="scale200"
                $style={{ fontWeight: result.customerNote ? 400 : undefined }}
              >
                <StyledLink
                  $as={result.customerId ? Link : 'span'}
                  {...(result.customerId
                    ? {
                        to: `/customer/${result.customerId}`,
                      }
                    : {})}
                >
                  {result.customerFirstname} {result.customerLastname}
                  {result.customerIsGuest === 1 ? '- Guest User' : ''}
                </StyledLink>
              </LabelSmall>

              <CardDivider />
              <CardSubheading>Information</CardSubheading>

              <div className={css({ paddingBottom: '6px' })} />

              <CardCopyData>
                <StyledLink
                  href={`mailto:${result.customerEmail}`}
                  $style={{
                    fontWeight: 300,
                    fontSize: $theme.typography.LabelSmall.fontSize,
                  }}
                >
                  {result.customerEmail}
                </StyledLink>
              </CardCopyData>
              <CardCopyData>{result.billingAddress?.telephone}</CardCopyData>

              <CardDivider />

              <CardSubheading>Shipping Address</CardSubheading>

              <div>
                {result.billingAddress && (
                  <RenderAddress
                    address={
                      result.billingAddress as SalesDataOrderAddressInterface
                    }
                  />
                )}
              </div>
            </Card>
          </Cell>
        </Grid>
      </div>
    </AppWrapper>
  );
}
