import {
  ApiResponse,
  GetV1OrdersRequest,
  InitOverrideFunction,
  OrdersApi,
  SalesDataOrderSearchResultInterface,
  SalesDataOrderSearchResultInterfaceFromJSON,
} from '../__generated__';
import * as runtime from '../__generated__/runtime';

export type ConditionType =
  | 'eq'
  | 'finset'
  | 'from'
  | 'gt'
  | 'gteq'
  | 'in'
  | 'like'
  | 'lt'
  | 'lteq'
  | 'moreq'
  | 'neq'
  | 'nfinset'
  | 'nin'
  | 'nlike'
  | 'notnull'
  | 'null'
  | 'to';

export type FilterGroup = {
  field: string;
  value: string;
  conditionType: ConditionType;
};

export class OrdersAPIsExtended extends OrdersApi {
  async getV1Orders(
    requestParameters: Omit<
      GetV1OrdersRequest,
      | 'searchCriteriaFilterGroups0Filters0Field'
      | 'searchCriteriaFilterGroups0Filters0Value'
      | 'searchCriteriaFilterGroups0Filters0ConditionType'
      | 'searchCriteriaSortOrders0Field'
      | 'searchCriteriaSortOrders0Direction'
    > & {
      filterGroups?: FilterGroup[][];
      sort?: {
        field: string;
        direction: 'ASC' | 'DESC';
      }[];
    },
    initOverrides?: RequestInit | InitOverrideFunction
  ): Promise<SalesDataOrderSearchResultInterface> {
    const queryParameters: any = {};

    requestParameters.filterGroups?.forEach((item, index) => {
      item.forEach((item2, index2) => {
        queryParameters[
          `searchCriteria[filterGroups][${index}][filters][${index2}][field]`
        ] = item2.field;
        queryParameters[
          `searchCriteria[filterGroups][${index}][filters][${index2}][value]`
        ] = item2.value;
        queryParameters[
          `searchCriteria[filterGroups][${index}][filters][${index2}][conditionType]`
        ] = item2.conditionType;
      });
    });

    requestParameters.sort?.forEach((item, index) => {
      queryParameters[`searchCriteria[sortOrders][${index}][field]`] =
        item.field;
      queryParameters[`searchCriteria[sortOrders][${index}][direction]`] =
        item.direction;
    });

    if (requestParameters.searchCriteriaPageSize !== undefined) {
      queryParameters['searchCriteria[pageSize]'] =
        requestParameters.searchCriteriaPageSize;
    }

    if (requestParameters.searchCriteriaCurrentPage !== undefined) {
      queryParameters['searchCriteria[currentPage]'] =
        requestParameters.searchCriteriaCurrentPage;
    }

    const headerParameters: runtime.HTTPHeaders = {};

    const response = await this.request(
      {
        path: `/V1/orders`,
        method: 'GET',
        headers: headerParameters,
        query: queryParameters,
      },
      initOverrides
    );

    return new runtime.JSONApiResponse(response, (jsonValue) =>
      SalesDataOrderSearchResultInterfaceFromJSON(jsonValue)
    ).value();
  }
}
