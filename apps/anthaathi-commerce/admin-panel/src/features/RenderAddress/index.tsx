import { ParagraphSmall } from 'baseui/typography';
import React from 'react';
import {
  CustomerDataAddressInterface,
  SalesDataOrderAddressInterface,
} from '../../__generated__';

export const RenderAddress = ({
  address,
}: {
  address: CustomerDataAddressInterface | SalesDataOrderAddressInterface;
}) => {
  return (
    <>
      <ParagraphSmall marginTop="scale400" marginBottom={0}>
        {address?.street}
      </ParagraphSmall>
      <ParagraphSmall marginTop={0} marginBottom={0}>
        {address?.city}
      </ParagraphSmall>
      <ParagraphSmall marginTop={0} marginBottom={0}>
        {(address?.region as CustomerDataAddressInterface).region?.region ??
          (address?.region as SalesDataOrderAddressInterface).region}
      </ParagraphSmall>
      <ParagraphSmall marginTop={0} marginBottom={0}>
        {address?.postcode}
      </ParagraphSmall>
    </>
  );
};
