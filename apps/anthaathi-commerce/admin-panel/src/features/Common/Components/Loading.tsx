import { Spinner } from 'baseui/spinner';
import { SIZE } from 'baseui/input';
import React from 'react';
import { useStyletron } from 'baseui';

export const DataTableLoading = () => {
  const [css, $theme] = useStyletron();

  return (
    <span
      className={css({
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
      })}
    >
      <Spinner $size={SIZE.large} />
      <span
        className={css({
          ...$theme.typography.HeadingXSmall,
          paddingBlockStart: $theme.sizing.scale650,
          paddingBlockEnd: $theme.sizing.scale500,
        })}
      >
        Loading...
      </span>
    </span>
  );
};
