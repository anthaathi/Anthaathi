import { styled, useStyletron } from 'baseui';
import React from 'react';
import { LabelMedium, LabelSmall } from 'baseui/typography';
import { Button, KIND, SIZE } from 'baseui/button';

export const CardCopyData = ({ children }: { children?: React.ReactNode }) => {
  const [css, $theme] = useStyletron();
  return (
    <div
      className={css({ display: 'flex', paddingTop: $theme.sizing.scale400 })}
    >
      <LabelMedium
        $style={{
          fontWeight: 300,
          fontSize: $theme.typography.LabelSmall.fontSize,
        }}
      >
        {children}
      </LabelMedium>
      <span className={css({ flexGrow: 1 })} />
      <Button size={SIZE.mini} kind={KIND.secondary}>
        Copy
      </Button>
    </div>
  );
};

export const CardDivider = () => {
  const [css, $theme] = useStyletron();

  return (
    <div
      className={css({
        width: `calc(100% + ${$theme.sizing.scale800} + ${$theme.sizing.scale800})`,
        marginLeft: `-${$theme.sizing.scale800}`,
        marginRight: `-${$theme.sizing.scale800}`,
        borderBottom: '1px solid #EEE',
        marginBottom: $theme.sizing.scale600,
        marginTop: $theme.sizing.scale600,
      })}
    />
  );
};

export const CardSubheading = ({
  children,
}: {
  children?: React.ReactNode;
}) => {
  return <LabelSmall $style={{ fontWeight: 400 }}>{children}</LabelSmall>;
};

export const CardTitle = ({
  action,
  children,
}: {
  action?: React.ReactNode;
  children: React.ReactNode;
}) => {
  const [css, $theme] = useStyletron();

  return (
    <div
      className={css({
        display: 'flex',
        paddingRight: $theme.sizing.scale200,
      })}
    >
      <LabelMedium
        marginTop={0}
        marginBottom={0}
        $style={{ display: 'flex', alignItems: 'center' }}
      >
        {children}
      </LabelMedium>

      <span className={css({ flexGrow: 1 })} />

      {action}
    </div>
  );
};

export const Card = styled('div', ({ $theme }) => ({
  boxShadow: $theme.lighting.shadow400,
  borderRadius: '10px',
  paddingLeft: $theme.sizing.scale800,
  paddingRight: $theme.sizing.scale800,
  paddingTop: $theme.sizing.scale800,
  paddingBottom: $theme.sizing.scale800,
}));

export const SpaceDivider = styled('div', ({ $theme }) => ({
  marginBottom: $theme.sizing.scale800,
}));
