import { CategoricalColumn, StringColumn } from 'baseui/data-table';
import { ColumnOptions } from 'baseui/data-table/types';

export type FilterPageColumn = {
  column: StringColumn | CategoricalColumn | ColumnOptions;
  authority?: string[];
  key?: string | string[];
  filterConfig?: false | {};
  filterOrder?: number;
  enums?: string[];
};
