import { Cell, Grid } from 'baseui/layout-grid';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { Button } from 'baseui/button';
import { Formik } from 'formik';
import React, { useMemo } from 'react';
import { Accordion, Panel } from 'baseui/accordion';
import { useStyletron } from 'baseui';
import { Datepicker } from 'baseui/datepicker';
import { Slider } from 'baseui/slider';
import { Select } from 'baseui/select';
import { FilterPageColumn } from '../types/FilterPageColumn';

export interface FilterBuilderProps {
  columns: FilterPageColumn[];
  onFilter: (filters: Record<string, any | any[]>) => void;
}

const Component = ({
  res,
  values,
  handleChange,
  key,
}: {
  res: FilterPageColumn;
  values: Record<string, any>;
  handleChange: {
    (e: React.ChangeEvent<any>): void;
    <T_1 = string | React.ChangeEvent<any>>(
      field: T_1
    ): T_1 extends React.ChangeEvent<any>
      ? void
      : (e: string | React.ChangeEvent<any>) => void;
  };
  key: string;
}) => {
  if (res.enums) {
    return (
      <Select
        options={res.enums.map((res) => ({
          id: res,
          label: res,
        }))}
        value={[{ id: values[res.key as string] }]}
        onChange={(e) => {
          handleChange({
            target: {
              name: res.key,
              value: e.option?.id,
            },
          });
        }}
      />
    );
  }

  switch (res.column.kind) {
    case 'NUMERICAL':
      return (
        <Slider
          value={values[res.key as string] || [0, 10_000]}
          max={10_000}
          onChange={({ value }) =>
            value &&
            handleChange({
              target: { name: res.key, value },
            })
          }
          onFinalChange={({ value }) =>
            handleChange({
              target: { name: res.key, value },
            })
          }
        />
      );
    case 'DATETIME':
      return (
        <Datepicker
          value={values[res.key as string] || [new Date()]}
          onChange={({ date }) => {
            handleChange({
              target: {
                name: res.key,
                value: Array.isArray(date) ? date : [date],
              },
            });
          }}
          range={true}
        />
      );
    default:
      return (
        <Input
          id={key + '_filter'}
          name={res.key as string}
          value={values[res.key as string]}
          onChange={(e) => {
            handleChange({
              target: {
                name: res.key,
                value: e.target.value,
              },
            });
          }}
        />
      );
  }
};

export function FilterBuilder({ columns, onFilter }: FilterBuilderProps) {
  const [css, $theme] = useStyletron();

  const renderColumn = useMemo(() => {
    return columns
      .filter((res) => res.filterConfig !== false)
      .sort((v1, v2) => (v1?.filterOrder ?? 9999) - (v2.filterOrder ?? 9999))
      .filter((res) => typeof res.key === 'string');
  }, [columns]);

  return (
    <div
      className={css({
        marginBottom: '1rem',
        borderRadius: '10px',
        boxShadow: $theme.lighting.shadow400,
      })}
    >
      <Formik
        initialValues={{} as Record<string, any>}
        onSubmit={(value) => {
          onFilter(value);
        }}
      >
        {({ handleSubmit, handleChange, values }) => (
          <Accordion overrides={{ Root: { style: { borderRadius: '10px' } } }}>
            <Panel
              title="Filters"
              overrides={{
                PanelContainer: { style: { borderRadius: '10px' } },
                Content: { style: { borderRadius: '10px' } },
                Header: { style: { borderRadius: '10px' } },
              }}
            >
              <form onSubmit={handleSubmit}>
                <Grid gridMaxWidth={0} gridMargins={0}>
                  {renderColumn.map((res, index) => {
                    const key = res.column.title + index;

                    return (
                      <Cell span={[12, 4, 3, 3]} key={key}>
                        <FormControl
                          label={res.column.title}
                          htmlFor={key + '_filter'}
                        >
                          <div>
                            <Component
                              res={res}
                              values={values}
                              handleChange={handleChange}
                              key={key + '_filter'}
                            />
                          </div>
                        </FormControl>
                      </Cell>
                    );
                  })}

                  <Cell span={2}>
                    <FormControl label={<>.</>}>
                      <Button type="submit">Search</Button>
                    </FormControl>
                  </Cell>
                </Grid>
              </form>
            </Panel>
          </Accordion>
        )}
      </Formik>
    </div>
  );
}
