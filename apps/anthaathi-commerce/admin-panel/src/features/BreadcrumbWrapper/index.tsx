import { styled } from 'baseui';

export const BreadcrumbWrapper = styled('div', ({ $theme }) => ({
  maxWidth: '1220px',
  margin: '0 auto',
  width: '100%',
  display: 'flex',
  alignItems: 'center',
  paddingTop: $theme.sizing.scale800,
  paddingBottom: $theme.sizing.scale800,
}));

export const AppWrapper = styled('div', {
  margin: '0 auto',
  maxWidth: '1220px',
  paddingInlineStart: '64px',
  paddingInlineEnd: '64px',
  height: '100%',
  display: 'flex',
  flexDirection: 'column',
});
