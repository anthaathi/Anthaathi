package org.anthaathi.commerce.product.entity

import java.io.Serializable
import java.time.OffsetDateTime
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "location")
open class Location : Serializable {
    @Id
    @Column(name = "id", nullable = false)
    open var id: UUID? = null

    @Column(name = "status", nullable = false)
    open var status: String? = null

    @Column(name = "sort")
    open var sort: Int? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_created")
    open var userCreated: DirectusUser? = null

    @Column(name = "date_created")
    open var dateCreated: OffsetDateTime? = null

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_updated")
    open var userUpdated: DirectusUser? = null

    @Column(name = "date_updated")
    open var dateUpdated: OffsetDateTime? = null

    @Column(name = "title", nullable = false)
    open var title: String? = null

    @Column(name = "fulfill_online_orders")
    open var fulfillOnlineOrders: Boolean? = null

    @OneToMany(mappedBy = "location")
    open var productVariantStocks: MutableSet<ProductVariantStock> = mutableSetOf()

    companion object {
        private const val serialVersionUID = -1844705367037807700L
    }

    //TODO [JPA Buddy] generate columns from DB
}
