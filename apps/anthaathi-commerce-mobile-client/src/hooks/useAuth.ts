import {gql, useLazyQuery, useMutation} from '@apollo/client';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {atom, useRecoilState} from 'recoil';

import {useCart} from './useCart';

export interface UserProps {
  email?: string;
  firstname?: string;
  lastname?: string;
  token?: string;
}

export const userDataRecoil = atom<UserProps | null>({
  key: 'userDataRecoil',
  default: null,
});

const signInQuery = gql`
  mutation SignIn($email: String!, $password: String!) {
    generateCustomerToken(email: $email, password: $password) {
      token
      __typename
    }
  }
`;

const signOutQuery = gql`
  mutation {
    revokeCustomerToken {
      result
    }
  }
`;

const getCustomerQuery = gql`
  query getCustomerData {
    customer {
      email
      firstname
      lastname
    }
  }
`;

const createAccountQuery = gql`
  mutation CreateAccount(
    $email: String!
    $firstname: String!
    $lastname: String!
    $password: String!
    $is_subscribed: Boolean!
  ) {
    createCustomerV2(
      input: {
        email: $email
        firstname: $firstname
        lastname: $lastname
        password: $password
        is_subscribed: $is_subscribed
      }
    ) {
      customer {
        email
        firstname
        lastname
      }
      __typename
    }
  }
`;

const resetPasswordQuery = gql`
  mutation resetPasswordQuery($email: String!) {
    requestPasswordResetEmail(email: $email)
  }
`;

export const useAuth = () => {
  const [user, setUser] = useRecoilState(userDataRecoil);
  const [getCustomer] = useLazyQuery(getCustomerQuery);
  const [signIn] = useMutation(signInQuery);
  const [createAccount] = useMutation(createAccountQuery);
  const [resetPasswordLink] = useMutation(resetPasswordQuery);
  const [signOut] = useMutation(signOutQuery);

  const {createEmptyCartAuthAndMergeCart} = useCart();

  async function getToken() {
    const userToken = await AsyncStorage.getItem('userToken');

    if (userToken === null) {
      return null;
    } else {
      return userToken;
    }
  }

  async function signInUser(
    email: string,
    password: string,
    called: boolean = true, // for create account
  ) {
    try {
      const userData: any = await signIn({
        variables: {
          email: email,
          password: password,
        },
      });
      setUser({...user, token: userData.data.generateCustomerToken.token});
      AsyncStorage.setItem(
        'userToken',
        userData.data.generateCustomerToken.token,
      );
      await createEmptyCartAuthAndMergeCart();
      if (called) {
        await getCustomerDetails();
      }
      return userData.data;
    } catch (e: any) {
      return e.message;
    }
  }

  async function createUserAccount(
    email: string,
    password: string,
    firstname: string = '',
    lastname: string = '',
    is_subscribed: boolean = false,
  ) {
    try {
      const userData: any = await createAccount({
        variables: {
          email: email,
          password: password,
          firstname: firstname,
          lastname: lastname,
          is_subscribed: is_subscribed,
        },
      });
      setUser({
        ...user,
        email: userData.data.createCustomerV2.customer.email,
        firstname: userData.data.createCustomerV2.customer.firstname,
        lastname: userData.data.createCustomerV2.customer.lastname,
      });
      await signInUser(email, password, false);
      return userData.data;
    } catch (e: any) {
      return e.message;
    }
  }

  async function getCustomerDetails() {
    const token = await getToken();
    try {
      if (token !== null) {
        const customerData: any = await getCustomer();
        setUser({
          ...user,
          token: token,
          email: customerData?.data.customer.email,
          firstname: customerData?.data.customer.firstname,
          lastname: customerData?.data.customer.lastname,
        });

        return customerData;
      }
    } catch (e: any) {
      console.log(e);
      return e.message;
    }
  }

  async function resetPassword(email: string) {
    try {
      const data: any = await resetPasswordLink({
        variables: {
          email: email,
        },
      });
      return data.data;
    } catch (e: any) {
      return e.message;
    }
  }

  async function signOutUser() {
    try {
      const data: any = await signOut();
      return data.data;
    } catch (e: any) {
      return e.message;
    }
  }

  return {
    getToken,
    signInUser,
    signOutUser,
    createUserAccount,
    getCustomerDetails,
    resetPassword,
  };
};
