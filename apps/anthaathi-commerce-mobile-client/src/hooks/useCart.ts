/* eslint-disable react-hooks/exhaustive-deps */
import {atom, useRecoilState} from 'recoil';
import {gql, useLazyQuery, useMutation} from '@apollo/client';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useEffect, useState} from 'react';

export const CartItemData = atom<any[]>({
  key: 'cartProduct',
  default: [],
});

const addProductToCartQuery = gql`
  mutation addSimpleProductsToCartQ(
    $cartId: String!
    $sku: String!
    $quantity: Float!
  ) {
    addSimpleProductsToCart(
      input: {
        cart_id: $cartId
        cart_items: [{data: {quantity: $quantity, sku: $sku}}]
      }
    ) {
      cart {
        items {
          id
          uid
          product {
            id: uid
            name
            sku
            title: meta_title
            description: meta_description
            url_key
            price: price_range {
              minimum_price {
                discount {
                  amount_off
                  percent_off
                }
                regular_price {
                  currency
                  value
                }
                final_price {
                  value
                  currency
                }
              }
            }
            image {
              url
            }
          }
          quantity
        }
      }
    }
  }
`;

const updateCartItemQujery = gql`
  mutation ($cartId: String!, $cartItemId: ID!, $quantity: Float!) {
    updateCartItems(
      input: {
        cart_id: $cartId
        cart_items: [{quantity: $quantity, cart_item_uid: $cartItemId}]
      }
    ) {
      cart {
        items {
          id
          uid
          product {
            id: uid
            name
            sku
            title: meta_title
            description: meta_description
            url_key
            price: price_range {
              minimum_price {
                discount {
                  amount_off
                  percent_off
                }
                regular_price {
                  currency
                  value
                }
                final_price {
                  value
                  currency
                }
              }
            }
            image {
              url
            }
          }
          quantity
        }
      }
    }
  }
`;

const createEmptyCartQuery = gql`
  mutation {
    createEmptyCart
  }
`;

const mergeCartQuery = gql`
  mutation MergeCartsAfterSignIn(
    $sourceCartId: String!
    $destinationCartId: String!
  ) {
    mergeCarts(
      source_cart_id: $sourceCartId
      destination_cart_id: $destinationCartId
    ) {
      id
      items {
        id
        uid
        product {
          id: uid
          name
          sku
          title: meta_title
          description: meta_description
          url_key
          price: price_range {
            minimum_price {
              discount {
                amount_off
                percent_off
              }
              regular_price {
                currency
                value
              }
              final_price {
                value
                currency
              }
            }
          }
          image {
            url
          }
        }
        quantity
      }
    }
  }
`;

export const getCartItemQuery = gql`
  query getCartItem($cartId: String!) {
    cart(cart_id: $cartId) {
      items {
        id
        uid
        product {
          id: uid
          name
          sku
          title: meta_title
          description: meta_description
          url_key
          price: price_range {
            minimum_price {
              discount {
                amount_off
                percent_off
              }
              regular_price {
                currency
                value
              }
              final_price {
                value
                currency
              }
            }
          }
          image {
            url
          }
        }
        quantity
      }
    }
  }
`;

export const useCart = () => {
  const [cart, setCart] = useRecoilState(CartItemData);
  const [addProduct, {loading: addLoading}] = useMutation(
    addProductToCartQuery,
  );
  const [updateProduct, {loading: updateLoading}] =
    useMutation(updateCartItemQujery);
  const [createEmptyCart] = useMutation(createEmptyCartQuery);
  const [mergeCart] = useMutation(mergeCartQuery);
  const [loadCartItem, {data, loading}] = useLazyQuery(getCartItemQuery);
  const [loader, setLoader] = useState<boolean>(false);

  useEffect(() => {
    if (!loading && data) {
      setCart(data?.cart?.items);
    }
  }, [data, loading]);

  async function getCartId() {
    const cartId = await AsyncStorage.getItem('cartId');

    if (cartId === null) {
      const id = await createEmptyCart();
      AsyncStorage.setItem('cartId', id.data.createEmptyCart);
      console.log(id.data.createEmptyCart, 'newwwwwwwww');
      return id.data.createEmptyCart;
    }

    return cartId;
  }

  async function addProductToCart(sku: string, quantity: number) {
    if (!addLoading && !updateLoading && !loader) {
      setLoader(true);
      const cartId = await getCartId();
      try {
        const addedData: any = await addProduct({
          variables: {
            cartId: cartId,
            sku: sku,
            quantity: quantity,
          },
        });

        setCart(addedData.data.addSimpleProductsToCart.cart.items);
        setLoader(false);
        return addedData;
      } catch (e: any) {
        if (
          e.message.includes(
            'The current user cannot perform operations on cart',
          )
        ) {
          const id = await createEmptyCart();
          AsyncStorage.setItem('cartId', id.data.createEmptyCart);
        }
        setLoader(false);
        return null;
      }
    }
  }
  async function updateProductToCart(
    cartItemId: string,
    currentQuantity: number,
  ) {
    if (!addLoading && !updateLoading && !loader) {
      setLoader(true);
      const cartId = await getCartId();
      try {
        const updatedData: any = await updateProduct({
          variables: {
            cartId: cartId,
            cartItemId: cartItemId,
            quantity: currentQuantity - 1,
          },
        });

        setCart(updatedData.data.updateCartItems.cart.items);
        setLoader(false);
        return updatedData;
      } catch (e) {
        setLoader(false);
        return null;
      }
    }
  }

  async function getCartItem() {
    const cartId = await getCartId();

    return await loadCartItem({
      variables: {
        cartId: cartId,
      },
    });
  }

  async function createEmptyCartAuthAndMergeCart() {
    const cartId = await AsyncStorage.getItem('cartId');

    if (cartId === null) {
      const id = await createEmptyCart();
      AsyncStorage.setItem('cartId', id.data.createEmptyCart);
      return id.data.createEmptyCart;
    } else {
      const id = await createEmptyCart();

      const mergeData = await mergeCart({
        variables: {
          sourceCartId: cartId,
          destinationCartId: id.data.createEmptyCart,
        },
      });
      setCart(mergeData?.data.mergeCarts?.items);
      AsyncStorage.setItem('cartId', id.data.createEmptyCart);
      return mergeData;
    }
  }

  return {
    addProductToCart,
    updateProductToCart,
    getCartItem,
    getCartId,
    createEmptyCartAuthAndMergeCart,
  };
};
