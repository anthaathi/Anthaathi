import {
  View,
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import React, {useState} from 'react';
import {Button, TextInput, useTheme} from 'react-native-paper';
import {RootStackParamList} from '../../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {useIntl} from 'react-intl';
import {useFormik} from 'formik';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import * as Yup from 'yup';
import {useAuth} from '../../../hooks/useAuth';

const ResetPasswordPage = (
  props: NativeStackScreenProps<RootStackParamList, 'ResetPassword'>,
) => {
  const theme = useTheme();
  const intl = useIntl();
  const [buttonLoading, setButtonLoading] = useState<boolean>(false);
  const {resetPassword} = useAuth();
  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .required('Email or username is required.')
      .email('email is invalid')
      .min(3, 'Must be more than 3 characters.'),
  });

  const formik = useFormik({
    initialValues: {
      email: '',
    },
    validationSchema,
    onSubmit: async values => {
      setButtonLoading(true);
      const data = await resetPassword(values.email);

      if (data && data.requestPasswordResetEmail) {
        props.navigation.navigate('SignIn');
        Toast.show({
          text1: 'Success!',
          type: 'success',
          autoHide: true,
          visibilityTime: 5000,
          position: 'bottom',
        });
      } else {
        Toast.show({
          text1: 'Failed!',
          type: 'error',
          autoHide: true,
          visibilityTime: 5000,
          position: 'bottom',
        });
      }
      setButtonLoading(false);
    },
  });

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled
      style={{flex: 1}}>
      <View
        style={{
          backgroundColor: '#fff',
          flex: 1,
        }}>
        <ScrollView style={{paddingHorizontal: 25}}>
          <View
            style={{
              paddingTop: '45%',
              marginBottom: 20,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                color: theme.colors.primary,
                fontWeight: '700',
                fontSize: 28,
              }}>
              {intl.formatMessage({defaultMessage: 'Reset your password'})}
            </Text>
          </View>
          <View style={{marginVertical: 15}}>
            <TextInput
              mode="flat"
              label={intl.formatMessage({
                defaultMessage: 'Username or email address',
              })}
              style={{
                backgroundColor: '#fff',
                fontSize: 14,
                height: 56,
                marginHorizontal: 5,
                marginVertical: 10,
              }}
              activeUnderlineColor={theme.colors.primary}
              onChangeText={formik.handleChange('email')}
              value={formik.values.email}
              onBlur={formik.handleBlur('email')}
            />
            {formik.touched.email && formik.errors.email && (
              <Text
                style={{fontSize: 12, color: '#FF0D10', marginHorizontal: 5}}>
                {formik.errors.email}
              </Text>
            )}
          </View>

          <Button
            mode="contained"
            loading={buttonLoading}
            uppercase={false}
            style={{
              marginVertical: 15,
              marginHorizontal: 30,
              borderRadius: 32,
            }}
            labelStyle={{paddingVertical: 5}}
            onPress={() => {
              if (!buttonLoading) {
                formik.handleSubmit();
              }
            }}>
            {intl.formatMessage({defaultMessage: 'Get password link'})}
          </Button>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

export default ResetPasswordPage;
