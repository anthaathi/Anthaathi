import {
  Image,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import React from 'react';
import {Button, Text, TextInput, useTheme} from 'react-native-paper';
import {RootStackParamList} from '../../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {useIntl} from 'react-intl';
import CheckBox from '../../../features/CMS/containers/Core/components/CheckBox';
import {useFormik} from 'formik';
import * as Yup from 'yup';
import {useAuth} from '../../../hooks/useAuth';
import {Toast} from 'react-native-toast-message/lib/src/Toast';
import AsyncStorage from '@react-native-async-storage/async-storage';

const SignUpPage = (
  props: NativeStackScreenProps<RootStackParamList, 'SignUp'>,
) => {
  const theme = useTheme();
  const intl = useIntl();
  const [passwordShow, setPasswordShow] = React.useState(true);
  const [checked, setChecked] = React.useState(false);
  const [newsLetterSub, setNewsLetterSub] = React.useState(true);
  const [buttonLoading, setButtonLoading] = React.useState(false);
  const {createUserAccount} = useAuth();

  const validationSchema = Yup.object().shape({
    email: Yup.string()
      .required('Email or username is required.')
      .email('email is invalid'),
    password: Yup.string()
      .required('Password is required.')
      .min(8, 'Passowrd must contain atleast 8 characters')
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
        'Must Contain 8 Characters, One Uppercase, One Lowercase, One Number and one special case Character',
      ),
    firstName: Yup.string()
      .required("First name can't be empty")
      .min(3, 'First name must have more than 3 characters'),
    lastName: Yup.string()
      .required("Last name can't be empty")
      .min(3, 'Last name must have more than 3 characters'),
  });

  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      firstName: '',
      lastName: '',
    },
    validationSchema,
    onSubmit: async values => {
      setButtonLoading(true);
      console.log(values);
      const data = await createUserAccount(
        values.email,
        values.password,
        values.firstName,
        values.lastName,
        newsLetterSub,
      );
      console.log(data);

      if (data && data.createCustomerV2) {
        props.navigation.navigate('Dashboard');
        console.log(data);
        Toast.show({
          text1: 'Loged in Successfully!',
          type: 'success',
          autoHide: true,
          visibilityTime: 5000,
          position: 'bottom',
        });
      } else {
        console.log('Failed');
        Toast.show({
          text1: 'Something went wrong!',
          text2: data,
          type: 'error',
          autoHide: true,
          visibilityTime: 5000,
          position: 'bottom',
        });
      }
      setButtonLoading(false);
    },
  });

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      enabled
      style={{flex: 1}}>
      <View
        style={{
          backgroundColor: '#fff',
          flex: 1,
        }}>
        <ScrollView style={{paddingHorizontal: 25}}>
          <View
            style={{
              paddingTop: '30%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image
              style={{
                width: 138,
                height: 128,
                zIndex: 999,
              }}
              source={{
                uri: 'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/logo-oxvdmbxi6g2vpdrt9kcwy3xyhpvajr03in9rykvzfk_220x.png?v=1653569545',
              }}
              resizeMode="contain"
            />
          </View>
          <View style={{marginVertical: 15}}>
            <TextInput
              mode="flat"
              label={'First Name'}
              style={{
                backgroundColor: '#fff',
                fontSize: 14,
                height: 56,
                marginHorizontal: 5,
                marginVertical: 10,
              }}
              activeUnderlineColor={theme.colors.primary}
              onChangeText={formik.handleChange('firstName')}
              value={formik.values.firstName}
              onBlur={formik.handleBlur('firstName')}
            />
            {formik.touched.firstName && formik.errors.firstName && (
              <Text
                style={{fontSize: 12, color: '#FF0D10', marginHorizontal: 5}}>
                {formik.errors.firstName}
              </Text>
            )}
            <TextInput
              mode="flat"
              label={'Last Name'}
              style={{
                backgroundColor: '#fff',
                fontSize: 14,
                height: 56,
                marginHorizontal: 5,
                marginVertical: 10,
              }}
              activeUnderlineColor={theme.colors.primary}
              onChangeText={formik.handleChange('lastName')}
              value={formik.values.lastName}
              onBlur={formik.handleBlur('lastName')}
            />
            {formik.touched.lastName && formik.errors.lastName && (
              <Text
                style={{fontSize: 12, color: '#FF0D10', marginHorizontal: 5}}>
                {formik.errors.lastName}
              </Text>
            )}
            <TextInput
              mode="flat"
              label={intl.formatMessage({defaultMessage: 'Email address'})}
              style={{
                backgroundColor: '#fff',
                fontSize: 14,
                height: 56,
                marginHorizontal: 5,
                marginVertical: 10,
              }}
              activeUnderlineColor={theme.colors.primary}
              onChangeText={formik.handleChange('email')}
              value={formik.values.email}
              onBlur={formik.handleBlur('email')}
            />
            {formik.touched.email && formik.errors.email && (
              <Text
                style={{fontSize: 12, color: '#FF0D10', marginHorizontal: 5}}>
                {formik.errors.email}
              </Text>
            )}
            <TextInput
              mode="flat"
              label={intl.formatMessage({defaultMessage: 'Password'})}
              style={{
                backgroundColor: '#fff',
                fontSize: 14,
                height: 56,
                marginHorizontal: 5,
                marginVertical: 10,
              }}
              activeUnderlineColor={theme.colors.primary}
              secureTextEntry={passwordShow}
              right={
                <TextInput.Icon
                  icon={passwordShow ? 'eye' : 'eye-off'}
                  onPress={() => {
                    setPasswordShow(!passwordShow);
                  }}
                />
              }
              onChangeText={formik.handleChange('password')}
              value={formik.values.password}
              onBlur={formik.handleBlur('password')}
            />
            {formik.touched.password && formik.errors.password && (
              <Text
                style={{fontSize: 12, color: '#FF0D10', marginHorizontal: 5}}>
                {formik.errors.password}
              </Text>
            )}
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <CheckBox
                color={theme.colors.primary}
                status={newsLetterSub}
                onPress={() => {
                  setNewsLetterSub(!newsLetterSub);
                }}
              />
              <Text style={{fontWeight: '500', fontSize: 14}}>
                Subscribe to Newsletter
              </Text>
            </View>
          </View>

          {/* <View
            style={{
              marginVertical: 10,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <CheckBox
              color={theme.colors.primary}
              status={checked}
              onPress={() => {
                setChecked(!checked);
              }}
            />
            <Text style={{fontWeight: '500', fontSize: 14}}>
              {intl.formatMessage({defaultMessage: 'I agree with'}) + ' '}
            </Text>
            <TouchableOpacity onPress={() => console.log('Pressed')}>
              <Text
                style={{
                  color: theme.colors.primary,
                  fontWeight: '700',
                  fontSize: 13,
                  textDecorationLine: 'underline',
                }}>
                {intl.formatMessage({defaultMessage: 'Terms and Conditions'})}
              </Text>
            </TouchableOpacity>
          </View> */}

          <Button
            mode="contained"
            uppercase={false}
            loading={buttonLoading}
            style={{
              marginVertical: 15,
              marginHorizontal: 30,
              borderRadius: 32,
            }}
            labelStyle={{paddingVertical: 5}}
            onPress={formik.handleSubmit}>
            {intl.formatMessage({defaultMessage: 'Create an account'})}
          </Button>

          <View
            style={{
              marginTop: 30,
              paddingBottom: '10%',
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text style={{fontWeight: '500', fontSize: 14, marginRight: 10}}>
              {intl.formatMessage({defaultMessage: 'OR'})}
            </Text>
            <TouchableOpacity
              onPress={() => {
                props.navigation.navigate('SignIn');
              }}>
              <Text
                style={{
                  color: theme.colors.primary,
                  fontWeight: '700',
                  fontSize: 13,
                }}>
                {intl.formatMessage({defaultMessage: 'Login to your account'})}
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </KeyboardAvoidingView>
  );
};

export default SignUpPage;
