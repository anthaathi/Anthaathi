import {ScrollView, View} from 'react-native';
import React, {useEffect, useMemo, useState} from 'react';
import CMSRenderer from '../../features/CMS';
import {HomePageComponentType} from '../../features/CMS/types/common';
import dataJson from '../../config/data.json';
import {RootStackParamList} from '../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {ProductProps} from '../../features/CMS/containers/HomePage/components/FeaturedCollection';
import Header from '../../features/CMS/containers/Header';
import {gql, useQuery} from '@apollo/client';
import {ActivityIndicator, useTheme} from 'react-native-paper';

const getProductQuery = gql`
  query getProductByHandle($handle: String!) {
    products(filter: {url_key: {eq: $handle}}) {
      items {
        id
        name
        description: meta_description
        sku
        url_key
        stock_status
        image {
          url
        }
        small_image {
          url
        }
        price_range {
          minimum_price {
            regular_price {
              value
              currency
            }
          }
        }
      }
    }
  }
`;

const getProductList = gql`
  query getCategoryByHandle($handle: String!, $currentPage: Int) {
    categories(filters: {url_key: {eq: $handle}}) {
      items {
        ... on CategoryTree {
          meta_title
          products(currentPage: $currentPage, pageSize: 6) {
            items {
              id: uid
              name
              sku
              title: meta_title
              description: meta_description
              url_key
              price: price_range {
                minimum_price {
                  discount {
                    amount_off
                    percent_off
                  }
                  regular_price {
                    currency
                    value
                  }
                  final_price {
                    value
                    currency
                  }
                }
              }
              image {
                url
              }
            }
            page_info {
              page_size
              current_page
              total_pages
            }
            total_count
          }
        }
      }
    }
  }
`;

const ProductPage = (
  props: NativeStackScreenProps<RootStackParamList, 'ProductPage'>,
) => {
  const theme = useTheme();
  const {loading: listLoading, data: listData} = useQuery(getProductList, {
    variables: {handle: 'fruits', currentPage: 1},
  });
  const {loading: listLoading1, data: listData1} = useQuery(getProductList, {
    variables: {handle: 'vegetables', currentPage: 1},
  });

  const {loading, data} = useQuery(getProductQuery, {
    variables: {handle: props.route.params?.handle},
  });

  const productList: any[] = useMemo(() => {
    return listLoading ? [] : listData?.categories?.items[0].products.items;
  }, [listData, listLoading]);

  const productList1: any[] = useMemo(() => {
    return listLoading1 ? [] : listData1?.categories?.items[0].products.items;
  }, [listData1, listLoading1]);

  const product = React.useMemo(() => {
    return loading ? null : data?.products?.items[0];
  }, [data, loading]);

  if (loading) {
    return (
      <View style={{flex: 1}}>
        <Header
          leftIcon={'arrow-left'}
          leftOnPress={() => props.navigation.goBack()}
          languageIcon={true}
          cartIcon={true}
          cartOnPress={() => {
            props.navigation.navigate('CartPage');
          }}
          mailIcon={false}
          searchIcon={true}
          logoImage={
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/logo-oxvdmbxi6g2vpdrt9kcwy3xyhpvajr03in9rykvzfk_220x.png?v=1653569545'
          }
        />
        <View style={{flex: 1, justifyContent: 'center'}}>
          <ActivityIndicator
            style={{
              marginVertical: 10,
              marginHorizontal: 5,
            }}
            size="large"
            color={theme.colors.primary}
          />
        </View>
      </View>
    );
  }

  return (
    <>
      <Header
        leftIcon={'arrow-left'}
        leftOnPress={() => props.navigation.goBack()}
        languageIcon={true}
        cartIcon={true}
        cartOnPress={() => {
          props.navigation.navigate('CartPage');
        }}
        mailIcon={false}
        searchIcon={true}
        logoImage={
          'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/logo-oxvdmbxi6g2vpdrt9kcwy3xyhpvajr03in9rykvzfk_220x.png?v=1653569545'
        }
      />

      <ScrollView contentContainerStyle={{paddingHorizontal: 5}}>
        <CMSRenderer
          components={[
            {
              _component: HomePageComponentType.FeaturedProduct,
              key: '126',
              productInfo: {
                id: product.id,
                sku: product.sku,
                description: product.description ?? '',
                weight_unit: product.weight_unit ?? '',
                packaging: product.packaging ?? '',
                key: product.url_key ?? '',
                notes: product.notes ?? '',
                name: product.name,
                listInfo: {
                  description:
                    '100% fresh. Sourced from UAE. Benefits: Dates contain vitamins such as B1, B2, B3 and B5, as well as A1 and C. Dates are loaded with potassium and rich in Iron, which is highly recommended for those who suffer from iron deficiency.',
                  shippingInformation: 'Shipping Information',
                },
                blockInfo: {
                  freeShipping: 'Free shipping in UAE',
                  inStock: 'In stock, ready to ship',
                  securePayments: 'Secure Payments',
                  isFresh: 'Fresh',
                },
                price: +product.price_range.minimum_price.regular_price.value,
                currency:
                  product.price_range.minimum_price.regular_price.currency,
                image: [product.image.url],
              },
              handleBuyItNow: () => {
                props.navigation.navigate('CartPage');
              },
            },
            {
              _component: HomePageComponentType.FeaturedCollection,
              key: '1251',
              title: 'Related Products',
              products: productList,
              handlePress: () => {
                props.navigation.navigate('ProductListPage1');
              },
              onProductPress: (item: any) => {
                props.navigation.navigate('ProductPage', {
                  handle: item.url_key,
                });
              },
            },
            {
              _component: HomePageComponentType.FeaturedCollection,
              key: '1250',
              title: 'Recently viewed',
              products: productList1,
              handlePress: () => {
                props.navigation.navigate('ProductListPage1');
              },
              onProductPress: (item: any) => {
                props.navigation.navigate('ProductPage', {
                  handle: item.url_key,
                });
              },
            },
          ]}
        />
      </ScrollView>
    </>
  );
};

export default ProductPage;
