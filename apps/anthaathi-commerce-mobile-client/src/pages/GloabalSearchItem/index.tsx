import {StyleSheet, View, SafeAreaView, StatusBar} from 'react-native';
import React, {useState} from 'react';
import SearchItemList from '../../features/CMS/containers/SearchBarPage/components/SearchItemList';
import SearchBar from '../../features/CMS/containers/SearchBarPage/components/SearchBar';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../types/Route';
import {TSProductProps} from '../ProductListPage';
import {useSearch} from '../../hooks/useSearch';

const GloabalSearchItem: React.FC<
  NativeStackScreenProps<RootStackParamList, 'GloabalSearchItem'>
> = props => {
  const [value, setValue] = useState('');
  const [productList, setProductList] = useState<TSProductProps[]>([]);
  // const [pageNo, setPageNo] = useState<number>(1);
  // const [loadMore, setLoadMore] = useState<boolean>(false);
  // const [loading, setLoading] = useState<boolean>(true);
  const {getRecords} = useSearch();

  return (
    <SafeAreaView style={styles.safe}>
      <StatusBar barStyle="light-content" />
      <View style={styles.container}>
        <SearchBar
          value={value}
          setValue={input => {
            setValue(input);
            getRecords(input).then(response => {
              setProductList(response.records);
            });
          }}
          onBackPress={() => props.navigation.goBack()}
        />
        {value.length > 0 ? (
          <SearchItemList
            productList={productList}
            // loading={loading}
            // loadMore={loadMore}
            // loadMorePress={() => {
            //   setPageNo(prev => prev + 1);
            // }}
          />
        ) : null}
      </View>
    </SafeAreaView>
  );
};

export default GloabalSearchItem;

const styles = StyleSheet.create({
  safe: {
    flex: 1,
    backgroundColor: '#252b33',
  },
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
  },
});
