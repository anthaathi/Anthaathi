/* eslint-disable react-hooks/exhaustive-deps */
import {View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {
  CartPageComponentType,
  ProductListPageComponentType,
} from '../../features/CMS/types/common';
import CMSRenderer from '../../features/CMS';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {RootStackParamList} from '../../types/Route';
import {gql, useQuery} from '@apollo/client';
export interface TSProductProps {
  id: string;
  title: string;
  description?: string;
  handle?: string;
  price: string;
  currency: string;
  image: string;
  collections?: string;
}
const getProductList = gql`
  query getCategoryByHandle($handle: String!, $currentPage: Int) {
    categories(filters: {url_key: {eq: $handle}}) {
      items {
        ... on CategoryTree {
          meta_title
          products(currentPage: $currentPage, pageSize: 6) {
            items {
              id: uid
              name
              sku
              title: meta_title
              description: meta_description
              url_key
              price: price_range {
                minimum_price {
                  discount {
                    amount_off
                    percent_off
                  }
                  regular_price {
                    currency
                    value
                  }
                  final_price {
                    value
                    currency
                  }
                }
              }
              image {
                url
              }
            }
            page_info {
              page_size
              current_page
              total_pages
            }
            total_count
          }
        }
      }
    }
  }
`;

const ProductListPage = (
  props: NativeStackScreenProps<RootStackParamList, 'ProductListPage'>,
) => {
  const [productList, setProductList] = useState<any[]>([]);
  const [pageNo, setPageNo] = useState<number>(1);
  const [loadMore, setLoadMore] = useState<boolean>(false);
  const [pageLoad, setPageLoad] = useState<boolean>(true);
  const {loading, fetchMore, data} = useQuery(getProductList, {
    variables: {handle: props.route.name, currentPage: pageNo},
  });

  useEffect(() => {
    if (!loading) {
      setProductList(prev => [
        ...prev,
        ...data?.categories?.items[0].products.items,
      ]);
      setPageLoad(false);
      if (
        data?.categories?.items[0].products.page_info.current_page <
        data?.categories?.items[0].products.page_info.total_pages
      ) {
        setLoadMore(true);
      } else {
        setLoadMore(false);
      }
    }
  }, [loading]);

  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <CMSRenderer
        components={[
          {
            _component: ProductListPageComponentType.ProductList,
            key: '1233',
            handleInfoPress: (item: any) => {
              props.navigation.navigate('ProductPage', {
                handle: item.url_key,
              });
            },
            products: productList,
            loading: loading,
            loadMore: loadMore,
            pageLoad: pageLoad,
            loadMorePress: () => {
              if (!loading) {
                fetchMore({
                  variables: {handle: props.route.name, currentPage: pageNo},
                });
                setPageNo(prev => prev + 1);
              }
            },
          },
        ]}
      />
      {/* {cartItem.length > 0 ? (
        <CMSRenderer
          components={[
            {
              _component: CoreComponentType.CMSFABButton,
              key: '123',
              title:
                'View Basket ' +
                (cartItem.length > 0
                  ? `(${cartItem.length} Items) (${intl.formatNumber(
                      productTotalPrice,
                      {
                        style: 'currency',
                        currency: 'AED',
                      },
                    )})`
                  : ''),
              icon: 'cart',
              buttonRadius: 50,
              buttonBackgroundColor: '#0f8443',
              buttonWidth: '90%',
              buttonViewWidth: '100%',
              handlePress: () => {
                props.navigation.navigate('CartPage');
              },
            },
          ]}
        />
      ) : null} */}
    </View>
  );
};

export default ProductListPage;
