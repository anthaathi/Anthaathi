import {ScrollView, Text, View} from 'react-native';
import React, {useMemo} from 'react';
import CMSRenderer from '../../features/CMS';
import {
  CartPageComponentType,
  CoreComponentType,
} from '../../features/CMS/types/common';
import {RootStackParamList} from '../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {useRecoilState} from 'recoil';
import {useIntl} from 'react-intl';
import {useTheme} from 'react-native-paper';
import {CartItemData, useCart} from '../../hooks/useCart';
import {gql, useQuery} from '@apollo/client';

const getProductList = gql`
  query getCategoryByHandle($handle: String!, $currentPage: Int) {
    categories(filters: {url_key: {eq: $handle}}) {
      items {
        ... on CategoryTree {
          meta_title
          products(currentPage: $currentPage, pageSize: 6) {
            items {
              id: uid
              name
              sku
              title: meta_title
              description: meta_description
              url_key
              price: price_range {
                minimum_price {
                  discount {
                    amount_off
                    percent_off
                  }
                  regular_price {
                    currency
                    value
                  }
                  final_price {
                    value
                    currency
                  }
                }
              }
              image {
                url
              }
            }
            page_info {
              page_size
              current_page
              total_pages
            }
            total_count
          }
        }
      }
    }
  }
`;

const CartPage: React.FC<
  NativeStackScreenProps<RootStackParamList, 'CartPage'>
> = props => {
  const theme = useTheme();
  const intl = useIntl();
  const [cartItem, setCartItem] = useRecoilState(CartItemData);
  const {addProductToCart} = useCart();
  const {loading, data} = useQuery(getProductList, {
    variables: {handle: 'fruits', currentPage: 1},
  });

  const suggestedItem: any[] = useMemo(() => {
    return loading ? [] : data?.categories?.items[0].products.items;
  }, [data, loading]);

  const productTotalPrice = React.useMemo(() => {
    return cartItem.reduce((accumulator, object) => {
      return (
        accumulator +
        object.quantity * object.product.price.minimum_price.final_price.value
      );
    }, 0);
  }, [cartItem]);

  return (
    <View style={{flex: 1, backgroundColor: '#FFF'}}>
      <CMSRenderer
        components={[
          {
            _component: CoreComponentType.Header,
            key: '142',
            title: 'Basket',
            leftIcon: 'close',
            leftOnPress: () => {
              props.navigation.goBack();
            },
          },
        ]}
      />
      {cartItem.length > 0 ? (
        <ScrollView
          style={{
            paddingHorizontal: 10,
            flex: 1,
            marginBottom: 60,
          }}>
          <View style={{marginTop: 14}} />
          <CMSRenderer
            components={[
              {
                _component: CartPageComponentType.SuggestedItem,
                key: '14',
                title: 'Suggested',
                handlePress2: (item: any) => {
                  // console.log('dasdasd', item);
                  addProductToCart(item.sku, 1);
                },
                products: suggestedItem,
              },
              {
                _component: CartPageComponentType.BasketItem,
                key: '1213',
                title: 'Items',
                items: cartItem,
                handlePressRemoveAllProduct: () => {},
                handlePressViewProduct: (item: any) => {
                  props.navigation.navigate('ProductPage', {
                    handle: item.url_key,
                  });
                },
                removeProductPress: (id: number, numberOfItems: number) => {
                  if (numberOfItems > 1) {
                    const newState = cartItem.map(obj => {
                      if (obj.id === id) {
                        return {
                          ...obj,
                          numberOfItems: obj.numberOfItems - 1,
                        };
                      }
                      return obj;
                    });
                    setCartItem(newState);
                  } else {
                    setCartItem(current =>
                      current.filter(obj => {
                        return obj.id !== id;
                      }),
                    );
                  }
                },
                addProductPress: (id: number) => {
                  const newState = cartItem.map(obj => {
                    if (obj.id === id) {
                      return {...obj, numberOfItems: obj.numberOfItems + 1};
                    }
                    return obj;
                  });
                  setCartItem(newState);
                },
              },
              {
                _component: CartPageComponentType.PromoCode,
                key: '12',
                title: 'Add your promo code',
                handlePress: () => {
                  props.navigation.navigate('ApplyCouponPage');
                },
              },
              {
                _component: CartPageComponentType.PricingCard,
                key: '1',
                subtotal: {currency: 'AED', price: productTotalPrice},
                discount: {currency: 'AED', price: 0},
                taxAmount: {currency: 'AED', price: productTotalPrice * 0.05},
                shippingCharges: {currency: 'AED', price: 0},
                total: {
                  currency: 'AED',
                  price: productTotalPrice + productTotalPrice * 0.05,
                },
              },
            ]}
          />
        </ScrollView>
      ) : (
        <View
          style={{alignItems: 'center', marginTop: 50, marginHorizontal: 10}}>
          <Text
            style={{
              color: theme.colors.titleTextColor,
              fontSize: 22,
              fontWeight: '700',
            }}>
            {intl.formatMessage({defaultMessage: 'Your cart is Empty'})}
          </Text>

          <Text
            style={{
              color: theme.colors.titleTextColor,
              fontSize: 16,
              fontWeight: '400',
              marginTop: 20,
              textAlign: 'center',
            }}>
            {intl.formatMessage({
              defaultMessage:
                "Looks like you haven't added any items to the cart yet.",
            })}
          </Text>

          <Text
            style={{
              color: theme.colors.titleTextColor,
              fontSize: 16,
              fontWeight: '400',
              marginTop: 5,
            }}>
            {intl.formatMessage({
              defaultMessage: 'Start shopping to fill it in.',
            })}
          </Text>
        </View>
      )}
      <CMSRenderer
        components={[
          {
            _component: CoreComponentType.CMSButton,
            key: '1241',
            title:
              cartItem.length > 0
                ? intl.formatMessage({
                    defaultMessage: 'Continue to Checkout',
                  })
                : intl.formatMessage({
                    defaultMessage: 'Start Shopping',
                  }),
            handlePress: () => {
              if (cartItem.length > 0) {
                props.navigation.navigate('CheckoutPage');
              } else {
                props.navigation.navigate('HomePage');
              }
            },
          },
        ]}
      />
    </View>
  );
};

export default CartPage;
