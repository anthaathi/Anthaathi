/* eslint-disable react/no-unstable-nested-components */
import {View} from 'react-native';
import React from 'react';
import {Avatar, Button, Divider, List, Text} from 'react-native-paper';
import {RootStackParamList} from '../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {useIntl} from 'react-intl';
import {useRecoilState} from 'recoil';
import {useAuth, userDataRecoil} from '../../hooks/useAuth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {CartItemData} from '../../hooks/useCart';

const ProfilePage = (
  props: NativeStackScreenProps<RootStackParamList, 'Profile'>,
) => {
  const intl = useIntl();
  const [user, setUser] = useRecoilState(userDataRecoil);
  const [_, setCart] = useRecoilState(CartItemData);
  const {signOutUser} = useAuth();

  return (
    <View style={{backgroundColor: 'white', flex: 1, paddingHorizontal: 5}}>
      {user === null ? (
        <View
          style={{
            backgroundColor: '#f0f4f7',
            justifyContent: 'space-between',
            paddingHorizontal: '15%',
            paddingVertical: 20,
            alignItems: 'center',
            flexDirection: 'row',
          }}>
          <Avatar.Icon size={64} icon="account" />
          <Button
            mode="contained"
            uppercase={false}
            style={{
              marginVertical: 15,
              paddingHorizontal: 10,
              marginHorizontal: 30,
              borderRadius: 4,
            }}
            labelStyle={{paddingVertical: 3}}
            onPress={() => {
              props.navigation.navigate('SignIn');
            }}>
            {intl.formatMessage({defaultMessage: 'Sign In'})}
          </Button>
        </View>
      ) : (
        <View>
          <View
            style={{
              backgroundColor: '#f0f4f7',
              paddingHorizontal: '10%',
              paddingVertical: 20,
              alignItems: 'center',
              flexDirection: 'row',
            }}>
            <Avatar.Icon size={64} icon="account" />
            <View style={{marginLeft: 24}}>
              <Text
                variant="titleLarge"
                style={{marginBottom: 0, fontSize: 18, fontWeight: '700'}}>
                {`${user.firstname} ${user.lastname}`}
              </Text>
              <Text
                variant="titleLarge"
                style={{fontSize: 14, fontWeight: '500'}}>
                {user.email}
              </Text>
            </View>
          </View>
          <ListItemData
            title="Personal details"
            iconName="account"
            onPress={() => {
              props.navigation.navigate('EditProfile');
            }}
          />
          <ListItemData
            title="Delivery addresses"
            iconName="map-marker"
            onPress={() => {
              props.navigation.navigate('AddressInfo');
            }}
          />
          <ListItemData
            title="Payment details"
            iconName="credit-card-edit-outline"
          />
          <ListItemData title="Gift cards" iconName="wallet-giftcard" />
          {/* <ListItemData title="My reviews" iconName="star" /> */}
          <ListItemData title="Settings" iconName="cog" />
          <ListItemData
            title="Log out"
            iconName="logout"
            onPress={async () => {
              // const data = await signOutUser();
              // console.log(data);
              // if (data.revokeCustomerToken.result) {
              await AsyncStorage.removeItem('userToken');
              await AsyncStorage.removeItem('cartId');
              setUser(null);
              setCart([]);
              // }
            }}
          />
        </View>
      )}

      {/* <ScrollView contentContainerStyle={{paddingHorizontal: 5}}>
        <CMSRenderer
          components={[
            {
              _component: CoreComponentType.CMSAvatar,
              key: '1231',
              userData: {
                name: 'OY',
                photo: '',
              },
              size: 120,
              type: 'image',
            },
            {
              _component: CoreComponentType.CMSText,
              key: '11',
              title: 'Welcome Omkar Yadav',
            },
            {
              _component: ProfilePageComponentType.WalletBalance,
              key: '2323',
              title: dataJson.core.profilePage.walletBalance.title,
              balance: dataJson.core.profilePage.walletBalance.balance,
              currency: dataJson.core.profilePage.walletBalance.currency,
              buttonTitle: dataJson.core.profilePage.walletBalance.buttonTitle,
            },
        />
      </ScrollView> */}
    </View>
  );
};

const ListItemData = ({
  title,
  subtitle,
  iconName,
  onPress,
}: {
  title: string;
  subtitle?: string;
  iconName: string;
  onPress?: () => void;
}) => {
  return (
    <>
      <List.Item
        onPress={onPress}
        style={{paddingVertical: 15}}
        title={title}
        description={subtitle}
        left={props => <List.Icon {...props} icon={iconName} />}
        right={props => <List.Icon {...props} icon="chevron-right" />}
      />
      <Divider />
    </>
  );
};
export default ProfilePage;
