import React from 'react';
import {Image, View, FlatList, Pressable} from 'react-native';
import {Divider, Text, useTheme} from 'react-native-paper';
import {useIntl} from 'react-intl';

const SearchItemList = ({
  productList,
  loading,
  loadMore,
  loadMorePress,
}: {
  productList: any[];
  loading?: boolean;
  loadMore?: boolean;
  loadMorePress?: () => void;
}) => {
  if (loading) {
    <View>
      <Text style={{fontSize: 16, fontWeight: '500'}}>Loading...</Text>
    </View>;
  }

  return (
    <View style={{paddingVertical: 10}}>
      <FlatList
        data={productList}
        keyExtractor={item => item.id}
        onEndReached={() => loadMore && loadMorePress}
        renderItem={({item}) => <ItemRenderer item={item} />}
      />
    </View>
  );
};

const ItemRenderer = ({item}: {item: any}) => {
  const theme = useTheme();
  const intl = useIntl();
  return (
    <View>
      <Pressable
        style={{marginHorizontal: 10, marginVertical: 5}}
        onPress={() => console.log(item)}>
        <View style={{flexDirection: 'row', paddingHorizontal: 5}}>
          <Image
            source={{
              uri: item.image,
            }}
            style={{height: 54, width: 54, borderRadius: 2}}
          />
          <View
            style={{
              marginHorizontal: 15,
            }}>
            <Text
              testID="productName"
              variant="titleLarge"
              style={{
                marginBottom: 5,
                fontSize: 14,
                color: theme.colors.titleTextColor,
                fontWeight: '900',
                flexWrap: 'wrap',
              }}>
              {item.title}
            </Text>

            <Text
              variant="titleLarge"
              style={{
                fontSize: 13,
                color: theme.colors.titleTextColor,
                fontWeight: '600',
              }}>
              {intl.formatNumber(item.price, {
                style: 'currency',
                currency: item.currency,
              }) +
                ' / ' +
                intl.formatMessage({defaultMessage: 'Piece'})}
            </Text>
          </View>
        </View>
      </Pressable>
      <Divider style={{marginHorizontal: 10}} />
    </View>
  );
};

export default SearchItemList;
