import React from 'react';
import {render} from '@testing-library/react-native';
import 'react-native';
import {ThemeProvider} from 'react-native-paper';
import ImageCarousel from './index';
import {IntlProvider} from 'react-intl';
import locale from '../../../../../../compiled-locales/en-US.json';

describe('ImageCorousel', () => {
  it('should render a item', function () {
    const temp = render(
      <ThemeProvider>
        <IntlProvider locale="en-US" messages={locale}>
          <ImageCarousel
            items={[
              {
                imageSrc:
                  'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/1_1080x.png?v=1666873198',
                onPress: () => {
                  console.log('pressed first');
                },
              },
            ]}
          />
        </IntlProvider>
      </ThemeProvider>,
    );
    expect(temp).toMatchSnapshot();
    expect(temp.queryByTestId('imageCorousel')).toBeTruthy();
  });
});
