import {View} from 'react-native';
import React from 'react';
import {Button, TextInput, useTheme} from 'react-native-paper';
import {useDimension} from '../../../../utils/useDimension';
import {useIntl} from 'react-intl';

export interface CouponCodeInputProps {
  handleOnPress?: () => void;
}

const CouponCodeInput = ({handleOnPress}: CouponCodeInputProps) => {
  const [getWidth] = useDimension();
  const theme = useTheme();
  const intl = useIntl();
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginVertical: 10,
        marginHorizontal: 10,
        elevation: 20,
      }}
      testID="couponCodeInput">
      <TextInput
        mode="flat"
        label={intl.formatMessage({
          defaultMessage: 'Enter coupon code',
        })}
        style={{
          backgroundColor: '#fff',
          fontSize: 14,
          height: 56,
          width: getWidth - 130,
          marginHorizontal: 5,
        }}
        activeUnderlineColor="#0f8443"
      />
      <Button
        mode="text"
        style={{
          borderRadius: 4,
          height: 56,
          width: 100,
          borderColor: theme.colors.primary,
          justifyContent: 'center',
          alignItems: 'center',
        }}
        onPress={handleOnPress}
        testID="applyPressed">
        {intl.formatMessage({defaultMessage: 'Apply'})}
      </Button>
    </View>
  );
};

export default CouponCodeInput;
