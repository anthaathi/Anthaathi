import {View, Text, ScrollView, Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {Button, Colors, Divider, useTheme} from 'react-native-paper';
import {useIntl} from 'react-intl';

export interface CouponCodeListProps {
  title: string;
  items: ItemProps[];
  handlePress?: (id: string) => void;
  handleViewDetailsPress?: (id: string) => void;
}

export interface ItemProps {
  id: string;
  title: string;
  subtitle?: string;
  imageUrl: string;
  couponCode: string;
}

const CouponCodeList = ({
  title,
  items,
  handlePress,
  handleViewDetailsPress,
}: CouponCodeListProps) => {
  return (
    <ScrollView
      testID="couponCodeList"
      style={{
        paddingHorizontal: 10,
        flex: 1,
        backgroundColor: '#fff',
      }}>
      <Text
        style={{
          color: '#364A15',
          fontSize: 18,
          fontWeight: '600',
          paddingVertical: 10,
        }}>
        {title}
      </Text>
      <View style={{marginTop: 14}} />
      {items.map((item, index) => (
        <ListItemRenderer
          key={index}
          item={item}
          handlePress={handlePress || (() => {})}
          handleViewDetailsPress={handleViewDetailsPress || (() => {})}
        />
      ))}
    </ScrollView>
  );
};

const ListItemRenderer = ({
  item,
  handlePress,
  handleViewDetailsPress,
}: {
  item: ItemProps;
  handlePress: (id: string) => void;
  handleViewDetailsPress: (id: string) => void;
}) => {
  const theme = useTheme();
  const intl = useIntl();

  return (
    <View style={{}}>
      <Image
        testID="productImage"
        source={{
          uri: item.imageUrl,
        }}
        style={{
          height: 32,
          width: 80,
          resizeMode: 'contain',
          marginVertical: 2,
        }}
      />
      <Text style={{color: '#364A15', fontSize: 16, fontWeight: '500'}}>
        {item.title}
      </Text>
      <TouchableOpacity onPress={() => handleViewDetailsPress(item.id)}>
        <Text
          style={{
            marginTop: 5,
            color: '#364A15',
            fontSize: 12,
            fontWeight: '400',
          }}>
          {item.subtitle}
        </Text>

        <Text style={{color: '#364A15', fontSize: 14, fontWeight: '500'}}>
          View Details
        </Text>
      </TouchableOpacity>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
          marginTop: 7,
        }}>
        <Text
          style={{
            borderStyle: 'dashed',
            borderWidth: 1,
            borderRadius: 4,
            borderColor: theme.colors.primary,
            width: 120,
            paddingVertical: 5,
            textAlign: 'center',
            color: Colors.grey600,
          }}>
          {item.couponCode}
        </Text>
        <Button
          mode="text"
          style={{
            borderRadius: 4,
            borderColor: theme.colors.primary,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          labelStyle={{
            fontSize: 13,
          }}
          uppercase={false}
          onPress={() => handlePress(item.id)}>
          {intl.formatMessage({defaultMessage: 'Apply'})}
        </Button>
      </View>
      <Divider style={{marginHorizontal: 5, marginVertical: 10}} />
    </View>
  );
};

export default CouponCodeList;
