import {Image, Pressable, View} from 'react-native';
import React from 'react';
import {Divider, IconButton, Text, useTheme} from 'react-native-paper';
import {useResponsiveValue} from '../../../../utils/useResponsiveValue';
import {useIntl} from 'react-intl';
import {CartPageComponentType} from '../../../../types/common';
import {ProductProps} from '../../../ProductListPage/components/ProductList';
import CartQuantityChange from '../CartQuantityChange';
import {useCart} from '../../../../../../hooks/useCart';
import {TSProductProps} from '../../../../../../pages/ProductListPage';

export interface ItemProps extends TSProductProps {
  numberOfItems: number;
}

export interface BasketItemProps {
  title: string;
  items: any[];
  handlePressRemoveAllProduct?: () => void;
  handlePressViewProduct?: (item: ProductProps) => void;
  addProductPress?: (id: number) => void;
  removeProductPress?: (id: number, numberOfItems: number) => void;
}

const BasketItem = (props: BasketItemProps) => {
  const theme = useTheme();
  const intl = useIntl();
  const itemHeight = useResponsiveValue([120, 250, 290, 330]);
  const itemWidth = useResponsiveValue([120, 240, 280, 320]);

  return (
    <View
      style={{
        marginHorizontal: 5,
      }}
      testID="basketItem">
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Text variant="titleLarge" style={{marginBottom: 9, fontSize: 20}}>
          {props.title}
        </Text>

        <Pressable
          onPress={props.handlePressRemoveAllProduct}
          testID="handlePressBasketItem">
          <Text
            variant="titleMedium"
            style={{
              marginBottom: 9,
              textDecorationLine: 'underline',
              fontSize: 14,
              color: theme.colors.greenTextColor,
            }}>
            {intl.formatMessage({defaultMessage: 'Remove All'})}
          </Text>
        </Pressable>
      </View>

      <View>
        {props.items.map(item => {
          return (
            <ItemRenderer
              key={item.id}
              item={item}
              itemHeight={itemHeight}
              itemWidth={itemWidth}
              handlePressViewProduct={
                props.handlePressViewProduct || (() => {})
              }
              addProductPress={props.addProductPress || (() => {})}
              removeProductPress={props.removeProductPress || (() => {})}
            />
          );
        })}
      </View>
    </View>
  );
};

const ItemRenderer = ({
  item,
  itemHeight,
  itemWidth,
  handlePressViewProduct,
  addProductPress,
  removeProductPress,
}: {
  item: any;
  itemHeight: number;
  itemWidth: number;
  handlePressViewProduct: (item: ProductProps) => void;
  addProductPress: (id: number) => void;
  removeProductPress: (id: number, numberOfItems: number) => void;
}) => {
  const theme = useTheme();
  const intl = useIntl();
  const {setCartItem} = useCart();

  return (
    <View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View style={{flexDirection: 'row'}}>
          <Pressable
            onPress={() => {
              handlePressViewProduct(item.product);
            }}>
            <Image
              testID="basketProductImage"
              source={{uri: item.product.image.url}}
              style={{height: itemHeight, width: itemWidth}}
            />
          </Pressable>

          <View
            style={{
              marginHorizontal: 15,
              flexDirection: 'column',
              justifyContent: 'space-between',
              width: '100%',
            }}>
            <View style={{flexDirection: 'row', width: '45%'}}>
              <Text
                testID="productName"
                variant="titleLarge"
                style={{
                  flex: 1,
                  fontSize: 14,
                  color: theme.colors.titleTextColor,
                  fontWeight: '900',
                  flexWrap: 'wrap',
                }}>
                {item.product.name}
              </Text>
            </View>

            <CartQuantityChange
              trashIcon
              initialValue={item.quantity}
              sku={item.product.sku}
              onChangeQuantity={qty => {
                setCartItem(item.id, qty);
              }}
              id={item.id}
            />

            <Text
              variant="titleLarge"
              style={{
                fontSize: 13,
                color: theme.colors.titleTextColor,
                fontWeight: '600',
              }}>
              {intl.formatNumber(
                item.product.price.minimum_price.final_price.value,
                {
                  style: 'currency',
                  currency:
                    item.product.price.minimum_price.final_price.currency,
                },
              ) +
                ' / ' +
                intl.formatMessage({defaultMessage: 'Piece'})}
            </Text>

            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <Text
                variant="titleLarge"
                style={{
                  fontSize: 14,
                  color: theme.colors.titleTextColor,
                  fontWeight: '900',
                }}>
                {intl.formatMessage({defaultMessage: 'Total'}) + ' :'}
              </Text>

              <Text
                testID="productPrice"
                variant="titleLarge"
                style={{
                  fontSize: 14,
                  color: theme.colors.greenTextColor,
                  fontWeight: '700',
                  marginLeft: 5,
                }}>
                {intl.formatNumber(
                  item.product.price.minimum_price.final_price.value *
                    item.quantity,
                  {
                    style: 'currency',
                    currency:
                      item.product.price.minimum_price.final_price.currency,
                  },
                )}
              </Text>
            </View>
          </View>
        </View>
      </View>
      <Divider style={{marginVertical: 10}} />
    </View>
  );
};

export default BasketItem;

export const BasketItemCMSInput = {
  _component: CartPageComponentType.BasketItem,
  component: BasketItem,
};
