import { FormControl } from 'baseui/form-control';
import { DatePicker } from 'baseui/datepicker';
import { Select } from 'baseui/select';
import { Button } from 'baseui/button';
import { useNavigate } from 'react-router-dom';
import { useStyletron } from 'baseui';
import { Radio, RadioGroup } from 'baseui/radio';
import { HeadingSmall } from 'baseui/typography';
import { ReviewBlock } from '../../Features/ECommerce/Components/ReviewBlock';
import { gql, useMutation } from '@apollo/client';
import {
  GetShippingMethodsQuery,
  GetShippingMethodsQueryVariables,
  SetShippingMethodMutation,
  SetShippingMethodMutationVariables,
} from '../../__generated__/graphql';
import { useRecoilValue } from 'recoil';
import { CartAtom } from '../../atoms';
import { useFormik } from 'formik';
import { useQuery } from '../../Features/Core/hooks/useSuspenseQuery';
import { useMemo } from 'react';
import unserialize from 'locutus/php/var/unserialize';
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
  // deliveryDate: Yup.string().required('Delivery date is required.'),
  timeslot: Yup.string().required('Time slot is required.'),
});

export default function Shipping() {
  const [css] = useStyletron();

  const cart = useRecoilValue(CartAtom);

  const { data: getShippingMethods } = useQuery<
    GetShippingMethodsQuery,
    GetShippingMethodsQueryVariables
  >(
    gql`
      query getShippingMethods($cartId: String!) {
        cart(cart_id: $cartId) {
          delivery_date {
            shipping_arrival_date
            shipping_arrival_timeslot
          }
          shipping_addresses {
            available_shipping_methods {
              method_code
              method_title
              carrier_code
              amount {
                currency
                value
              }
            }
          }
        }

        bssDeliveryDateStoreConfig(store_view: 0) {
          cut_off_time
          time_slots
          block_out_holidays
        }
      }
    `,
    {
      variables: {
        cartId: JSON.parse(cart?.value!!),
      },
      suspense: true,
    },
  );

  const deliveryConfig = useMemo(() => {
    const timeSlots =
      getShippingMethods?.bssDeliveryDateStoreConfig?.time_slots;
    if (!timeSlots) {
      return null;
    }

    const result = unserialize(timeSlots);

    return Object.keys(result).map((res) => {
      return {
        ...result[res],
        id: res,
      };
    }) as {
      from: string;
      name: string;
      note: string;
      price: string;
      to: string;
      id: string;
    }[];
  }, [getShippingMethods?.bssDeliveryDateStoreConfig?.time_slots]);

  const [applyShippingMethod] = useMutation<
    SetShippingMethodMutation,
    SetShippingMethodMutationVariables
  >(gql`
    mutation setShippingMethod(
      $cartId: String!
      $methodCode: String!
      $carrierCode: String!
      $timeSlot: String!
      $arrivalDate: String!
    ) {
      setShippingMethodsOnCart(
        input: {
          cart_id: $cartId
          shipping_methods: {
            method_code: $methodCode
            carrier_code: $carrierCode
          }
        }
      ) {
        cart {
          id
        }
      }

      applyDeliveryCart(
        input: {
          shipping_arrival_timeslot: $timeSlot
          shipping_arrival_date: $arrivalDate
          cart_id: $cartId
        }
      ) {
        cart {
          __typename
        }
      }
    }
  `);

  const navigate = useNavigate();

  const formik = useFormik({
    initialValues: {
      shippingMethod: 0,
      deliveryDate: new Date(new Date().setDate(new Date().getDate() + 1)),
      timeslot: '',
    },
    validationSchema,
    onSubmit: (values, formikHelpers) => {
      const v =
        getShippingMethods?.cart?.shipping_addresses?.[0]
          ?.available_shipping_methods?.[values.shippingMethod];

      function pad(num: string, size: number) {
        const s = '000000000' + num;
        return s.substring(s.length - size);
      }

      applyShippingMethod({
        variables: {
          cartId: JSON.parse(cart?.value!!),
          carrierCode: v?.carrier_code!!,
          methodCode: v?.method_code!!,
          arrivalDate: `${pad(
            values.deliveryDate.getDate().toString(),
            2,
          )}/${pad(
            values.deliveryDate.getMonth().toString(),
            2,
          )}/${values.deliveryDate.getFullYear().toString()}`,
          timeSlot: values.timeslot,
        },
      }).then(() => {
        formikHelpers.setSubmitting(false);
        navigate({ pathname: '/checkout/payment' });
      });
    },
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <ReviewBlock />

      <HeadingSmall marginTop="scale600" marginBottom="scale600">
        Shipping Information
      </HeadingSmall>

      <FormControl
        label="Delivery date"
        // caption={() =>
        //   formik.errors.deliveryDate ? formik.errors.deliveryDate : null
        // }
        overrides={{
          Caption: {
            style: () => ({
              color: 'red',
            }),
          },
        }}
      >
        <DatePicker
          placeholder="Delivery date"
          value={[formik.values.deliveryDate]}
          minDate={new Date()}
          onChange={(option) => {
            formik.handleChange({
              target: {
                name: 'deliveryDate',
                value: Array.isArray(option.date)
                  ? option.date[0]
                  : option.date,
              },
            });
          }}
        />
      </FormControl>
      <FormControl
        label="Time slot"
        caption={() => (formik.errors.timeslot ? formik.errors.timeslot : null)}
        overrides={{
          Caption: {
            style: () => ({
              color: 'red',
            }),
          },
        }}
      >
        <Select
          onChange={(e) => {
            formik.handleChange({
              target: {
                name: 'timeslot',
                value: Array.isArray(e.value) ? e.value[0].id : e.value,
              },
            });
          }}
          value={[{ id: formik.values.timeslot }]}
          options={
            deliveryConfig?.map((res) => ({
              label: res.name,
              id: res.name,
            })) || []
          }
        />
      </FormControl>

      <FormControl label="Shipping Method">
        <RadioGroup
          value={formik.values.shippingMethod.toString()}
          onChange={(e) => {
            formik.handleChange({
              target: {
                name: 'shippingMethod',
                value: +e.target.value,
              },
            });
          }}
        >
          {getShippingMethods?.cart?.shipping_addresses?.[0]?.available_shipping_methods?.map(
            (res: any, index: any) => (
              <Radio
                value={index.toString()}
                key={(res?.method_title ?? '') + index}
                description={Intl.NumberFormat('en-US', {
                  style: 'currency',
                  currency: res?.amount.currency!!,
                }).format(res?.amount.value!!)}
              >
                {res?.method_title}
              </Radio>
            ),
          )}
        </RadioGroup>
      </FormControl>

      <div
        className={css({
          display: 'flex',
          flexDirection: 'row-reverse',
          marginTop: '24px',
        })}
      >
        <Button
          type="submit"
          overrides={{
            Root: {
              style: ({ $theme }) => ({
                width: '100%',
                [$theme.mediaQuery.medium || '']: {
                  width: 'max-content',
                },
              }),
            },
          }}
          colors={{ backgroundColor: '#03703c', color: 'white' }}
          isLoading={formik.isSubmitting}
        >
          Save and Continue
        </Button>
        <span className={css({ flexGrow: 1 })} />
      </div>
    </form>
  );
}
