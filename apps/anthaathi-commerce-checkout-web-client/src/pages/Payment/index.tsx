import React, { useState } from 'react';
import { Panel, StatelessAccordion } from 'baseui/accordion';
import { ReviewBlock } from '../../Features/ECommerce/Components/ReviewBlock';
import { useStyletron } from 'baseui';
import { HeadingSmall, ParagraphSmall } from 'baseui/typography';
import { Radio } from 'baseui/radio';
import { AddressForm } from '../../Features/ECommerce/Containers/AddressForm';
import CartDetails from '../../Features/ECommerce/Components/CartDetails';
import { gql, useMutation } from '@apollo/client';
import {
  GetPaymentMethodsQuery,
  GetPaymentMethodsQueryVariables,
  SetPaymentMethodMutation,
  SetPaymentMethodMutationVariables,
} from '../../__generated__/graphql';
import { useRecoilState, useRecoilValue } from 'recoil';
import { CartAtom } from '../../atoms';
import { Button } from 'baseui/button';
import { useQuery } from '../../Features/Core/hooks/useSuspenseQuery';
import toast from 'react-hot-toast';

export const PaymentMethods: PaymentMethod[] = [
  {
    id: 'card',
    name: 'Card',
    component: <CartDetails />,
    hasCardInput: true,
  },
  {
    id: 'card-on-delivery',
    name: 'Card on delivery',
  },
  {
    id: 'cash-on-delivery',
    name: 'Cash on delivery',
  },
];

export default function Payment() {
  const [css, $theme] = useStyletron();
  const [paymentMethod, setPaymentMethod] = useState<React.Key[]>([]);
  const [billingAddress, setBillingAddress] = useState<React.Key[]>([]);

  const [cart, setCart] = useRecoilState(CartAtom);

  const { data, loading } = useQuery<
    GetPaymentMethodsQuery,
    GetPaymentMethodsQueryVariables
  >(
    gql`
      query GetPaymentMethods($cartId: String!) {
        cart(cart_id: $cartId) {
          available_payment_methods {
            id: code
            name: title
          }

          shipping_addresses {
            firstname
            lastname
            street
            telephone
            city
            company
            country {
              code
              label
            }
            postcode
            region {
              code
              label
              region_id
            }
          }
        }
      }
    `,
    {
      variables: {
        cartId: JSON.parse(cart?.value!!),
      },
      suspense: true,
    },
  );

  const BillingAddressMethods: PaymentMethod[] = [
    {
      id: 'same-as-shipping-address',
      name: 'Same as shipping address',
    },
    {
      id: 'different-address',
      name: 'Use a different billing address',
      component: (
        <AddressForm
          onChange={(data: {}) => {}}
          initialValue={{}}
          noSaveButton={true}
        />
      ),
      hasCardInput: true,
    },
  ];

  const [placeOrder] = useMutation<
    SetPaymentMethodMutation,
    SetPaymentMethodMutationVariables
  >(gql`
    mutation setPaymentMethod(
      $cartId: String!
      $code: String!
      $sameAsShipping: Boolean
      $address: CartAddressInput
    ) {
      setBillingAddressOnCart(
        input: {
          cart_id: $cartId
          billing_address: {
            same_as_shipping: $sameAsShipping
            address: $address
          }
        }
      ) {
        cart {
          id
        }
      }
      setPaymentMethodOnCart(
        input: { cart_id: $cartId, payment_method: { code: $code } }
      ) {
        cart {
          id
        }
      }
      placeOrder(input: { cart_id: $cartId }) {
        order {
          order_number
        }
      }
    }
  `);

  const [isSubmitting, setIsSubmitting] = useState(false);

  return (
    <form onSubmit={(e) => {
      e.preventDefault();
      setIsSubmitting(true);
      placeOrder({
        variables: {
          cartId: JSON.parse(cart?.value!!),
          code: paymentMethod[0] as string,
          sameAsShipping:
            billingAddress[0] === 'same-as-shipping-address',
        },
      }).then((docs) => {
        setIsSubmitting(false);
        setCart(null);
        window.open('/my-account/order/' + docs.data?.placeOrder?.order.order_number);
      }).catch(e => {
        setIsSubmitting(false);
      });
    }}>
      <ReviewBlock />

      <>
        <HeadingSmall marginTop="scale1200" marginBottom="scale300">
          Payment
        </HeadingSmall>

        <ParagraphSmall marginTop="scale200">
          All transactions are secure and encrypted.
        </ParagraphSmall>

        <div className={css({ borderRadius: '10px', backgroundColor: '#EEE' })}>
          <StatelessAccordion
            expanded={paymentMethod}
            overrides={{
              Root: {
                style: { borderRadius: '12px' },
              },
            }}
            onChange={({ expanded }) => setPaymentMethod(expanded)}
            accordion
          >
            {data?.cart?.available_payment_methods
              ?.filter((res) => Boolean(res))
              .map((res, index) => (
                <Panel
                  key={res!!.id}
                  title={
                    <div
                      className={css({ display: 'flex', alignItems: 'center' })}
                    >
                      <Radio checked={paymentMethod.indexOf(res!!.id) !== -1} />{' '}
                      &nbsp; {res!!.name}
                    </div>
                  }
                  overrides={{
                    Content: {
                      style: {
                        paddingTop: 0,
                        paddingBottom: 0,
                        borderBottomLeftRadius:
                          index === PaymentMethods.length - 1 ? '12px' : 0,
                        borderBottomRightRadius:
                          index === PaymentMethods.length - 1 ? '12px' : 0,
                      },
                    },
                    Header: {
                      style: {
                        borderTopLeftRadius: index === 0 ? '12px' : '0px',
                        borderTopRightRadius: index === 0 ? '12px' : '0px',
                        borderBottomLeftRadius:
                          index === PaymentMethods.length - 1 ? '12px' : '0px',
                        borderBottomRightRadius:
                          index === PaymentMethods.length - 1 ? '12px' : '0px',
                        paddingTop: $theme.sizing.scale400,
                        paddingBottom: $theme.sizing.scale400,
                        borderBottomWidth:
                          billingAddress.indexOf(res!!.id) !== -1 ? '1px' : 0,
                        borderBottomStyle:
                          billingAddress.indexOf(res!!.id) !== -1
                            ? 'solid'
                            : null,
                        borderBottomColor:
                          billingAddress.indexOf(res!!.id) !== -1
                            ? '#e2e2e2'
                            : null,
                      },
                    },
                    PanelContainer: {
                      style: {
                        borderTopLeftRadius: index === 0 ? '12px' : '0px',
                        borderTopRightRadius: index === 0 ? '12px' : '0px',
                        borderBottomLeftRadius:
                          index === PaymentMethods.length - 1 ? '12px' : '0px',
                        borderBottomRightRadius:
                          index === PaymentMethods.length - 1 ? '12px' : '0px',
                        borderRightWidth: '1px',
                        borderTopWidth: '1px',
                        borderLeftWidth: '1px',
                        borderTopStyle: 'solid',
                        borderLeftStyle: 'solid',
                        borderRightStyle: 'solid',
                        borderTopColor: '#e2e2e2',
                        borderLeftColor: '#e2e2e2',
                        borderRightColor: '#e2e2e2',
                      },
                    },
                    ToggleIcon: {
                      component: () => <></>,
                    },
                  }}
                >
                  <></>
                </Panel>
              ))}
          </StatelessAccordion>
        </div>
      </>

      <>
        <HeadingSmall marginTop="scale1200" marginBottom="scale300">
          Billing address
        </HeadingSmall>

        <ParagraphSmall marginTop="scale200">
          Select the address that matches your card or payment method.
        </ParagraphSmall>

        <div className={css({ borderRadius: '10px', backgroundColor: '#EEE' })}>
          <StatelessAccordion
            expanded={billingAddress}
            overrides={{ Root: { style: { borderRadius: '12px' } } }}
            onChange={({ expanded }) => setBillingAddress(expanded)}
            accordion
          >
            {BillingAddressMethods.map((res, index) => (
              <Panel
                key={res.id}
                title={
                  <div
                    className={css({ display: 'flex', alignItems: 'center' })}
                  >
                    <Radio checked={billingAddress.indexOf(res.id) !== -1} />{' '}
                    &nbsp; {res.name}
                  </div>
                }
                overrides={{
                  Content: {
                    style: {
                      paddingTop: 0,
                      paddingBottom: 0,
                      borderBottomLeftRadius: '12px',
                      borderBottomRightRadius: '12px',
                    },
                  },
                  Header: {
                    style: {
                      borderTopLeftRadius: index === 0 ? '12px' : '0px',
                      borderTopRightRadius: index === 0 ? '12px' : '0px',
                      borderBottomLeftRadius:
                        index === BillingAddressMethods.length - 1 &&
                        billingAddress.indexOf(res.id) === -1
                          ? '12px'
                          : '0px',
                      borderBottomRightRadius:
                        index === BillingAddressMethods.length - 1 &&
                        billingAddress.indexOf(res.id) === -1
                          ? '12px'
                          : '0px',
                      paddingTop: $theme.sizing.scale400,
                      paddingBottom: $theme.sizing.scale400,
                      borderBottomWidth:
                        billingAddress.indexOf(res.id) !== -1 ? '1px' : 0,
                      borderBottomStyle:
                        billingAddress.indexOf(res.id) !== -1 ? 'solid' : null,
                      borderBottomColor:
                        billingAddress.indexOf(res.id) !== -1
                          ? '#e2e2e2'
                          : null,
                    },
                  },
                  PanelContainer: {
                    style: {
                      borderTopLeftRadius: index === 0 ? '12px' : '0px',
                      borderTopRightRadius: index === 0 ? '12px' : '0px',
                      borderBottomLeftRadius:
                        index === BillingAddressMethods.length - 1
                          ? '12px'
                          : '0px',
                      borderBottomRightRadius:
                        index === BillingAddressMethods.length - 1
                          ? '12px'
                          : '0px',
                      borderRightWidth: '1px',
                      borderTopWidth: '1px',
                      borderLeftWidth: '1px',
                      borderTopStyle: 'solid',
                      borderLeftStyle: 'solid',
                      borderRightStyle: 'solid',
                      borderTopColor: '#e2e2e2',
                      borderLeftColor: '#e2e2e2',
                      borderRightColor: '#e2e2e2',
                    },
                  },
                  ToggleIcon: {
                    component: () => <></>,
                  },
                }}
              >
                {res.component}
              </Panel>
            ))}
          </StatelessAccordion>
        </div>
      </>

      <div className={css({ display: 'flex', marginTop: '24px' })}>
        <span className={css({ flexGrow: 1 })} />
        <Button type="submit" isLoading={isSubmitting}>
          PLACE ORDER
        </Button>
      </div>
    </form>
  );
}

export interface PaymentMethod {
  id: string;
  name: string;
  description?: string;
  hasCardInput?: boolean;
  component?: JSX.Element;
}
