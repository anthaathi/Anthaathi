import { useStyletron } from 'baseui';
import { FlexGrid, FlexGridItem } from 'baseui/flex-grid';
import { FormControl } from 'baseui/form-control';
import { Input } from 'baseui/input';
import { PaymentCard } from 'baseui/payment-card';
import { useState } from 'react';

const CartDetails = () => {
  const [value, setValue] = useState('123213213213211');
  const [css, $theme] = useStyletron();

  return (
    <div className={css({ marginTop: $theme.sizing.scale500 })}>
      <PaymentCard
        overrides={{
          Root: {
            style: {
              marginBottom: $theme.sizing.scale600,
            },
          },
        }}
        value={value}
        onChange={(e) => setValue(e.target.value)}
        clearOnEscape
        placeholder="Card Number"
      />
      <FormControl>
        <Input
          placeholder="Name on card"
          id="nameOnCard"
          name="nameOnCard"
          type="text"
        />
      </FormControl>
      <FlexGrid
        flexGridColumnCount={[1, 1, 2, 2]}
        flexGridColumnGap="scale700"
        flexGridRowGap="scale100"
      >
        <FlexGridItem>
          <FormControl>
            <Input
              placeholder="Expiration date (MM / YY)"
              id="expireDate"
              name="expireDate"
              type="text"
            />
          </FormControl>
        </FlexGridItem>
        <FlexGridItem>
          <FormControl>
            <Input
              placeholder="Security code"
              id="securityCode"
              name="securityCode"
              type="text"
            />
          </FormControl>
        </FlexGridItem>
      </FlexGrid>
    </div>
  );
};

export default CartDetails;
