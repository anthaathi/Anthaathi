import React, { useMemo } from 'react';
import { LabelSmall } from 'baseui/typography';
import { Button, KIND, SIZE } from 'baseui/button';
import { useStyletron } from 'baseui';
import { gql } from '@apollo/client';
import { useRecoilValue } from 'recoil';
import { CartAtom } from '../../../../atoms';
import {
  GetReviewBlockQuery,
  GetReviewBlockQueryVariables,
} from '../../../../__generated__/graphql';
import { Link } from 'react-router-dom';
import { useQuery } from '../../../Core/hooks/useSuspenseQuery';

export interface ReviewBlockProps {}

export interface ReviewBlock {
  title: React.ReactNode;
  content: React.ReactNode;
  href?: string;
}

export function ReviewBlock({}: ReviewBlockProps) {
  const [css, $theme] = useStyletron();

  const cart = useRecoilValue(CartAtom);

  const { loading, data } = useQuery<
    GetReviewBlockQuery,
    GetReviewBlockQueryVariables
  >(
    gql`
      query GetReviewBlock($cartId: String!) {
        cart(cart_id: $cartId) {
          email
          shipping_addresses {
            street
            region {
              label
            }
            country {
              label
            }
            selected_shipping_method {
              method_title
            }
          }
        }
      }
    `,
    {
      variables: {
        cartId: JSON.parse(cart?.value!!),
      },
      suspense: true,
    },
  );

  const block = useMemo(() => {
    const addr = data?.cart?.shipping_addresses?.[0];

    return (
      [
        {
          title: 'Email',
          content: data?.cart?.email,
          href: '/checkout/information#email',
        },
        {
          title: 'Address',
          content:
            addr &&
            `${addr.street.join(' ')}, ${addr.region?.label}, ${
              addr.country.label
            }`,
          href: '/checkout/information#address',
        },
        {
          title: 'Shipping Method',
          content:
            data?.cart?.shipping_addresses?.[0]?.selected_shipping_method
              ?.method_title,
          href: '/checkout/shipping',
        },
      ] as ReviewBlock[]
    ).filter((res) => res.content);
  }, [data]);

  if (loading) {
    return <>Loading</>;
  }

  return (
    <div
      className={css({
        border: '1px solid #e2e2e2',
        borderRadius: '10px',
        marginTop: '1rem',
      })}
    >
      {block.map((res, index) => {
        return (
          <div
            key={index}
            className={css({
              display: 'flex',
              padding: '.5rem 1rem',
              borderTop: index === 0 ? undefined : '1px solid #e2e2e2',
              alignItems: 'center',
            })}
          >
            <div
              className={css({
                display: 'flex',
                flexDirection: 'column',
                [$theme.mediaQuery.medium]: {
                  flexDirection: 'row',
                },
                width: '90%',
              })}
            >
              <div className={css({ width: '20%', flexGrow: 1 })}>
                <LabelSmall color="#666">{res.title}</LabelSmall>
              </div>
              <div
                className={css({
                  [$theme.mediaQuery.medium]: {
                    marginLeft: $theme.sizing.scale500,
                    width: `calc(75% - ${$theme.sizing.scale500})`,
                  },
                  marginLeft: 0,
                  width: '75% ',
                  flexGrow: 1,
                })}
              >
                <LabelSmall>{res.content}</LabelSmall>
              </div>
            </div>
            {res.href && (
              <Button
                kind={KIND.tertiary}
                size={SIZE.mini}
                $as={Link}
                to={res.href}
              >
                Change
              </Button>
            )}
          </div>
        );
      })}
    </div>
  );
}
