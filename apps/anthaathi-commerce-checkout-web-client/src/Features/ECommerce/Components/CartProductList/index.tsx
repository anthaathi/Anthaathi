import { useStyletron } from 'baseui';
import { Link } from 'react-router-dom';
import { LabelMedium } from 'baseui/typography';
import { gql } from '@apollo/client';
import { useRecoilValue } from 'recoil';
import { CartAtom } from '../../../../atoms';
import { GetCartProductListQuery } from '../../../../__generated__/graphql';
import { StyledLoadingIndicator } from 'baseui/select/styled-components';
import { useQuery } from '../../../Core/hooks/useSuspenseQuery';

export function CartProductList() {
  const [css, $theme] = useStyletron();

  const cart = useRecoilValue(CartAtom);

  const { data, loading, error } = useQuery<GetCartProductListQuery>(
    gql`
      query getCartProductList($cartId: String!) {
        cart(cart_id: $cartId) {
          items {
            prices {
              price_including_tax {
                currency
                value
              }
              discounts {
                amount {
                  currency
                  value
                }
                label
              }
            }
            quantity

            product {
              name
              image {
                url
              }
            }
          }
        }
      }
    `,
    {
      suspense: true,
      variables: {
        cartId: JSON.parse(cart?.value ?? ''),
      },
    },
  );

  if (loading) {
    return <StyledLoadingIndicator />;
  }

  return (
    <ul
      className={css({
        paddingLeft: 0,
        marginLeft: 0,
        listStyle: 'none',
        height: '320px',
        overflowY: 'auto',
        paddingTop: '.8rem',
        paddingRight: '.6rem',
        paddingBottom: '.6rem',
      })}
    >
      <li>
        {data?.cart?.items?.map((item, index) => (
          <Link
            key={index}
            className={css({
              textDecoration: 'none',
              color: '#000',
              display: 'flex',
              paddingBottom: $theme.sizing.scale600,
            })}
            to="/"
          >
            <div className={css({ position: 'relative' })}>
              <p
                className={css({
                  position: 'absolute',
                  top: '-24px',
                  right: '-12px',
                  borderRadius: '50%',
                  ...$theme.typography.LabelXSmall,
                  width: '21px',
                  height: '21px',
                  display: 'flex',
                  alignItems: 'center',
                  placeContent: 'center',
                  color: '#fff',
                  backgroundColor: '#7e7e7e',
                })}
              >
                {item?.quantity}
              </p>

              {item?.product.image?.url && (
                <img
                  src={item?.product.image?.url}
                  alt=""
                  className={css({
                    width: '68px',
                    borderRadius: '12px',
                  })}
                />
              )}
            </div>

            <div
              className={css({
                display: 'flex',
                flexDirection: 'column',
                placeContent: 'center',
                marginLeft: $theme.sizing.scale400,
                flexGrow: 1,
              })}
            >
              <LabelMedium>{item?.product.name}</LabelMedium>
            </div>

            <div
              className={css({
                display: 'flex',
                placeContent: 'center',
                alignItems: 'center',
              })}
            >
              <LabelMedium>
                {Intl.NumberFormat('en-US', {
                  style: 'currency',
                  currency: item?.prices?.price_including_tax.currency ?? 'AED',
                }).format(item?.prices?.price_including_tax.value ?? 0)}
              </LabelMedium>
            </div>
          </Link>
        ))}
      </li>
    </ul>
  );
}
