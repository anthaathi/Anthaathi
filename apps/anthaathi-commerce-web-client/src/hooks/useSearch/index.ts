import { client } from '~/utils/fetchGraphQL';
import { gql } from '@apollo/client';
import {
  SearchItemsQuery,
  SearchItemsQueryVariables,
} from '../../../operations-types';

export const useSearch = () => {
  async function getRecords(
    title: string,
  ) {
    const result = await client.query<
      SearchItemsQuery,
      SearchItemsQueryVariables
    >({
      query: gql`
        query searchItems($search: String!) {
          products(search: $search, pageSize: 25) {
            page_info {
              total_pages
              page_size
              current_page
            }
            items {
              id: uid
              title: name
              sku
              description: meta_description
              handle: url_key
              price: price_range {
                minimum_price {
                  final_price {
                    value
                    currency
                  }
                }
              }
              small_image {
                url
              }
            }
          }
        }
      `,
      variables: {
        search: title,
      },
    });

    return {
      records: result.data.products,
    };
  }

  return { getRecords };
};
