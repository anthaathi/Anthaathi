import {
  createHandler,
  renderStream,
  StartServer,
} from 'solid-start/entry-server';
import { Server } from 'styletron-engine-atomic';
import { StyletronProvider } from '@anthaathi/solid-styletron';
import { createLightTheme } from '~/utils/createLightTheme';
import { addServerTiming } from '~/utils/add-server-timing';
import { CartProvider } from '~/Features/Cart/Provider/CartProvider';
import Cookies from 'universal-cookie';

import { ApolloProvider } from '@merged/solid-apollo';
import { client as apolloClient } from './utils/fetchGraphQL';

const client = new Server({
  prefix: '_',
});

const theme = createLightTheme();

export default createHandler(
  renderStream((event) => {
    const time = new Date();

    const cook = new Cookies(event.request.headers);

    const root = (
      <StyletronProvider theme={theme} client={client}>
        <ApolloProvider client={apolloClient}>
          <CartProvider>
            <StartServer event={event} />
          </CartProvider>
        </ApolloProvider>
      </StyletronProvider>
    );

    const timeEnd = new Date();

    addServerTiming(
      event.responseHeaders,
      'app-render',
      timeEnd.getTime() - time.getTime(),
      'Render App',
    );

    return root;
  }),
);
