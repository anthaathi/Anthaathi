import { useStyletron } from '@anthaathi/solid-styletron';
import { CartQuantityChange } from '~/Features/Commerce/Components/CartQuantityChange';
import { Img } from '~/Features/Core/Components/Image';
import { ProductProps } from '../ProductTile';
import { useCheckout } from '~/Features/Cart/Provider/CartProvider';
import { TSProductProps } from '~/Features/Core/Components/Searchbar';
import { useCart } from '~/Features/Cart/Hooks';
import debounce from 'lodash.debounce';
import { createMemo, createSignal } from 'solid-js';

export interface CartItemProps {
  product: any;
  numberOfItems: number;
}

export function CartItem(props: CartItemProps) {
  const [css, $theme] = useStyletron();
  const { updateProductToCart, addProductToCart } = useCart();
  const [error, setError] = createSignal(false);

  const packaging = createMemo(() => {
    return props.product.product.custom_attributes.find(
      (el: any) => el.attribute_metadata.code === 'packaging',
    ).entered_attribute_value.value;
  }, [props.product.product]);

  return (
    <div
      class={css({
        display: 'flex',
        height: error() ? '118px' : '107px',
        flexDirection: 'row',
        borderBottom: '1px solid #d9d9d9',
        paddingBottom: '20px',
        marginBottom: '20px',
      })}
    >
      <div
        class={css({
          width: '100%',
          maxWidth: '120px',
          display: 'none',
          [$theme.mediaQuery?.sm || '']: {
            display: 'block',
          },
        })}
      >
        <Img
          src={props.product.product.image.url}
          $override={{
            Root: {
              $style: {
                width: '100%',
                height: '100%',
                objectFit: 'cover',
                border: '1px solid #e4e4d9',
              },
            },
          }}
        />
      </div>
      <div
        class={css({
          display: 'flex',
          flex: 6,
          marginLeft: $theme.sizing.scale600,
          flexDirection: 'column',
          justifyContent: 'space-evenly',
          alignItems: 'flex-start',
          lineHeight: '1.5',
          letterSpacing: '0.05em',
        })}
      >
        <div
          class={css({
            ...$theme.typography.HeadingXSmall,
            paddingBottom: $theme.sizing.scale400,
          })}
        >
          {props.product.product.name}
        </div>
        <div
          class={css({
            ...$theme.typography.LabelMedium,
            paddingBottom: $theme.sizing.scale400,
            marginTop: 0,
            marginBottom: $theme.sizing.scale100,
          })}
        >
          {`${Intl.NumberFormat('en-US', {
            style: 'currency',
            currency:
              props.product.product?.price.minimum_price.final_price.currency,
          }).format(
            props.product.product?.price.minimum_price.final_price.value,
          )} / ${packaging()}`}
        </div>

        <div
          class={css({
            paddingBottom: $theme.sizing.scale400,
            display: 'flex',
          })}
        >
          <CartQuantityChange
            packaingKG={packaging() === 'KG'}
            trashIcon
            initialValue={props.numberOfItems}
            onChangeQuantity={async (qty) => {
              if (props.numberOfItems > qty) {
                await updateProductToCart(props.product.uid, qty + 1);
                if (error()) {
                  setError(false);
                }
              } else {
                const data = await addProductToCart(
                  props.product.product.sku,
                  packaging() === 'KG' ? 0.25 : 1,
                );
                if (error() || props.numberOfItems === qty) {
                  setError(false);
                }
                setError(data === null && props.numberOfItems < qty);
              }
            }}
            id={props.product.product.sku}
          />
        </div>
        {error() && (
          <div
            class={css({
              ...$theme.typography.LabelSmall,
              paddingBottom: $theme.sizing.scale200,
              marginTop: 0,
              color: 'red',
              marginBottom: $theme.sizing.scale100,
            })}
          >
            Could not update the product: The requested qty is not available
          </div>
        )}
      </div>
      <div
        class={css({
          display: 'flex',
          justifyContent: 'flex-end',
          alignItems: 'center',
        })}
      >
        <div
          class={css({
            marginRight: $theme.sizing.scale700,
            fontSize: '18px',
            fontWeight: 700,
          })}
        >
          {Intl.NumberFormat('en-US', {
            style: 'currency',
            currency:
              props.product.product?.price.minimum_price.final_price.currency,
          }).format(
            props.product.product?.price.minimum_price.final_price.value,
          )}
        </div>
      </div>
    </div>
  );
}
