import { useStyletron } from '@anthaathi/solid-styletron';
import { Button, Kind, Size } from '~/Features/Core/Components/Button';
import {
  IconHeartOLarge,
  IconMinusSmall,
  IconSearchLarge,
} from '@anthaathi/oracle-apex-solid-icons';
import { createEffect, createMemo, createSignal } from 'solid-js';
import { useNavigate } from '@solidjs/router';
import { Img } from '~/Features/Core/Components/Image';
import debounce from 'lodash.debounce';
import { cartItemStore, useCart } from '~/Features/Cart/Hooks';
import toast from 'solid-toast';

export interface ProductProps {
  id: number;
  name: string;
  name_ar?: string;
  description?: string;
  category?: string;
  price: string;
  currency: string;
  image: string;
  weight_unit?: string;
  packaging?: string;
  key?: string;
  notes?: string;
  salableStock?: number;
}

export function ProductTile(props: any) {
  const [css, $theme] = useStyletron();
  const [isOpen, setIsOpen] = createSignal(false);
  const [loader, setLoader] = createSignal(false);
  const navigate = useNavigate();
  const [cart] = cartItemStore;

  const { updateProductToCart, addProductToCart } = useCart();

  const cartProductData = createMemo(() => {
    if (cart.some((el) => el.product.sku === props.sku)) {
      return cart.find((el: any) => el.product.sku === props.sku);
    }

    return {
      product: props,
    };
  }, [cart, props.sku]);

  const _quantity = cartProductData()?.quantity;

  const [quantity, setQuantity] = createSignal(_quantity);
  const [prevValue, setPrevValue] = createSignal(_quantity);

  async function increaseSizeInServer_() {
    const delta = quantity() - (prevValue() ?? 0);

    setLoader(true);
    await addProductToCart(props.sku, delta ?? 1).then((docs) => {
      if (docs) {
        const newValue =
          docs.addSimpleProductsToCart?.cart?.items?.find(
            (res: any) => res?.product?.sku === props.sku,
          )?.quantity ?? prevValue();

        setQuantity(newValue);
        setPrevValue(newValue);
      } else {
        if (isNaN(prevValue())) {
          setQuantity(0);
        } else {
          setQuantity(prevValue());
        }
      }
    });
    setLoader(false);
  }

  const increaseSizeInServer = debounce(increaseSizeInServer_, 400);

  const decreaseSizeInServer = debounce(async () => {
    if (!cartProductData().uid) {
      return;
    }

    setLoader(true);

    await updateProductToCart(
      cartProductData().uid,
      (quantity() ?? 0) + 1,
    ).then((docs) => {
      if (docs) {
        setPrevValue(quantity() ?? 0);
        setQuantity(quantity() ?? 0);
      }
    });
    setLoader(false);
  }, 400);

  createEffect(() => {
    setQuantity(cartProductData()?.quantity ?? 0);
  });

  function getReduceQuantity() {
    return debounce(() => {
      if (!loader()) {
        setQuantity((prev) =>
          props.packaging === 'KG' && prev > 1 ? prev - 0.25 : prev - 1,
        );
        decreaseSizeInServer();
      }
    }, 50);
  }

  function getOnIncreaseQuantity() {
    return debounce((e: Event) => {
      const datasetElement = (e.target as HTMLDivElement).closest('button')
        ?.dataset['action'];

      switch (datasetElement) {
        case 'view-product':
          navigate('/product/' + props.handle);
          return;
        case 'reduce-quantity':
          return;
        case 'ratings':
          return;
        case '':
          return;
      }
      
      if (!loader()) {
        if (quantity() < props.salableStock) {
          setQuantity((prev) =>
            props.packaging === 'KG' && prev >= 1 ? prev + 0.25 : prev + 1,
          );

          increaseSizeInServer();
        } else {
          toast.error(
            `${props.name} stock: The requested qty is not available`,
          );
        }
      }
    }, 50);
  }

  const onIncreaseQuantity = getOnIncreaseQuantity();

  return (
    <div
      onClick={onIncreaseQuantity}
      class={css({
        textDecoration: 'none',
        color: '#000',
        backgroundColor: '#fff',
        borderBottomLeftRadius: '10px',
        borderBottomRightRadius: '10px',
        borderTopRightRadius: '10px',
        borderTopLeftRadius: '10px',
        userSelect: 'none',
        border: '1px solid #e4e4d9',
        ':hover': {
          boxShadow:
            '0 1px 1px 0 rgba(66, 66, 66, 0.08), 0 1px 3px 1px rgba(66, 66, 66, 0.16)',
        },
      })}
      onTouchEnd={onIncreaseQuantity}
    >
      <div
        class={css({
          width: 'auto',
          position: 'relative',
          height: `calc(100% - ${$theme.sizing.scale600})`,
          cursor: 'pointer',
          paddingLeft: $theme.sizing.scale600,
          paddingRight: $theme.sizing.scale600,
          paddingTop: $theme.sizing.scale600,
          [$theme.mediaQuery?.md || '']: {
            height: `calc(100% - ${$theme.sizing.scale800})`,
            paddingLeft: $theme.sizing.scale800,
            paddingRight: $theme.sizing.scale800,
            paddingTop: $theme.sizing.scale800,
          },
        })}
        onMouseOver={() => setIsOpen(true)}
        onMouseOut={() => setIsOpen(false)}
      >
        <div
          class={css({
            position: 'absolute',
            left: '5px',
            top: '14px',
            opacity:
              (cartProductData() &&
                cartProductData()?.product?.sku === props.sku &&
                quantity() !== 0) ||
              isOpen()
                ? 1
                : 0,
            transitionTimingFunction: 'ease',
            transitionDuration: '100ms',
            transitionProperty: 'opacity',
            zIndex: 1,
            display: 'block',
          })}
        >
          <Button
            $override={{
              Root: {
                style: {
                  position: 'absolute',
                  opacity:
                    cartProductData() &&
                    cartProductData()?.product?.sku === props.sku &&
                    quantity() !== 0
                      ? 1
                      : 0,
                  userSelect:
                    cartProductData() &&
                    cartProductData()?.product?.sku === props.sku &&
                    quantity() !== 0
                      ? 'element'
                      : 'none',
                  display:
                    cartProductData() &&
                    cartProductData()?.product?.sku === props.sku &&
                    quantity() !== 0
                      ? 'block'
                      : 'none',
                  paddingLeft: '10px',
                  paddingRight: '10px',
                  paddingTop: '10px',
                  paddingBottom: '10px',
                  borderTopRightRadius: '50%',
                  borderTopLeftRadius: '50%',
                  borderBottomLeftRadius: '50%',
                  borderBottomRightRadius: '50%',
                  marginBottom: '12px',
                  border: '1px solid #ea3323',
                  background: '#ea3323',
                  ':hover': {
                    background: '#ea3323',
                  },
                },
              },
            }}
            data-action="reduce-quantity"
            onClick={getReduceQuantity()}
            $size={Size.Mini}
            $kind={Kind.Secondary}
            $startEnhancer={() => (
              <IconMinusSmall width="20px" height="20px" fill="#fff" />
            )}
          />
        </div>
        <div
          class={css({
            position: 'absolute',
            right: '5px',
            top: '14px',
            opacity:
              (cartProductData() &&
                cartProductData()?.product?.sku === props.sku &&
                quantity() !== 0) ||
              isOpen()
                ? 1
                : 0,
            transitionTimingFunction: 'ease',
            transitionDuration: '100ms',
            transitionProperty: 'opacity',
            zIndex: 1,
            display: 'block',
          })}
        >
          <Button
            $override={{
              Root: {
                style: {
                  opacity:
                    cartProductData() &&
                    cartProductData()?.product?.sku === props.sku &&
                    quantity() !== 0
                      ? 1
                      : 0,
                  userSelect:
                    cartProductData() &&
                    cartProductData()?.product?.sku === props.sku &&
                    quantity() !== 0
                      ? 'element'
                      : 'none',
                  display:
                    cartProductData() &&
                    cartProductData()?.product?.sku === props.sku &&
                    quantity() !== 0
                      ? 'block'
                      : 'none',
                  paddingLeft: '10px',
                  paddingRight: '10px',
                  paddingTop: '10px',
                  paddingBottom: '10px',
                  borderTopRightRadius: '50%',
                  borderTopLeftRadius: '50%',
                  borderBottomLeftRadius: '50%',
                  borderBottomRightRadius: '50%',
                  marginBottom: '12px',
                  background: '#108943',
                  ':hover': {
                    background: '#108943',
                  },
                },
              },
            }}
            $size={Size.Mini}
            $kind={Kind.Secondary}
            $startEnhancer={() => (
              <div
                class={css([
                  $theme.typography.LabelLarge,
                  {
                    marginTop: 0,
                    marginBottom: 0,
                    color: '#fff',
                    height: '20px',
                    width: '20px',
                    alignItems: 'center',
                    display: 'flex',
                    justifyContent: 'center',
                  },
                ])}
              >
                {quantity}
              </div>
            )}
          />
          <Button
            $override={{
              Root: {
                style: {
                  opacity:
                    (cartProductData() &&
                      cartProductData()?.product?.sku === props.sku &&
                      quantity() !== 0) ||
                    isOpen()
                      ? 1
                      : 0,
                  paddingLeft: '10px',
                  paddingRight: '10px',
                  paddingTop: '10px',
                  paddingBottom: '10px',
                  borderTopRightRadius: '50%',
                  borderTopLeftRadius: '50%',
                  borderBottomLeftRadius: '50%',
                  borderBottomRightRadius: '50%',
                  marginBottom: '12px',
                  border: '1px solid #e4e4d9',
                  ':hover': {
                    background: '#E5E5EA',
                  },
                },
              },
            }}
            onClick={() => {
              navigate('/product/' + props.handle);
            }}
            data-action="view-product"
            $size={Size.Mini}
            $kind={Kind.Secondary}
            $startEnhancer={() => (
              <IconSearchLarge width="20px" height="20px" />
            )}
          />
          <Button
            $override={{
              Root: {
                style: {
                  opacity:
                    (cartProductData() &&
                      cartProductData()?.product?.sku === props.sku &&
                      quantity() !== 0) ||
                    isOpen()
                      ? 1
                      : 0,
                  paddingLeft: '10px',
                  paddingRight: '10px',
                  paddingTop: '10px',
                  paddingBottom: '10px',
                  borderTopRightRadius: '50%',
                  borderTopLeftRadius: '50%',
                  borderBottomLeftRadius: '50%',
                  borderBottomRightRadius: '50%',
                  marginBottom: '12px',
                  border: '1px solid #e4e4d9',
                  ':hover': {
                    background: '#E5E5EA',
                  },
                },
              },
            }}
            $size={Size.Mini}
            data-action="ratings"
            $kind={Kind.Secondary}
            $startEnhancer={() => (
              <IconHeartOLarge width="20px" height="20px" />
            )}
          />
        </div>

        <div
          class={css({
            width: '100%',
          })}
        >
          <Img
            src={props.image}
            alt=""
            $override={{
              Root: {
                $style: {
                  width: '100%',
                  flexGrow: 1,
                  objectFit: 'cover',
                  [$theme.mediaQuery?.md || '']: {
                    width: `calc(100% - ${$theme.sizing.scale400} - ${$theme.sizing.scale400})`,
                    paddingLeft: $theme.sizing.scale400,
                    paddingRight: $theme.sizing.scale400,
                  },
                },
              },
            }}
          />
          <h4
            class={css([
              $theme.typography.HeadingXSmall,
              {
                marginBottom: $theme.sizing.scale200,
                marginTop: $theme.sizing.scale800,
              },
            ])}
          >
            {props.title}
          </h4>

          <h5
            class={css([
              $theme.typography.ParagraphSmall,
              {
                marginTop: $theme.sizing.scale100,
                marginBottom: 0,
                color: '#858585',
              },
            ])}
          >
            {props.shortDescription}
          </h5>

          <h5
            class={css([
              $theme.typography.ParagraphLarge,
              {
                marginTop: 0,
                marginBottom: $theme.sizing.scale800,
                fontWeight: 'bold',
              },
            ])}
          >
            {`${
              Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: props.currency,
              }).format(+props.price) + props.weight
            } / ${props.packaging}`}
          </h5>

          {props.salableStock === 0 && (
            <h5
              class={css([
                $theme.typography.ParagraphSmall,
                {
                  marginTop: $theme.sizing.scale100,
                  marginBottom: 0,
                  color: 'red',
                },
              ])}
            >
              Out of stock
            </h5>
          )}
        </div>
      </div>
    </div>
  );
}
