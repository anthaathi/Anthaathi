import { useStyletron } from '@anthaathi/solid-styletron';
import { Button, Kind } from '~/Features/Core/Components/Button';
import { Link } from '@solidjs/router';
import { Grid } from '~/Features/Core/Components/Grid';
import { createMemo, For, Show } from 'solid-js';
import { ProductTile } from '~/Features/Commerce/Components/ProductTile';
import { ProductInformationFragment } from '../../../../../operations-types';
import { TSProductProps } from '~/Features/Core/Components/Searchbar';

export function FeaturedCollection(props: { title: string; products: any[] }) {
  const [css, $theme] = useStyletron();

  return (
    <div
      class={css({
        margin: '0 auto',
        width: $theme.sizing.maxWidth,
        maxWidth: `calc(100% - ${$theme.sizing.scale500} - ${$theme.sizing.scale500})`,
        paddingLeft: $theme.sizing.scale500,
        paddingRight: $theme.sizing.scale500,
      })}
    >
      <div class={css({ display: 'flex', alignItems: 'center' })}>
        <h4
          class={css([
            $theme.typography.HeadingMedium,
            {
              marginTop: $theme.sizing.scale400,
              marginBottom: $theme.sizing.scale400,
            },
          ])}
        >
          {props.title}
        </h4>
        <span class={css({ flexGrow: 1 })} />

        <Button $as={Link} $kind={Kind.Tertiary} href="/collections/delmonte">
          View all
        </Button>
      </div>
      <Show when={props.products.length > 0}>
        <Grid
          $override={{
            Root: {
              style: {
                marginBottom: '12px',
                gridGap: '8px',
                [$theme.mediaQuery?.md || '']: {
                  gridGap: '28px',
                },
              },
            },
          }}
          columns={[2, 4, 4, 5]}
        >
          <For each={props.products}>
            {(product: any) => {
              const packaging = createMemo(() => {
                return product.custom_attributes.find(
                  (el: any) => el.attribute_metadata.code === 'packaging',
                ).entered_attribute_value.value;
              }, [product]);
              
              return (
                <ProductTile
                  price={
                    product?.price.minimum_price.final_price.value?.toString() ??
                    ''
                  }
                  currency={
                    product?.price.minimum_price?.final_price?.currency ?? 'AED'
                  }
                  sku={product?.sku ?? ''}
                  title={product?.title ?? product?.name ?? ''}
                  image={product?.image?.url ?? ''}
                  id={product?.id ?? ''}
                  handle={(product?.url_key ?? '') + '.html'}
                  description={product?.description ?? ''}
                  shortDescription={product?.short_description.html ?? ''}
                  weight={product?.weight ?? ''}
                  packaging={packaging()}
                  salableStock={product?.salableStock}
                />
              );
            }}
          </For>
        </Grid>
      </Show>
    </div>
  );
}
