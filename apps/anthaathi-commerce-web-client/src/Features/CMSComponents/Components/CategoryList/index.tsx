import {
  IconArrowLeftLarge,
  IconArrowRightLarge,
} from '@anthaathi/oracle-apex-solid-icons';
import { useStyletron } from '@anthaathi/solid-styletron';
import { Link } from '@solidjs/router';
import { createSignal, For } from 'solid-js';
import { Grid } from '~/Features/Core/Components/Grid';
import { Img } from '~/Features/Core/Components/Image';
import { useCarousel } from '~/hooks/useCarousel';

import { Button, Kind } from '~/Features/Core/Components/Button';

export interface CategoryDetailsProps {
  id: string;
  title_ar: string;
  title: string;
  image: string;
  key: string;
  href: string;
  srcSet: string[];
}

export interface CategoryListProps {
  title?: string;
  items: CategoryDetailsProps[];
  handlePress?: (key: string) => void;
}

export function CategoryList(props: CategoryListProps) {
  const [css, $theme] = useStyletron();
  let divRef: HTMLDivElement | null;

  const { scrollNext, scrollPrevious } = useCarousel(() => divRef, true);

  return (
    <div
      class={css({
        maxWidth: $theme.sizing.maxWidth,
        margin: '0 auto',
        width: `calc(100% - ${$theme.sizing.scale500} - ${$theme.sizing.scale500})`,
      })}
    >
      {props.title && (
        <h5
          class={css([
            $theme.typography.HeadingMedium,
            {
              marginTop: $theme.sizing.scale600,
              marginBottom: $theme.sizing.scale600,
              fontWeight: 'bold',
              color: '#000',
            },
          ])}
        >
          {props.title}
        </h5>
      )}
      <div
        class={css({
          marginTop: $theme.sizing.scale700,
          marginBottom: $theme.sizing.scale700,
          display: 'flex',
          flexDirection: 'row',
          position: 'relative',
        })}
      >
        <div
          class={css({
            position: 'absolute',
            [$theme.mediaQuery?.md || '']: {
              top: '42px',
              left: '36px',
              display: 'flex',
              opacity: 0.8,
              transitionProperty: 'opacity',
              transitionDuration: '.2s',
              transitionTimingFunction: 'ease',
              ':hover': {
                opacity: 1,
              },
            },
            display: 'none',
            zIndex: 1,
          })}
        >
          <Button
            $kind={Kind.Secondary}
            $override={{
              Root: {
                style: {
                  borderTopLeftRadius: '50%',
                  borderTopRightRadius: '50%',
                  borderBottomLeftRadius: '50%',
                  borderBottomRightRadius: '50%',
                  paddingLeft: '12px',
                  paddingRight: '12px',
                  paddingTop: '12px',
                  paddingBottom: '12px',
                  marginLeft: $theme.sizing.scale200,
                  marginRight: $theme.sizing.scale200,
                },
              },
            }}
            $startEnhancer={() => (
              <IconArrowLeftLarge width="21px" height="21px" />
            )}
            onClick={() => {
              scrollPrevious();
            }}
          />
        </div>
        <div
          ref={(ref) => (divRef = ref)}
          class={css({
            display: 'flex',
            overflowY: 'auto',
            scrollbarWidth: 'none',
            [$theme.mediaQuery?.md || '']: {
              overflowY: 'hidden',
              flexWrap: 'nowrap',
              scrollSnapType: 'x mandatory',
              flexDirection: 'row',
              '::-webkit-scrollbar': {
                display: 'none',
              },
              scrollBehavior: 'smooth',
              marginTop: $theme.sizing.scale700,
              marginBottom: $theme.sizing.scale700,
            },
          })}
        >
          <For each={props.items}>
            {(item) => <CategoryDetails item={item} />}
          </For>
        </div>
        <div
          class={css({
            position: 'absolute',
            [$theme.mediaQuery?.md || '']: {
              top: '42px',
              right: '36px',
              display: 'flex',
              opacity: 0.8,
              transitionProperty: 'opacity',
              transitionDuration: '.2s',
              transitionTimingFunction: 'ease',
              ':hover': {
                opacity: 1,
              },
            },
            display: 'none',
            top: '26px',
            right: '20px',
            zIndex: 1,
          })}
        >
          <Button
            $kind={Kind.Secondary}
            $override={{
              Root: {
                style: {
                  borderTopLeftRadius: '50%',
                  borderTopRightRadius: '50%',
                  borderBottomLeftRadius: '50%',
                  borderBottomRightRadius: '50%',
                  paddingLeft: '12px',
                  paddingRight: '12px',
                  paddingTop: '12px',
                  paddingBottom: '12px',
                  marginLeft: $theme.sizing.scale200,
                  marginRight: $theme.sizing.scale200,
                },
              },
            }}
            onClick={() => scrollNext()}
            $startEnhancer={() => (
              <IconArrowRightLarge width="21px" height="21px" />
            )}
          />
        </div>
      </div>
    </div>
  );
}

const CategoryDetails = ({ item }: { item: CategoryDetailsProps }) => {
  const [css, $theme] = useStyletron();
  return (
    <Link
      href={item.href}
      class={css({
        textDecoration: 'none',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column',
        marginLeft: $theme.sizing.scale600,
        marginRight: $theme.sizing.scale600,
      })}
    >
      {item.image ? (
        <Img
          src={item.image}
          srcSet={item.srcSet.join(', ')}
          $override={{
            Root: {
              $style: {
                marginTop: '10px',
                marginBottom: '0px',
                marginLeft: '0px',
                marginRight: '0px',
                objectFit: 'cover',
                [$theme.mediaQuery?.md || '']: {
                  width: '104px',
                  height: '104px',
                },
                width: '72px',
                height: '72px',
                borderRadius: '50%',
                ':hover': {
                  animation: 'fade-in-show .4s',
                  transform: 'scale(1.03)',
                },
              },
            },
          }}
        />
      ) : (
        <div
          class={css({
            marginTop: '10px',
            marginBottom: '0px',
            marginLeft: '0px',
            marginRight: '0px',
            backgroundColor: '#efefef',
            [$theme.mediaQuery?.md || '']: {
              width: '104px',
              height: '104px',
            },
            width: '72px',
            height: '72px',
            borderRadius: '50%',
            ':hover': {
              animation: 'fade-in-show .4s',
              transform: 'scale(1.03)',
            },
          })}
        />
      )}
      <h5
        class={css([
          $theme.typography.ParagraphLarge,
          {
            marginTop: $theme.sizing.scale600,
            marginBottom: $theme.sizing.scale500,
            textAlign: 'center',
            fontWeight: 'bold',
            color: '#000',
          },
        ])}
      >
        {item.title}
      </h5>
    </Link>
  );
};
