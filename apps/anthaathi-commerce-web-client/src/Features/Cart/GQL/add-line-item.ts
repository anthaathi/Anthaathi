import { client } from '~/utils/fetchGraphQL';
import { gql } from '@apollo/client';
import Cookies from 'js-cookie';
import {
  AddLineItemsMutation,
  AddLineItemsMutationVariables,
  CheckoutLineItemInput,
  CheckoutLineItemUpdateInput, GetCheckoutMutation, GetCheckoutMutationVariables,
} from '../../../../operations-types';

export const getCheckoutId = () => Cookies.get('checkout_id');
export const setCheckoutId = (input: string) => Cookies.set('checkout_id', input);


type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;

export async function addLineItem(
  item: (Optional<CheckoutLineItemUpdateInput, 'id'>)[],
): Promise<any> {
  const checkoutId = getCheckoutId();
}
