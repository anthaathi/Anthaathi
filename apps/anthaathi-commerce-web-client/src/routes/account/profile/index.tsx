import { AccountDashBoard } from '~/Features/Commerce/Components/AccountDashboard';

export default function DashboardPage() {
  return <AccountDashBoard />;
}
