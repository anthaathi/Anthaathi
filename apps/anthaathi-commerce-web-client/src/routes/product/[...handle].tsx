import { FeaturedProduct } from '~/Features/CMSComponents/Components/FeaturedProduct';
import { Breadcrumbs } from '~/Features/Core/Components/Breadcrumbs';
import { useStyletron } from '@anthaathi/solid-styletron';
import { useParams, useRouteData } from '@solidjs/router';
import { createRouteData, refetchRouteData, RouteDataArgs } from 'solid-start';
import { client } from '~/utils/fetchGraphQL';
import { gql } from '@apollo/client';
import { createEffect, createMemo, createSignal, Show } from 'solid-js';
import { FeaturedCollection } from '~/Features/CMSComponents/Components/FeaturedCollection';
import {
  GetProductByHandleQuery,
  GetProductByHandleQueryVariables,
  ProductInformationFragment,
} from '../../../operations-types';
import { createLazyQuery } from '@merged/solid-apollo';
import { getCollectionQuery } from '~/Features/CMSComponents';

export const routeData = ({ params }: RouteDataArgs) => {
  const handleData = createRouteData(
    async () => {
      return client
        .query<GetProductByHandleQuery, GetProductByHandleQueryVariables>({
          query: gql`
            fragment ProductInformation on SimpleProduct {
              title: meta_title
              name
              description {
                html
              }
              sku
              meta_keyword
              uid
              short_description {
                html
              }
              custom_attributes {
                attribute_metadata {
                  label
                  code
                }
                entered_attribute_value {
                  value
                }
              }
              country_of_manufacture
              categories {
                meta_title
                url_key
              }
              media_gallery {
                url
              }
              url_key
              price_range {
                maximum_price {
                  final_price {
                    value
                    currency
                  }
                }
                minimum_price {
                  final_price {
                    currency
                    value
                  }
                }
              }
            }

            query getProductByHandle($handle: String!) {
              product: route(url: $handle) {
                type
                relative_url
                ... on SimpleProduct {
                  ...ProductInformation
                  related_products {
                    ...ProductInformation
                  }
                  crosssell_products {
                    ...ProductInformation
                  }
                }
              }
            }
          `,
          variables: {
            handle: '/' + params.handle,
          },
        })
        .then(
          (docs) =>
            docs.data.product as ProductInformationFragment &
              GetProductByHandleQuery['product'],
        );
    },
    {
      key: params.handle,
    },
  );

  return { product: handleData };
};

export default function ProductPage() {
  const { product: product$ } = useRouteData<typeof routeData>();

  const [css, $theme] = useStyletron();
  const params = useParams<{ handle: string }>();
  const [loadCollectionProduct] = createLazyQuery(getCollectionQuery);
  const [list1, setList1] = createSignal([]);
  const [list2, setList2] = createSignal([]);

  async function getFeaturedCollectionProducts() {
    const data: any = await loadCollectionProduct({
      variables: {
        handle: 'fruits',
      },
    });
    setList1(data?.categories.items[0].products.items);
    const data1: any = await loadCollectionProduct({
      variables: {
        handle: 'juices',
      },
    });
    setList2(data1?.categories.items[0].products.items);
  }

  createEffect(() => {
    getFeaturedCollectionProducts();
  });

  const packaging = createMemo(() => {
    return product$()?.custom_attributes.find(
      (el: any) => el.attribute_metadata.code === 'packaging',
    ).entered_attribute_value.value;
  }, [product$()]);

  createEffect(() => {
    const _a = params.handle;
    refetchRouteData();
  });

  return (
    <Show
      when={product$()}
      keyed={true}
      fallback={
        <div
          class={css({
            maxWidth: $theme.sizing.maxWidth,
            margin: '12px auto',
          })}
        >
          Product not found
        </div>
      }
    >
      <Breadcrumbs
        list={[
          { key: '1', title: 'Home', link: '/' },
          {
            key: '3',
            title: product$()?.title ?? '',
            link: '/',
          },
        ]}
      />
      <FeaturedProduct
        productInfo={{
          id: product$()?.uid!!,
          name: product$()?.name || '',
          sku: product$()?.sku || '',
          listInfo: {
            description: product$()?.description?.html || '',
            shippingInformation: 'Shipping Information',
          },
          blockInfo: {
            freeShipping: 'Free shipping in UAE',
            inStock: 'In stock, ready to ship',
            securePayments: 'Secure Payments',
            isFresh: 'Fresh',
          },
          price:
            product$()?.price_range?.minimum_price?.final_price?.value?.toString() ||
            '0',
          currency:
            product$()?.price_range.minimum_price.final_price?.currency!,
          image: product$()?.media_gallery?.map((res) => res?.url!!)!!,
          notes: product$()?.short_description.html,
          weight: product$()?.weight,
          packaging: packaging(),
        }}
      />

      <FeaturedCollection title="Related Products" products={list1()} />

      <FeaturedCollection title="Recommended Products" products={list2()} />

      <div class={css({ paddingBottom: '12px' })} />
    </Show>
  );
}
