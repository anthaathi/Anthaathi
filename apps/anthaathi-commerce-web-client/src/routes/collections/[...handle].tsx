import { useStyletron } from '@anthaathi/solid-styletron';
import { createEffect, createMemo, createSignal, For, Show } from 'solid-js';
import { useParams, useRouteData, useSearchParams } from '@solidjs/router';
import { createRouteData, RouteDataArgs } from 'solid-start';
import { client } from '~/utils/fetchGraphQL';
import { gql } from '@apollo/client';
import { Breadcrumbs } from '~/Features/Core/Components/Breadcrumbs';
import {
  GetAllCategoriesOnCategoriesPageQuery,
  GetAllCategoriesOnCategoriesPageQueryVariables,
  GetCategoryByHandleQuery,
  GetCategoryByHandleQueryVariables,
} from '../../../operations-types';
import { CategoryList } from '~/Features/CMSComponents/Components/CategoryList';
import { ProductTile } from '~/Features/Commerce/Components/ProductTile';
import { Grid } from '~/Features/Core/Components/Grid';
import { Button, Size } from '~/Features/Core/Components/Button';

function getCategoryProductByHandle(handle: string, currentPage = 1) {
  return client
    .query<GetCategoryByHandleQuery, GetCategoryByHandleQueryVariables>({
      query: gql`
        query getCategoryByHandle($handle: String!, $currentPage: Int) {
          categories(filters: { url_key: { eq: $handle } }) {
            page_info {
              current_page
              page_size
              total_pages
            }
            items {
              uid
              ... on CategoryTree {
                meta_title
                products(currentPage: $currentPage, pageSize: 25) {
                  items {
                    id: uid
                    name
                    sku
                    title: meta_title
                    description: meta_description
                    url_key
                    salableStock
                    custom_attributes {
                      attribute_metadata {
                        label
                        code
                      }
                      entered_attribute_value {
                        value
                      }
                    }
                    country_of_manufacture
                    short_description {
                      html
                    }
                    ... on PhysicalProductInterface {
                      weight
                    }
                    price: price_range {
                      minimum_price {
                        discount {
                          amount_off
                          percent_off
                        }
                        regular_price {
                          currency
                          value
                        }
                        final_price {
                          value
                          currency
                        }
                      }
                    }
                    image {
                      url
                    }
                  }
                  page_info {
                    page_size
                    current_page
                    total_pages
                  }
                  total_count
                }
              }
            }
          }
        }
      `,
      variables: {
        handle: handle,
        currentPage: currentPage,
      },
    })
    .then((docs) => docs?.data?.categories);
}

export const routeData = ({ params, location }: RouteDataArgs) => {
  const category = createRouteData(
    async (key) => {
      return {
        data: await getCategoryProductByHandle(
          params.handle,
          +(location.query.page || '1'),
        ),
        handle: params.handle,
        page: +(location.query.page || '1'),
      };
    },
    {
      key: params.handle + (location.query.page || '1'),
    },
  );

  const categoriesList = createRouteData(
    () => {
      return client
        .query<
          GetAllCategoriesOnCategoriesPageQuery,
          GetAllCategoriesOnCategoriesPageQueryVariables
        >({
          query: gql`
            query GetAllCategoriesOnCategoriesPage {
              categories: categoryList(
                filters: { parent_category_uid: { eq: "Mg==" } }
              ) {
                original_title: name
                title: meta_title
                description: meta_description
                url: image
                id: uid
                position
                href: url_key
              }
            }
          `,
        })
        .then((docs) => docs.data);
    },
    {
      key: 'categories',
    },
  );

  return {
    category: category,
    categoriesList: categoriesList,
  };
};

export default function () {
  const { category: productList, categoriesList } =
    useRouteData<typeof routeData>();

  const [css, $theme] = useStyletron();

  const params = useParams();

  const [queryParams, setQueryParams] = useSearchParams<{ page: string }>();

  const [data, setNewData] = createSignal(productList());
  const currentPage = createMemo(() => {
    return +(queryParams.page || '1');
  });

  createEffect(() => {
    if (data()?.handle !== params.handle || data()?.page !== currentPage()) {
      const currentPage = +(queryParams.page || 1);
      const handle = params.handle;

      setIsLoading(true);

      getCategoryProductByHandle(handle, currentPage)
        .then((docs) => {
          const condition =
            handle !== params.handle ||
            +(queryParams.page || 1) !== currentPage;

          if (condition) {
            return;
          }

          setIsLoading(false);

          return setNewData(() => ({
            data: docs!!,
            page: currentPage!!,
            handle: handle!!,
          }));
        })
        .catch(() => {
          setIsLoading(false);
        });
    }
  });

  const [isLoading, setIsLoading] = createSignal(false);

  return (
    <>
      <div
        class={css({
          display: 'flex',
          marginTop: $theme.sizing.scale500,
          marginBottom: $theme.sizing.scale500,
        })}
        data-type="plp-page"
      >
        <Breadcrumbs
          list={[
            { key: '1', title: 'Home', link: '/' },
            { key: '2', title: 'Collections', link: '/collections' },
          ]}
        />
      </div>
      <CategoryList
        items={
          categoriesList()
            ?.categories?.filter((res) => Boolean(res))
            .map((res) => ({
              key: res!!.id,
              title: res!!.title!! ?? res!!.original_title,
              image: res!!.url!!,
              href: `/collections/${res!!.href}`,
              id: res!!.id,
              srcSet: [],
              title_ar: '',
            })) || []
        }
      />
      <div
        class={css({
          maxWidth: $theme.sizing.maxWidth,
          margin: '0 auto',
          width: `calc(100% - ${$theme.sizing.scale500} - ${$theme.sizing.scale500})`,
          display: 'flex',
          [$theme.mediaQuery?.md || '']: {
            flexDirection: 'row',
          },
          flexDirection: 'column',
        })}
      >
        <div
          class={css({
            [$theme.mediaQuery?.md || '']: {
              flexShrink: 0,
              width: '100%',
            },
            flex: 8,
            paddingBottom: $theme.sizing.scale1200,
            paddingLeft: $theme.sizing.scale500,
            paddingRight: $theme.sizing.scale500,
            position: 'relative',
          })}
        >
          <div
            data-type="loading-overlay"
            class={css([
              {
                display: 'flex',
              },
              isLoading()
                ? {
                    '::before': {
                      content: "''",
                      position: 'absolute',
                      top: 0,
                      bottom: 0,
                      left: 0,
                      right: 0,
                      backgroundColor: 'rgba(255, 255, 255, .4)',
                      zIndex: 1,
                    },
                  }
                : {},
            ])}
          >
            <div
              class={css({
                flexGrow: 1,
                [$theme.mediaQuery?.md ?? '']: {
                  width: 'calc(100% - 320px)',
                  flexWrap: 'wrap',
                },
              })}
            >
              <Grid
                $override={{
                  Root: {
                    style: {
                      gridGap: '15px',
                    },
                  },
                }}
                columns={[2, 2, 3, 5]}
              >
                <For each={data()?.data?.items?.[0]?.products?.items}>
                  {(product: any) => {
                    const packaging = createMemo(() => {
                      return product.custom_attributes.find(
                        (el: any) => el.attribute_metadata.code === 'packaging',
                      ).entered_attribute_value.value;
                    }, [product]);
                    
                    return (
                      <ProductTile
                        price={
                          product?.price.minimum_price.final_price.value?.toString() ??
                          ''
                        }
                        currency={
                          product?.price.minimum_price?.final_price?.currency ??
                          'AED'
                        }
                        sku={product?.sku ?? ''}
                        title={product?.title ?? product?.name ?? ''}
                        image={product?.image?.url ?? ''}
                        id={product?.id ?? ''}
                        handle={(product?.url_key ?? '') + '.html'}
                        description={product?.description ?? ''}
                        shortDescription={product?.short_description.html ?? ''}
                        weight={product?.weight ?? ''}
                        packaging={packaging()}
                        salableStock={product?.salableStock}
                      />
                    );
                  }}
                </For>
              </Grid>
            </div>
          </div>

          <div
            class={css({
              display: (data()?.data?.items?.[0]?.products?.items?.length !== 0 && typeof data()?.data?.items?.[0]?.products?.items?.length !== 'undefined') ? 'flex' : 'none',
              alignContent: 'center',
              placeContent: 'center',
              marginTop: '24px',
            })}
          >
            <Show when={currentPage() !== 1} keyed={true}>
              <Button
                $size={Size.Large}
                onClick={() => {
                  setQueryParams(
                    { page: currentPage() - 1 },
                    {
                      scroll: true,
                    },
                  );
                }}
              >
                Previous
              </Button>
            </Show>
            <span class={css({ width: '12px' })}></span>
            <Button
              $size={Size.Large}
              onClick={() =>
                setQueryParams(
                  { page: currentPage() + 1 },
                  {
                    scroll: true,
                  },
                )
              }
            >
              Next
            </Button>
          </div>
        </div>
      </div>
    </>
  );
}
