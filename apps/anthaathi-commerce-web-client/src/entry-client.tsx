import { mount, StartClient } from 'solid-start/entry-client';
import { StyletronProvider } from '@anthaathi/solid-styletron';
import { createLightTheme } from '~/utils/createLightTheme';
import { Client } from 'styletron-engine-atomic';
import { CartProvider } from '~/Features/Cart/Provider/CartProvider';

import { ApolloProvider } from '@merged/solid-apollo';

import { client as apolloClient } from './utils/fetchGraphQL';

const theme = createLightTheme();

const client = new Client({
  prefix: '_',
  hydrate: document.querySelectorAll('.styletron'),
});

mount(
  () => (
    <StyletronProvider theme={theme} client={client}>
      <ApolloProvider client={apolloClient}>
        <CartProvider>
          <StartClient />
        </CartProvider>
      </ApolloProvider>
    </StyletronProvider>
  ),
  document,
);
