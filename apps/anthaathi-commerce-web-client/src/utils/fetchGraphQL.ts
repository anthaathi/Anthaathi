import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client';
import { isServer } from 'solid-js/web';
import { setContext } from '@apollo/client/link/context';

const graphql =
  typeof process !== 'undefined' ? process.env.GRAPHQL_ENDPOINT : '';

const authLink = setContext(async (_, { headers }) => {
  if (typeof localStorage === 'undefined') {
    return {
      headers,
    };
  }

  const userToken = localStorage.getItem(
    'M2_VENIA_BROWSER_PERSISTENCE__signin_token',
  );

  const newHeaders = { ...headers };

  if (userToken) {
    newHeaders['Authorization'] = `Bearer ${JSON.parse(
      JSON.parse(userToken).value,
    )}`;
  }

  return {
    headers: newHeaders,
  };
});

const httpLink = createHttpLink({
  uri: graphql ?? '/graphql' ?? 'test',
  credentials: 'same-origin',
});

export const client = new ApolloClient({
  cache: new InMemoryCache(),
  ssrMode: isServer, // set to true for SSR
  link: authLink.concat(httpLink),
});
