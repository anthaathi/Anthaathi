package org.anthaathi.gateway.types

import org.anthaathi.commerce.checkout.utils.RelayUtils
import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Ignore
import java.util.*

class CustomPaymentType : PaymentType, Node {
    override var title: String = type

    companion object {
        const val type = "CustomPayment"
    }

    @Ignore
    var rawId: UUID? = null

    @get:Id
    @set:Id
    override var id: String
        get() = RelayUtils.fromGlobalID(RelayUtils.GlobalID(rawId!!.toString(), type))!!
        set(value) {
            val result = RelayUtils.toGlobalID(value, type)
            rawId = UUID.fromString(result.id)
        }

}
