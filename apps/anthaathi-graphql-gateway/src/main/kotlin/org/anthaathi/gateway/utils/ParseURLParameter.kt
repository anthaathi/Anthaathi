package org.anthaathi.commerce.checkout.utils

import java.net.URLDecoder

class ParseURLParameter {
    companion object {
        fun getUrlParameters(url: String): Map<String, MutableList<String>> {
            val params: MutableMap<String, MutableList<String>> = HashMap()
            val urlParts = url.split("\\?".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            if (urlParts.size > 1) {
                val query = urlParts[1]
                for (param in query.split("&".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()) {
                    val pair = param.split("=".toRegex(), limit = 2).toTypedArray()
                    val key: String = URLDecoder.decode(pair[0], "UTF-8")
                    var value = ""
                    if (pair.size > 1) {
                        value = URLDecoder.decode(pair[1], "UTF-8")
                    }
                    var values = params[key]
                    if (values == null) {
                        values = ArrayList()
                        params[key] = values
                    }
                    values.add(value)
                }
            }
            return params
        }
    }
}
