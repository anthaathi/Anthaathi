package org.anthaathi.gateway.types

enum class DiscountApplicationTargetSelection {
    ALL,
    ENTITLED,
    EXPLICIT,
}
