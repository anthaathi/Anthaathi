package org.anthaathi.gateway.types

class CheckoutDiscountCodeApplyPayload {
    var checkout: Checkout? = null
    var checkoutUserErrors: List<CheckoutUserError>? = null
}
