package org.anthaathi.gateway.types

class CheckoutShippingAddressUpdatePayload {
    var checkout: Checkout? = null
    var checkoutUserErrors: List<CheckoutUserError>? = null
}
