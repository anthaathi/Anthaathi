package org.anthaathi.gateway.types

import org.anthaathi.gateway.types.CustomerErrorCode

class CustomerUserError {
    var code: CustomerErrorCode? = null
    var field: List<String>? = null
    var message: String? = null
}
