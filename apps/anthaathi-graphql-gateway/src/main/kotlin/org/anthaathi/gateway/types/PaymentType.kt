package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Description

@Description("Payment type")
interface PaymentType : Node {
    var title: String
}
