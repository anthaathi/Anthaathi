package org.anthaathi.gateway.types

class CustomerAddressCreateResponse {
    var address: MailingAddress? = null
    var customerUserErrors: List<CustomerUserError>? = null

    companion object {
        fun fromGRPCObject() {

        }
    }
}
