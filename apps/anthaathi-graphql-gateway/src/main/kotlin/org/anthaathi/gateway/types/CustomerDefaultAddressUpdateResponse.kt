package org.anthaathi.gateway.types

class CustomerDefaultAddressUpdateResponse {
    var address: MailingAddress? = null
    var customerUserErrors: List<CustomerUserError>? = null
}
