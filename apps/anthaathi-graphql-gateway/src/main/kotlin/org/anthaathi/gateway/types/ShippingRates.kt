package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.NonNull

class ShippingRates {
    @NonNull
    var handle: String? = null
    @NonNull
    var price: MoneyV2? = null
    @NonNull
    var title: String? = null
}
