package org.anthaathi.gateway.types

import org.anthaathi.common.CustomAttribute

class AttributeInput : CartAttribute() {
    fun toGRPCType(): CustomAttribute {
        return CustomAttribute.newBuilder()
            .setKey(key)
            .setValue(value)
            .build()
    }
}
