package org.anthaathi.gateway.types

import io.smallrye.graphql.api.Union
import org.eclipse.microprofile.graphql.NonNull

interface DiscountApplication {
    @get:NonNull
    var allocationMethod: DiscountApplicationAllocationMethod?

    @get:NonNull
    var targetSelection: DiscountApplicationTargetSelection?

    @get:NonNull
    var targetType: DiscountApplicationTargetType?

    @get:NonNull
    var value: PricingValue?
}
