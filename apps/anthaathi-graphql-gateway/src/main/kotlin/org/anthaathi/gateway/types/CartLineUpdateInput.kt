package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Input
import org.eclipse.microprofile.graphql.NonNull

@Input
class CartLineUpdateInput {
    var attributes: List<AttributeInput>? = null
    @NonNull
    @Id
    var id: String? = null

    var quantity: Int? = null
}
