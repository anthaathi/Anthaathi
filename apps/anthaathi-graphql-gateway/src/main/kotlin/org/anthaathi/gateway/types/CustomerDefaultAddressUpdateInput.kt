package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Input
import org.eclipse.microprofile.graphql.NonNull

@Input
class CustomerDefaultAddressUpdateInput {
    @NonNull
    var customerAccessToken: String? = null
    @NonNull
    var id: String? = null
}
