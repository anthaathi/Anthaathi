package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Input
import org.eclipse.microprofile.graphql.NonNull

@Input
class CustomerAddressUpdateInput {
    @NonNull
    var id: String? = null

    @NonNull
    var customerAccessToken: String? = null

    var address: MailingAddressInput? = null
}
