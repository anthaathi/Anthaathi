package org.anthaathi.gateway.gqlservice

import org.anthaathi.gateway.types.Node
import org.eclipse.microprofile.graphql.GraphQLApi
import org.eclipse.microprofile.graphql.NonNull
import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Query

@GraphQLApi
class CommonNodeGQLService {
    @Query
    fun node(@Id @NonNull id: String): Node? {
        TODO("")
    }

    @Query
    fun nodes(@NonNull @Id ids: @NonNull List<String>): Node {
        TODO("")
    }
}
