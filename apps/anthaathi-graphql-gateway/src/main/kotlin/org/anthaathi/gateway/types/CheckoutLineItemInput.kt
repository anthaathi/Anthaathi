package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Input
import org.eclipse.microprofile.graphql.NonNull
import org.anthaathi.common.CheckoutLineItemInput as CheckoutLineItemInputGRPC

@Input
open class CheckoutLineItemInput {
    fun toGRPCType(): CheckoutLineItemInputGRPC {
        return CheckoutLineItemInputGRPC.newBuilder()
            .setQuantity(this.quantity!!)
            .setVariantId(this.variantId)
            .addAllCustomAttributes(this.customAttributes?.map { it.toGRPCType() })
            .build()
    }

    @NonNull
    var quantity: Long? = null

    @Id
    @NonNull
    var variantId: String? = null

    var customAttributes: List<AttributeInput>? = null
}
