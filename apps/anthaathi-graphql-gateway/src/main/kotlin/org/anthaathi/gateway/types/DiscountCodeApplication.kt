package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.NonNull

class DiscountCodeApplication : DiscountApplication {
    @NonNull
    var applicable: Boolean? = null

    @NonNull
    var code: String? = null

    @NonNull
    override var value: PricingValue? = null

    @NonNull
    override var allocationMethod: DiscountApplicationAllocationMethod? = null

    @NonNull
    override var targetSelection: DiscountApplicationTargetSelection? = null

    @NonNull
    override var targetType: DiscountApplicationTargetType? = null

}
