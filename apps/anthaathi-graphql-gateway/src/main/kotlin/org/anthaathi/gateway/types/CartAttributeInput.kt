package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Input

@Input
class CartAttributeInput : CartAttribute()
