package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.NonNull

class AvailableShippingRates {
    var ready: Boolean? = null
    var shippingRates: List<@NonNull ShippingRates>? = null
}
