package org.anthaathi.gateway.types

import io.smallrye.graphql.api.Union

@Union
interface PricingValue {}
