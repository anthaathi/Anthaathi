package org.anthaathi.gateway.types

enum class CurrencyCode {
    AED,
    INR
}
