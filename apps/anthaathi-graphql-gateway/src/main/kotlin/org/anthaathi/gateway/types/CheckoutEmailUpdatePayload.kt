package org.anthaathi.gateway.types

class CheckoutEmailUpdatePayload {
    var checkout: Checkout? = null
    var checkoutUserErrors: List<CheckoutUserError>? = null
}
