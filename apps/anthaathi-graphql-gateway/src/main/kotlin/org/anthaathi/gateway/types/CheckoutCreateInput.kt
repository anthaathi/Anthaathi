package org.anthaathi.gateway.types

import org.anthaathi.common.CheckoutCreateMessage
import org.eclipse.microprofile.graphql.Input
import org.eclipse.microprofile.graphql.NonNull

@Input
class CheckoutCreateInput {
    var lineItems: List<@NonNull CheckoutLineItemInput>? = null
    var buyerIdentity: CheckoutBuyerIdentityInput? = null
    var customAttributes: List<AttributeInput>? = null
    var email: String? = null
    var note: String? = null
    var shippingAddress: MailingAddressInput? = null

    fun toGRPCObject(customerId: String): CheckoutCreateMessage {
        return CheckoutCreateMessage.newBuilder()
            .setInput(
                org.anthaathi.common.CheckoutCreateInput.newBuilder()
                    .setEmail(email)
                    .setNote(note)
                    .addAllLineItems(lineItems?.map { it.toGRPCType() })
                    .setBuyerIdentity(buyerIdentity?.toGRPCObject(customerId))
                    .addAllCustomAttributes(customAttributes?.map { it.toGRPCType() })
                    .setShippingAddress(shippingAddress?.toGRPCType())
                    .build()
            )
            .build()
    }
}
