package org.anthaathi.gateway.types

import org.anthaathi.commerce.checkout.utils.RelayUtils
import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Ignore
import org.eclipse.microprofile.graphql.NonNull
import java.util.*

class Image : Node {
    companion object {
        const val type = "Image"
    }

    @Ignore
    var rawId: UUID? = null

    @get:Id
    @set:Id
    override var id: String
        get() = RelayUtils.fromGlobalID(RelayUtils.GlobalID(rawId!!.toString(), type))!!
        set(value) {
            val result = RelayUtils.toGlobalID(value, type)
            rawId = UUID.fromString(result.id)
        }


    @NonNull
    var url: String? = null
}
