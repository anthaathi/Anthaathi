package org.anthaathi.gateway.types

import org.anthaathi.gateway.types.MailingAddressInput
import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Input
import java.util.*

@Input
data class DeliveryAddressPreferences(
    @Id
    var id: String,
    var deliveryAddress: MailingAddressInput
)
