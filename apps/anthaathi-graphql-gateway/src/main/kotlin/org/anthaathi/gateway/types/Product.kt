package org.anthaathi.gateway.types

import graphql.relay.Connection
import org.anthaathi.commerce.checkout.utils.RelayUtils
import org.eclipse.microprofile.graphql.Id
import org.eclipse.microprofile.graphql.Ignore
import org.eclipse.microprofile.graphql.NonNull
import java.util.*
import org.anthaathi.common.Product as ProductGPRC

class Product : Node {
    @Ignore
    var rawId: UUID? = null

    @get:Id
    @set:Id
    override var id: String
        get() = RelayUtils.fromGlobalID(RelayUtils.GlobalID(rawId!!.toString(), type))!!
        set(value) {
            val result = RelayUtils.toGlobalID(value, type)
            rawId = UUID.fromString(result.id)
        }


    var title: String? = null
    var description: String? = null
    var handle: String? = null
    var origin: String? = null
    var vendor: String? = null
    @NonNull
    var priceRange: ProductPriceRange? = null

    companion object {
        fun fromGRPCType(input: ProductGPRC): Product {
            val returnValue = Product()

            returnValue.rawId = UUID.fromString(input.id)
            returnValue.handle = input.handle
            returnValue.origin = input.origin
            returnValue.title = input.title
            returnValue.description = input.description
            returnValue.vendor = input.vendor

            return returnValue
        }

        const val type = "Product"
    }
}

