package org.anthaathi.gateway.types

class CheckoutLineItemsRemovePayload {
    var checkout: Checkout? = null
    var checkoutUserErrors: List<CheckoutUserError>? = null
}
