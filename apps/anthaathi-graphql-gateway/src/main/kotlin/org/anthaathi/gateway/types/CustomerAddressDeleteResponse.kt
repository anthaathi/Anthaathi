package org.anthaathi.gateway.types

import org.eclipse.microprofile.graphql.Id

class CustomerAddressDeleteResponse {
    var customerUserErrors: List<CustomerUserError>? = null

    @Id
    var deletedCustomerAddressId: String? = null
}
