import { gql, useMutation, useQuery } from '@apollo/client';
import { useStyletron } from 'baseui';
import { Button } from 'baseui/button';
import React from 'react';
import {
  AddressCard,
  AddressCardProps,
} from '../../../features/address/components/AddressCard';
import { Modal, ModalBody, ModalHeader } from 'baseui/modal';
import { AddAddress } from '../AddAddress';

export interface AddressBookProps {
  defaultAddress: AddressCardProps;
  addresses: AddressCardProps[];
  reload: () => void;
}

const getCustomerAddresses = gql`
  query customerAddresses {
    customer {
      firstname
      lastname
      suffix
      email
      default_shipping
      addresses {
        id
        firstname
        lastname
        street
        city
        region {
          region_code
          region
          region_id
        }
        postcode
        country_code
        telephone
      }
    }
  }
`;

const deleteAddressQuery = gql`
  mutation deleteAddress($id: Int!) {
    deleteCustomerAddress(id: $id)
  }
`;

export function AddressDetails(props: AddressBookProps) {
  const [css, $theme] = useStyletron();
  const [isDialogOpen, setDialogOpen] = React.useState(false);
  const [addAddress, setAddAddress] = React.useState(false);
  const [addressValue, setAddressValue] = React.useState<{
    id: number;
    firstName: string;
    lastName: string;
    address: string;
    apartment: string;
    area: string;
    emirate: any;
    telephone: string;
    email: string;
    country: string;
  } | null>(null);

  const [addNewAddress, { loading }] = useMutation(gql`
    mutation addNewAddressForCustomer($input: CustomerAddressInput!) {
      createCustomerAddress(input: $input) {
        id
      }
    }
  `);

  const [updateAddress, { loading: loadingUpdate }] = useMutation(gql`
    mutation updateAddress($id: Int!, $input: CustomerAddressInput!) {
      updateCustomerAddress(id: $id, input: $input) {
        id
      }
    }
  `);

  const [deleteAddress] = useMutation(deleteAddressQuery);

  return (
    <div
      className={css({
        width: '100%',
        height: '100%',
        boxSizing: 'border-box',
      })}
    >
      <div
        className={css({
          fontSize: $theme.sizing.scale600,
          fontWeight: 400,
          textAlign: 'left',
          paddingBottom: $theme.sizing.scale700,
        })}
      >
        The following addresses will be used on the checkout page by default.
      </div>
      <AddressCard
        address={props.defaultAddress}
        editClick={(address: AddressCardProps) => {
          console.log(address);
          setAddAddress(false);
          setAddressValue({
            ...addressValue,
            id: address.id,
            firstName: address.firstname,
            lastName: address.lastname,
            address: address.street.join(),
            apartment: '',
            area: '',
            emirate: {
              id: address.region.region_id,
              label: address.region.region,
            },
            telephone: address.telephone,
            email: '',
            country: address.country_code,
          });
          setDialogOpen(true);
        }}
        defaultAddress
        deleteClick={() => {}}
        makeDefaultClick={() => {}}
      />

      <div
        className={css({
          fontSize: $theme.sizing.scale600,
          fontWeight: 400,
          textAlign: 'left',
          paddingBottom: $theme.sizing.scale700,
        })}
      >
        The following addresses are available during the checkout process.
      </div>
      {props.addresses.map((data: any, index: number) => (
        <AddressCard
          address={data}
          key={index}
          editClick={(address: AddressCardProps) => {
            console.log(address);
            setAddressValue({
              ...addressValue,
              id: address.id,
              firstName: address.firstname,
              lastName: address.lastname,
              address: address.street.join(),
              apartment: '',
              area: '',
              emirate: {
                id: address.region.region_id,
                label: address.region.region,
              },
              telephone: address.telephone,
              email: '',
              country: address.country_code,
            });
            setDialogOpen(true);
          }}
          makeDefaultClick={(address: AddressCardProps) => {
            updateAddress({
              variables: {
                id: address.id,
                input: {
                  default_shipping: true,
                },
              },
            })
              .then((docs) => {
                props.reload();
              })
              .catch((e) => {});
          }}
          deleteClick={(address: AddressCardProps) => {
            deleteAddress({
              variables: {
                id: address.id,
              },
            })
              .then((docs) => {
                props.reload();
              })
              .catch((e) => {});
          }}
        />
      ))}
      <div
        className={css({
          display: 'flex',
          flexDirection: 'row-reverse',
        })}
      >
        <Button
          onClick={() => {
            setAddAddress(true);
            setDialogOpen(true);
          }}
          overrides={{
            BaseButton: {
              style: ({ $theme }) => ({
                marginBottom: $theme.sizing.scale1000,
                backgroundColor: '#0f8443',
                ':hover': {
                  backgroundColor: '#0f8443',
                },
              }),
            },
          }}
        >
          Add new address
        </Button>
      </div>
      <Modal
        isOpen={isDialogOpen}
        closeable
        animate
        autoFocus
        onClose={() => {
          setDialogOpen(false);
        }}
        overrides={{
          Dialog: {
            style: {
              [$theme.mediaQuery?.large || '']: {
                width: '40vw',
              },
              [$theme.mediaQuery?.medium || '']: {
                width: '70vw',
              },
              width: '100%',
              display: 'flex',
              flexDirection: 'column',
            },
          },
        }}
      >
        <ModalHeader>
          {addAddress ? 'Add new Address' : 'Edit Address'}
        </ModalHeader>

        <ModalBody>
          <AddAddress
            isSubmitting={loading}
            onChange={(values) => {
              addNewAddress({
                variables: {
                  input: {
                    firstname: values.firstName,
                    lastname: values.lastName,
                    telephone: values.telephone,
                    country_code: values.country,
                    street: values.address.split('\n'),
                    city: values.area,
                    region: {
                      region_id: +values.emirate,
                    },
                  },
                },
              })
                .then((docs) => {
                  props.reload();
                })
                .catch((e) => {});
            }}
            initialValue={addAddress ? {} : addressValue}
          />
        </ModalBody>
      </Modal>
    </div>
  );
}

export default function AddressBook() {
  const [css, $theme] = useStyletron();
  const { loading, data, error, refetch } = useQuery(getCustomerAddresses);

  const defaultAddress: AddressCardProps[] = React.useMemo(() => {
    if (!loading && data.customer) {
      return data.customer.addresses.filter(
        (item: any) => item.id === +data.customer.default_shipping,
      );
    }
  }, [data, loading]);

  if (loading && !defaultAddress) {
    return <div>loading...</div>;
  }

  return (
    <div
      className={css({
        marginLeft: $theme.sizing.scale500,
        marginTop: $theme.sizing.scale500,
      })}
    >
      <AddressDetails
        reload={() => {
          refetch();
        }}
        defaultAddress={defaultAddress[0]}
        addresses={data.customer.addresses}
      />
    </div>
  );
}
