import React from 'react';
import {
  I18nManager,
  Image,
  Platform,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import {Appbar, useTheme, TextInput, Colors} from 'react-native-paper';
import RNRestart from 'react-native-restart';
import {useIntl} from 'react-intl';

export interface HeaderProps {
  leftIcon?: string;
  leftOnPress?: () => void;
  languageIcon?: boolean;
  mailIcon?: boolean;
  searchIcon?: boolean;
  title?: string;
  logoImage?: string;
  isInlineSearch?: boolean;
}

const Header = ({
  leftIcon,
  leftOnPress,
  languageIcon = false,
  mailIcon = false,
  searchIcon = false,
  title = '',
  logoImage,
  isInlineSearch = false,
}: HeaderProps) => {
  const intl = useIntl();
  const [searchShow, setSearchShow] = React.useState(false);
  const theme = useTheme();

  async function changeLanguage() {
    if (I18nManager.isRTL) {
      await I18nManager.forceRTL(false);
    } else {
      if (!I18nManager.isRTL) {
        await I18nManager.forceRTL(true);
      }
    }
    RNRestart.Restart();
  }

  return (
    <View testID="header" style={{backgroundColor: '#fff', elevation: 1}}>
      <StatusBar
        animated={true}
        backgroundColor={theme.colors.primary}
        barStyle={
          theme.dark || Platform.OS === 'ios' ? 'dark-content' : 'light-content'
        }
      />
      <Appbar.Header
        style={{
          height: 56,
          backgroundColor: '#fff', // theme.colors.background,
          elevation: 0,
        }}>
        {leftIcon && (
          <Appbar.Action
            testID="leftOnHandler"
            icon={leftIcon}
            color={Colors.grey800}
            onPress={leftOnPress}
          />
        )}
        {languageIcon && (
          <Appbar.Action
            testID="translateOnHandler"
            icon={'translate'}
            color={Colors.grey800}
            onPress={() => {
              changeLanguage();
            }}
          />
        )}

        {logoImage && (
          <>
            <Appbar.Content
              title={''}
              style={{
                alignItems: 'center',
              }}
            />
            <Image
              testID="headerImage"
              style={[
                {
                  height: 52,
                  width: 70,
                  // alignItems: 'center'
                  // marginLeft: leftIcon ? '14%' : '40%',
                },
              ]}
              source={{
                uri: logoImage,
              }}
            />
          </>
        )}

        <Appbar.Content title={title} />

        {mailIcon && (
          <Appbar.Action
            onPress={() => {}}
            color={Colors.grey800}
            icon="email-outline"
          />
        )}

        {searchIcon && (
          <Appbar.Action
            testID="searchOnHandler"
            icon={'magnify'}
            color={Colors.grey800}
            onPress={() => {
              setSearchShow(!searchShow);
            }}
          />
        )}
      </Appbar.Header>
      {(searchShow || isInlineSearch) && (
        <View
          style={[
            I18nManager.isRTL ? styles.containerRTL : styles.containerLTR,
            {paddingTop: 2, paddingBottom: 10},
          ]}>
          <TextInput
            placeholder={intl.formatMessage({defaultMessage: 'Search'})}
            dense
            underlineColor="transparent"
          />
        </View>
      )}
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  headerAlign: {
    alignItems: 'center',
  },
  containerLTR: {
    paddingRight: 24,
    paddingLeft: 8,
  },
  containerRTL: {
    paddingLeft: 48,
    paddingRight: 0,
  },
});
