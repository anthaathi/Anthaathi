import {StyleSheet, View} from 'react-native';
import React from 'react';
import {TouchableRipple} from 'react-native-paper';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export type CheckBoxProps = {
  status: boolean;
  disabled?: boolean;
  onPress?: () => void;
  color: string;
  rippleColor?: string;
};

const CheckBox = ({
  status,
  disabled,
  onPress,
  color,
  rippleColor,
}: CheckBoxProps) => {
  const checkedColor = disabled ? '#999' : color;

  if (disabled) {
    rippleColor = '#999';
  } else {
    rippleColor = rippleColor || '#999';
  }

  const icon = status ? 'checkbox-outline' : 'checkbox-blank-outline';

  return (
    <TouchableRipple
      borderless
      rippleColor={rippleColor}
      onPress={onPress}
      disabled={false}
      style={styles.container}>
      <View>
        <MaterialCommunityIcon
          allowFontScaling={false}
          name={icon}
          size={24}
          color={checkedColor}
        />
      </View>
    </TouchableRipple>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 18,
    padding: 6,
  },
});

export default CheckBox;
