import {View, Text, TextStyle, StyleProp} from 'react-native';
import React from 'react';
import {Card, useTheme} from 'react-native-paper';
import {useIntl} from 'react-intl';

export interface ViewTextProps {
  title: string;
  subtitle: string;
  titleStyle?: StyleProp<TextStyle>;
  subtitleStyle?: StyleProp<TextStyle>;
}

export interface PriceCurrencyProps {
  currency: string;
  price: number;
}
export interface PricingCardProps {
  title?: string;
  subtotal: PriceCurrencyProps;
  discount: PriceCurrencyProps;
  taxAmount: PriceCurrencyProps;
  shippingCharges: PriceCurrencyProps;
  total: PriceCurrencyProps;
}

const PricingCard = (props: PricingCardProps) => {
  const intl = useIntl();
  const theme = useTheme();

  return (
    <View
      style={{marginHorizontal: 10, marginVertical: 5}}
      testID="pricingCard">
      {props.title && (
        <Text
          style={{
            color: theme.colors.titleTextColor,
            fontSize: 16,
            fontWeight: '600',
            marginBottom: 5,
          }}>
          {props.title}
        </Text>
      )}

      <Card
        style={{
          borderColor: theme.colors.cardBorderColor,
          borderWidth: 1,
          borderRadius: 4,
        }}>
        <Card.Content>
          <ViewText
            title={intl.formatMessage({defaultMessage: 'Subtotal'})}
            subtitle={intl.formatNumber(props.subtotal.price, {
              style: 'currency',
              currency: props.subtotal.currency,
            })}
          />
          <ViewText
            title={intl.formatMessage({defaultMessage: 'Tax (5% VAT)'})}
            subtitle={intl.formatNumber(props.taxAmount.price, {
              style: 'currency',
              currency: props.taxAmount.currency,
            })}
          />
          <ViewText
            title={intl.formatMessage({defaultMessage: 'Discount'})}
            subtitle={intl.formatNumber(props.discount.price, {
              style: 'currency',
              currency: props.discount.currency,
            })}
          />
          <ViewText
            title={intl.formatMessage({defaultMessage: 'Shipping Charges.'})}
            subtitle={intl.formatNumber(props.shippingCharges.price, {
              style: 'currency',
              currency: props.shippingCharges.currency,
            })}
            subtitleStyle={{fontWeight: '500'}}
          />
          <ViewText
            title={intl.formatMessage({defaultMessage: 'Total'})}
            subtitle={intl.formatNumber(props.total.price, {
              style: 'currency',
              currency: props.total.currency,
            })}
            titleStyle={{
              fontSize: 16,
              fontWeight: '700',
              color: theme.colors.titleTextColor,
            }}
            subtitleStyle={{
              fontSize: 16,
              fontWeight: '700',
              color: theme.colors.greenTextColor,
            }}
          />
        </Card.Content>
      </Card>
    </View>
  );
};

const ViewText = ({
  title,
  subtitle,
  titleStyle,
  subtitleStyle,
}: ViewTextProps) => {
  const theme = useTheme();
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginHorizontal: 3,
        marginVertical: 5,
      }}>
      <Text
        testID="title"
        style={[{color: theme.colors.greyTextColor, fontSize: 14}, titleStyle]}>
        {title}
      </Text>
      <Text
        testID="subtitle"
        style={[
          {color: theme.colors.titleTextColor, fontSize: 14, fontWeight: '600'},
          subtitleStyle,
        ]}>
        {subtitle}
      </Text>
    </View>
  );
};

export default PricingCard;
