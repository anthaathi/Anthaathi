import {View, ScrollView, Text} from 'react-native';
import React from 'react';
import {RootStackParamList} from '../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import {Button, TextInput, useTheme} from 'react-native-paper';
import {useIntl} from 'react-intl';
import Header from '../../features/containers/Core/components/Header';
import SelectOption from '../../features/containers/Core/components/SelectOption';

const ReturnItemPage = (
  props: NativeStackScreenProps<RootStackParamList, 'ReturnItemPage'>,
) => {
  const intl = useIntl();
  const theme = useTheme();

  return (
    <View style={{flex: 1}}>
      <Header
        leftIcon={'arrow-left'}
        leftOnPress={() => props.navigation.goBack()}
        title={'Return Order'}
      />
      <ScrollView
        contentContainerStyle={{
          flexGrow: 1,
          paddingBottom: 100,
          paddingHorizontal: 10,
          paddingVertical: 5,
        }}>
        <SelectOption
          title="Select Reason"
          options={[
            {
              id: 1,
              key: '1',
              title: 'Reason 1',
            },
            {
              id: 2,
              key: '2',
              title: 'Reason 2',
            },
            {
              id: 3,
              key: '3',
              title: 'Reason 3',
            },
          ]}
        />
        <View style={{marginVertical: 5}}>
          <Text
            style={{
              color: theme.colors.titleTextColor,
              fontSize: 16,
              fontWeight: '600',
            }}>
            Reason for return
          </Text>
          <TextInput
            mode="flat"
            label={intl.formatMessage({
              defaultMessage: 'Enter reason',
            })}
            style={{
              backgroundColor: '#fff',
              fontSize: 14,
              height: 120,
              marginTop: 5,
            }}
            multiline={true}
            activeUnderlineColor={theme.colors.primary}
          />
        </View>
      </ScrollView>
      <Button
        mode="contained"
        style={{
          marginVertical: 10,
          marginHorizontal: '4%',
          borderRadius: 4,
          width: '92%',
          position: 'absolute',
          bottom: 0,
        }}
        labelStyle={{
          paddingVertical: 5,
        }}
        onPress={() => {
          props.navigation.goBack();
        }}>
        Save
      </Button>
    </View>
  );
};

export default ReturnItemPage;
