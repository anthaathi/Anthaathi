import {NativeStackScreenProps} from '@react-navigation/native-stack';
import * as React from 'react';
import {ScrollView, View} from 'react-native';
import {DataTable} from 'react-native-paper';
import {RootStackParamList} from '../../../types/Route';

const optionsPerPage = [2, 3, 4];
const Delivered = (
  _props: NativeStackScreenProps<RootStackParamList, 'Delivered'>,
) => {
  const [page, setPage] = React.useState<number>(0);
  const [itemsPerPage, setItemsPerPage] = React.useState(optionsPerPage[0]);

  React.useEffect(() => {
    setPage(0);
  }, [itemsPerPage]);

  return (
    <View style={{marginHorizontal: 10, marginVertical: 12}}>
      <ScrollView contentContainerStyle={{flexGrow: 1}}>
        <DataTable>
          <DataTable.Header>
            <DataTable.Title>Order No.</DataTable.Title>
            <DataTable.Title>Addresss</DataTable.Title>
            <DataTable.Title>Date</DataTable.Title>
            <DataTable.Title>Time</DataTable.Title>
          </DataTable.Header>

          {[...Array(6)].map((_, index) => (
            <DataTable.Row key={index}>
              <DataTable.Cell>{index}</DataTable.Cell>
              <DataTable.Cell>UAE</DataTable.Cell>
              <DataTable.Cell>12/12/2022</DataTable.Cell>
              <DataTable.Cell>12:30 AM</DataTable.Cell>
            </DataTable.Row>
          ))}

          <DataTable.Pagination
            page={page}
            numberOfPages={3}
            onPageChange={pageNo => setPage(pageNo)}
            label="1-2 of 6"
            optionsPerPage={optionsPerPage}
            itemsPerPage={itemsPerPage}
            setItemsPerPage={setItemsPerPage}
            showFastPagination
            optionsLabel={'Rows per page'}
          />
        </DataTable>
      </ScrollView>
    </View>
  );
};

export default Delivered;
