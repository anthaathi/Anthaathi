import {ScrollView, View} from 'react-native';
import React from 'react';
import {RootStackParamList} from '../../types/Route';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import Header from '../../features/containers/Core/components/Header';
import CartCard from '../../features/containers/HomePage/components/CartCard';
import OrderedItems from '../../features/containers/OrderDetailsPage/components/OrderedItems';
import DeliveryAddressDetailsCard from '../../features/containers/OrderDetailsPage/components/DeliveryAddressDetailsCard';
import PricingCard from '../../features/containers/OrderDetailsPage/components/PricingCard';
import {Button} from 'react-native-paper';
import CustomerSignature from '../../features/containers/Core/components/CustomerSignature';

const OrderDetailsPage = (
  props: NativeStackScreenProps<RootStackParamList, 'OrderDetailsPage'>,
) => {
  const [modalVisible, setModalVisible] = React.useState(false);
  return (
    <View style={{flex: 1}}>
      <Header
        leftIcon={'arrow-left'}
        leftOnPress={() => props.navigation.goBack()}
        title={'Order Details'}
      />
      <ScrollView contentContainerStyle={{flexGrow: 1, paddingBottom: 50}}>
        <CartCard
          key={(1).toString()}
          title="Order #123"
          statusTitle="In Transit"
          statusIcon="basket"
          deliveryDate="Sun, 17 Jul 2022"
          deliveryAddress="13C, UAE"
          noOfItems="2 Items"
          orderStatus={true}
          imageList={[
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/a-papaya-is-surrounded-by-fruit-on-yellow-background_900x.jpg?v=1653586970',
            'https://cdn.shopify.com/s/files/1/0648/1303/9842/files/fresh-vegetables-flatlay_900x.jpg?v=1653677616',
          ]}
        />
        <OrderedItems
          title="Items"
          items={[
            {
              id: 1,
              name: 'Capsicum mixed',
              image:
                'https://burst.shopifycdn.com/photos/red-and-green-gooseberries-against-white.jpg?width=373&format=pjpg&exif=1&iptc=1',
              key: '213',
              price: 23,
              numberOfItems: 2,
              currency: 'USD',
              weight_unit: 'KG',
              packaging: '500 gms',
            },
            {
              id: 2,
              name: 'Capsicum mixed',
              image:
                'https://burst.shopifycdn.com/photos/red-and-green-gooseberries-against-white.jpg?width=373&format=pjpg&exif=1&iptc=1',
              key: '23',
              price: 23,
              numberOfItems: 2,
              currency: 'USD',
              weight_unit: 'KG',
              packaging: '500 gms',
            },
          ]}
        />
        <DeliveryAddressDetailsCard
          deliveryTitle="Delivery Address"
          deliveryAddress="USA"
          mobileNumber="+90909090"
        />
        <PricingCard
          title="Payment Information"
          subtotal={{currency: 'AED', price: 100}}
          discount={{currency: 'AED', price: 0}}
          taxAmount={{currency: 'AED', price: 100 * 0.05}}
          shippingCharges={{currency: 'AED', price: 0}}
          total={{
            currency: 'AED',
            price: 100 + 100 * 0.05,
          }}
        />
      </ScrollView>
      <View style={{flexDirection: 'row'}}>
        <Button
          mode="contained"
          style={{
            width: '50%',
            backgroundColor: '#E4572E',
            borderTopLeftRadius: 4,
            borderTopRightRadius: 0,
            borderBottomLeftRadius: 0,
            borderBottomRightRadius: 0,
          }}
          labelStyle={{
            paddingVertical: 10,
          }}
          onPress={() => props.navigation.navigate('ReturnItemPage')}>
          Return Item
        </Button>
        <Button
          mode="contained"
          style={{
            width: '50%',
            borderTopRightRadius: 4,
            borderTopLeftRadius: 0,
            borderBottomLeftRadius: 0,
            borderBottomRightRadius: 0,
          }}
          labelStyle={{
            paddingVertical: 10,
          }}
          onPress={() => {
            setModalVisible(!modalVisible);
          }}>
          Mark as delivered
        </Button>
      </View>
      <CustomerSignature
        onRequestClose={() => setModalVisible(!modalVisible)}
        isVisible={modalVisible}
        getSign={(sign: string) => {
          console.log(sign);
        }}
      />
    </View>
  );
};

export default OrderDetailsPage;
