import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {RootStackParamList} from '../types/Route';
import SignInPage from '../pages/Authentication/SignInPage';
import MainPage from '../pages/MainPage';
import ResetPasswordPage from '../pages/Authentication/ResetPasswordPage';
import EditProfile from '../pages/EditProfile';
import OrderDetailsPage from '../pages/OrderDetailsPage';
import PickerOrderDetailsPage from '../pages/PickerOrderDetailsPage';
import ReturnItemPage from '../pages/ReturnItemPage';

const Stack = createNativeStackNavigator<RootStackParamList>();

const MyStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="SignIn"
        component={SignInPage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Dashboard"
        component={MainPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ResetPassword"
        component={ResetPasswordPage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="OrderDetailsPage"
        component={OrderDetailsPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="PickerOrderDetailsPage"
        component={PickerOrderDetailsPage}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="ReturnItemPage"
        component={ReturnItemPage}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default MyStack;
